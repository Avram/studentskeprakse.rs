var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss').version(["css/app.css"]);
    mix.copy('resources/assets/fonts', 'public/build/fonts');
    //mix.copy('resources/assets/css', 'public/build/css');
    mix.copy('resources/assets/images', 'public/images');

    mix.copy('bower_components/magnific-popup/dist/*.min.js', 'public/build/js');
    mix.copy('bower_components/magnific-popup/dist/*.css', 'public/build/css');

    mix.copy('bower_components/vex/js/vex.combined.min.js', 'public/build/js');
    mix.copy('bower_components/vex/css/vex.css', 'public/build/css');
    mix.copy('bower_components/vex/css/vex-theme-default.css', 'public/build/css');

    mix.copy('bower_components/select2/dist/css/select2.min.css', 'public/build/css');
    mix.copy('bower_components/select2/dist/js/select2.full.min.js', 'public/build/js');

    mix.copy('bower_components/toastr/toastr.css', 'public/build/css');
    mix.copy('bower_components/toastr/toastr.min.js', 'public/build/js');

    mix.copy('bower_components/jquery-sortable/source/js/jquery-sortable-min.js', 'public/build/js');

    mix.copy('bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css', 'public/build/css');
    mix.copy('bower_components/malihu-custom-scrollbar-plugin/mCSB_buttons.png', 'public/build/css');
    mix.copy('bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js ', 'public/build/js');


    mix.copy('bower_components/slick-carousel/slick/slick.min.js ', 'public/build/js');
    mix.copy('bower_components/slick-carousel/slick/slick.css ', 'public/build/css');
    mix.copy('bower_components/slick-carousel/slick/slick-theme.css ', 'public/build/css');
    mix.copy('bower_components/slick-carousel/slick/fonts/slick.woff ', 'public/build/css/fonts');
});
