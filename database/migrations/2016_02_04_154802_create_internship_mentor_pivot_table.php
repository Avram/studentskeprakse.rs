<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateInternshipMentorPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internship_mentor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('internship_id');
            $table->integer('mentor_id');

            $table->index(['internship_id', 'mentor_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('internship_mentor');
    }
}
