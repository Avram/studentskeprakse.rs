<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internships', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('views')->unsigned()->default(0);
            $table->string('name');
            $table->text('content');
            $table->text('required_skills');
            $table->string('status');
            $table->string('duration')->nullable();
            $table->string('duration_type')->nullable();
            $table->string('salary')->nullable();
            $table->string('food')->nullable();
            $table->string('work_hours')->nullable();
            $table->string('slug');
            $table->timestamps();

            $table->index('user_id');
            $table->index('city_id');
            $table->index('category_id');

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('internships');
    }
}
