<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->boolean('verified')->default(false);
            $table->string('token')->nullable();;
            $table->string('type')->nullable();;
            $table->string('employees')->nullable();
            $table->integer('founded')->nullable();
            $table->string('address')->nullable();
            $table->text('description')->nullable();
            $table->text('team_description')->nullable();
            $table->string('url')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('linkedin_url')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('slug')->unique();
            $table->string('logo')->nullable();
            $table->string('cover')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->index('category_id');
            $table->index('city_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
