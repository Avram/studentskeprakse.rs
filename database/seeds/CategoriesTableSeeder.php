<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{

    private $categories = array('Administracija, računovodstvo, revizija', 'Arhitektura, geodezija', 'Briga o lepoti', 'Dizajn, umetnost', 'Ekologija', 'Ekonomija', 'Farmacija', 'Finansije, bankarstvo', 'Građevinarstvo', 'Hemija, fizika, matematika', 'Istorija, geografija', 'Informacione tehnologije', 'Izdavačka delatnost', 'Izrada odeće i obuće', 'Jezici, prevođenje, književnost', 'Ljudski resursi (HR)', 'Marketing, PR', 'Mašinstvo, elektrotehnika', 'Medicina, zdravstvo', 'Mediji, kultura, muzika', 'Menadžment', 'Obezbeđenje, zaštita', 'Obrazovanje, školstvo', 'Osiguranje', 'Ostalo', 'Poljoprivreda, šumarstvo', 'Pomoćni poslovi', 'Pravo', 'Prodaja, trgovina', 'Proizvodnja', 'Psihologija, pedagogija, sociologija', 'Saobraćaj', 'Serviseri, mehaničari, električari', 'Sport', 'Tehnologija', 'Transport, špedicija', 'Ugostiteljstvo, turizam', 'Veterina');

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataSet = [];
        foreach ($this->categories as $category) {
            $dataSet[] = [
                'name' => $category
            ];
        }

        DB::table('categories')->insert($dataSet);
    }
}
