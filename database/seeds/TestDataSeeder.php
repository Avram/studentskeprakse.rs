<?php

use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory(StudentskePrakse\Model\Category::class, 10)->create();

        factory(StudentskePrakse\Model\User::class, 50)->create()->each(function ($u) {
            $u->category_id = rand(1, 38);
            $u->city_id     = rand(1, 100);

            $u->save();
        });

        factory(StudentskePrakse\Model\Mentor::class, 300)->create()->each(function ($m) {
            $m->user_id = rand(1, 50);

            $m->save();
        });


        factory(StudentskePrakse\Model\Internship::class, 100)->create()->each(function ($i) {
            $i->user_id     = rand(1, 50);
            $i->city_id     = rand(1, 100);
            $i->category_id = rand(1, 38);

            $mntrs = $i->user()
                ->orderBy(DB::raw('RAND()'))
                ->take(rand(1, 3))
                ->get();

            foreach ($mntrs as $mentor) {
                $i->mentors()->save($mentor);
            }



            $i->save();
        });

        factory(StudentskePrakse\Model\FAQ::class, 100)->create();

        factory(StudentskePrakse\Model\Feedback::class, 100)->create();

    }
}
