<?php

use Illuminate\Database\Seeder;
use Illuminate\Hashing\BcryptHasher;
use StudentskePrakse\Model\User;

class CreateAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $hasher = new BcryptHasher();
        $admin = new User();
        $admin->setName("Administrator");
        $admin->setEmail("admin@studentskeprakse.rs");
        $admin->setPassword($hasher->make('admin'));
        $admin->setType(User::TYPE_ADMIN);
        $admin->setVerified(true);
        $admin->setSlug('administrator');
        $admin->save();

        echo "User \"Administrator\" (admin@studentskeprakse.rs) with password \"admin\" created!\n\n";
    }
}
