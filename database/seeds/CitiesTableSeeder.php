<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesTableSeeder extends Seeder
{

    private $cities = array('Ada', 'Aleksandrovac', 'Aleksinac', 'Aleksinački Rudnik', 'Alibunar', 'Apatin', 'Aranđelovac', 'Arilje', 'Babušnica', 'Batočina', 'Bajina Bašta', 'Bajmok', 'Baljevac', 'Banatski Karlovac', 'Banja Koviljača', 'Bač', 'Bačka Palanka', 'Bačka Topola', 'Bački Jarak', 'Bački Petrovac', 'Belanovica', 'Bela Palanka', 'Bela Crkva', 'Beli Potok', 'Belo Polje', 'Beograd', 'Beočin', 'Bečej', 'Blace', 'Bogovina', 'Boljevac', 'Bor', 'Borča', 'Bosilegrad', 'Brza Palanka', 'Brus', 'Bujanovac', 'Gornji Milanovac', 'Grdelica', 'Grocka', 'Guča', 'Gnjilane', 'Valjevo', 'Velika Plana', 'Veliki Crljeni', 'Veliko Gradište', 'Vladičin Han', 'Vlasotince', 'Vranje', 'Vranjska Banja', 'Vrbas', 'Vrnjačka Banja', 'Vršac', 'Vučje', 'Despotovac', 'Divčibare', 'Dimitrovgrad', 'Dobanovci', 'Donji Milanovac', 'Žabalj', 'Žitište', 'Zaječar', 'Zlatibor', 'Zrenjanin', 'Ivanjica', 'Inđija', 'Irig', 'Jagodina', 'Jaša Tomić', 'Jošanička Banja', 'Kanjiža', 'Kačarevo', 'Kikinda', 'Kladovo', 'Knjaževac', 'Kovačica', 'Kovin', 'Kosjerić', 'Kostolac', 'Kragujevac', 'Kraljevo', 'Krupanj', 'Kruševac', 'Kula', 'Kuršumlija', 'Kuršumlijska Banja', 'Kučevo', 'Lazarevac', 'Lajkovac', 'Lapovo', 'Lebane', 'Leskovac', 'Loznica', 'Lučani', 'Ljig', 'Majdanpek', 'Mali Zvornik', 'Mataruška Banja', 'Mačvanska Mitrovica', 'Medveđa', 'Mionica', 'Mladenovac', 'Mol', 'Negotin', 'Niš', 'Niška Banja', 'Nova Varoš', 'Novi Bečej', 'Novi Kneževac', 'Novi Pazar', 'Novi Sad', 'Obrenovac', 'Ovča', 'Opovo', 'Ostružnica', 'Odžaci', 'Palić', 'Pančevo', 'Paraćin', 'Petrovaradin', 'Petrovac', 'Pećani', 'Pinosava', 'Pirot', 'Požarevac', 'Požega', 'Priboj', 'Prijepolje', 'Priština', 'Prizren', 'Prokuplje', 'Rača', 'Raška', 'Resavica', 'Ribnica', 'Rudovci', 'Ruma', 'Rucka', 'Svilajnac', 'Svrljig', 'Sevojno', 'Senta', 'Sijarinska Banja', 'Sjenica', 'Smederevo', 'Smederevska Palanka', 'Sokobanja', 'Sombor', 'Sopot', 'Srbobran', 'Sremska Kamenica', 'Sremska Mitrovica', 'Sremski Karlovci', 'Stara Pazova', 'Starčevo', 'Subotica', 'Surdulica', 'Surčin', 'Temerin', 'Titel', 'Topola', 'Trstenik', 'Tutin', 'Ćićevac', 'Ćuprija', 'Ub', 'Užice', 'Umka', 'Uljma', 'Futog', 'Crvenka', 'Čajetina', 'Čačak', 'Čoka', 'Čelarevo', 'Šabac', 'Šid');
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataSet = [];
        foreach ($this->cities as $city) {
            $dataSet[] = [
                'name'  => $city
            ];
        }

        DB::table('cities')->insert($dataSet);
    }
}
