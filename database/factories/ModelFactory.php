<?php

$factory->define(StudentskePrakse\Model\User::class, function (Faker\Generator $faker) {
    $name = $faker->unique()->company;
    return [
        'name'             => $name,
        'email'            => $faker->unique()->companyEmail,
        'type'             => 'company',
        'password'         => bcrypt('password'),
        'remember_token'   => str_random(10),
        'employees'        => rand(2, 10000),
        'founded'          => rand(1950, date('Y')),
        'address'          => $faker->streetName.' '.$faker->buildingNumber,
        'description'      => $faker->paragraph(10),
        'team_description' => $faker->paragraph(10),
        'url'              => $faker->unique()->url,
        'facebook_url'     => $faker->unique()->url,
        'twitter_url'      => $faker->unique()->url,
        'linkedin_url'     => $faker->unique()->url,
        'category_id'      => 1,
        'verified'         => true,
        'slug'             => str_slug($name)
    ];
});

$factory->define(StudentskePrakse\Model\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->country
    ];
});

$factory->define(StudentskePrakse\Model\Mentor::class, function (Faker\Generator $faker) {
    return [
        'image'        => '',
        'name'         => $faker->unique()->name,
        'description'  => $faker->sentence(),
        'url'          => $faker->unique()->url,
        'facebook_url' => $faker->unique()->url,
        'twitter_url'  => $faker->unique()->url,
        'linkedin_url' => $faker->unique()->url
    ];
});

$factory->define(StudentskePrakse\Model\Internship::class, function (Faker\Generator $faker) {

    $mealsArr = \StudentskePrakse\Model\Internship::$ALLOWED_MEALS;

    $rnd = rand(0, 4);

    switch ($rnd) {
        case 1:
            $meals = $mealsArr[0];
            break;
        case 2:
            $meals = implode(',', [$mealsArr[0], $mealsArr[1]]);
            break;
        case 3:
            $meals = implode(',', [$mealsArr[0], $mealsArr[1], $mealsArr[2]]);
            break;
        default:
            $meals = null;
            break;
    }


    $name  = $faker->unique()->name.' wanted';

    return [
        'name'            => $name,
        'content'         => $faker->paragraph(10),
        'required_skills' => $faker->paragraph(10),
        'status'          => \StudentskePrakse\Model\Internship::STATUS_PUBLISHED,
        'duration'        => $faker->randomDigit,
        'duration_type'   => \StudentskePrakse\Model\Internship::DURATION_WEEKS,
        'views'           => rand(10, 10000),
        'salary'          => $faker->randomNumber(4),
        'food'            => $meals,
        'work_hours'      => rand(20, 40),
        'slug'            => str_slug($name)
    ];
});

$factory->define(StudentskePrakse\Model\FAQ::class, function (Faker\Generator $faker) {
    return [
        'question'   => $faker->sentence().'?',
        'answer'     => (rand(1, 2) == 1) ? $faker->paragraph(10) : null,
        'owner_type' => StudentskePrakse\Model\User::class,
        'owner_id'   => rand(1, 50)
    ];
});

$factory->define(StudentskePrakse\Model\Feedback::class, function (Faker\Generator $faker) {
    return [
        'name'     => $faker->name,
        'feedback' => $faker->paragraph(10),
        'reply'    => (rand(1, 2) == 1) ? $faker->paragraph(6) : null,
        'user_id'  => rand(1, 50)
    ];
});

$factory->define(StudentskePrakse\Model\Post::class, function (Faker\Generator $faker) {

    $title = $faker->sentence(5);
    return [
        'title'      => $title,
        'content'    => $faker->paragraph(16),
        'status'     => \StudentskePrakse\Model\Post::STATUS_PUBLISHED,
        'user_id'    => rand(1, 50),
        'slug'       => str_slug($title),
        'up_votes'   => rand(0, 1000),
        'down_votes' => rand(0, 1000),
    ];
});