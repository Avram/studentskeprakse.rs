<html>
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700&subset=latin,latin-ext' rel='stylesheet'
          type='text/css'>
    {{--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">--}}
    <link href="{{ asset('css/normalize.css') }}" rel="stylesheet">
    {{--        <link href="{{ asset('css/skeleton.css') }}" rel="stylesheet">--}}
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    <style>
        body {
            font-family: 'Roboto';
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <h4>Form elements</h4>
        <hr/>
        <div class="six columns">
            <div class="row form-element">
                <div class="custom-checkbox">
                    <input type="checkbox" class="checkbox" id="filled-in-box" checked="checked"/>
                    <label for="filled-in-box">Filled in</label>
                </div>
            </div>

        </div>
        <div class="five columns">
            <div class="row form-element">
                <input type="text" class="u-full-width" placeholder="Example"/>
            </div>
            <div class="row form-element">
                <select>
                    <option>Select</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <h4>Iconography</h4>
        <hr/>
        <div class="six columns">
            <span class="icon-comment"></span>
            <span class="icon-trash"></span>
            <span class="icon-pdf1"></span>
            <span class="icon-pen"></span>
            <span class="icon-pic"></span>
            <span class="icon-plus"></span>
            <span class="icon-reply"></span>
            <span class="icon-search"></span>
            <span class="icon-seen"></span>
            <span class="icon-dollar"></span>
            <span class="icon-calendar"></span>
            <span class="icon-person"></span>
            <span class="icon-watch"></span>
            <span class="icon-arrow-down"></span>
            <span class="icon-arrow-up"></span>
            <span class="icon-pdf2"></span>
        </div>
    </div>
    <div class="row">
        <h4>Buttons</h4>
        <hr/>
        <div class="six columns">
            <div class="row twelve columns">
                {{--<button class="btn btn-info">Postavi praksu</button>--}}
                <button class="btn btn-info">Sačuvaj</button>
            </div>
            <div class="row twelve columns">
                {{--<button class="btn btn-success icon"><span class="icon-plus full"></span> Postavi</button>--}}
                <button class="btn btn-success icon"><span class="icon-plus"></span> Postavi</button>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
</body>
</html>