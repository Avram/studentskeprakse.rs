{{--<div class="full-width-tabs">--}}
    {{--<ul class="nav nav-tabs">--}}
        {{--@if(isset($settings) && $settings)--}}
            {{--@foreach($items as $item)--}}
                {{--<li class="@if($item['active']) active @endif">--}}
                    {{--<a href="{{ URL::route('company.'.$item['route']) }}">{{ $item['title'] }}</a>--}}
                {{--</li>--}}
            {{--@endforeach--}}
        {{--@else--}}
            {{--@foreach($items as $item)--}}
                {{--<li class="@if($item['active']) active @endif">--}}
                    {{--<a href="{{ URL::route('company.'.$item['route'], ['username' => $user->slug]) }}">{{ $item['title'] }}</a>--}}
                {{--</li>--}}
            {{--@endforeach--}}
        {{--@endif--}}
    {{--</ul>--}}
{{--</div>--}}

<div class="row twelve columns nav-options">

    @foreach($items as $item)
        <div class="option-item two columns">
            <a @if($item['active']) class="active" href="#" @else href="{{ URL::route($item['route']) }}" @endif>{{ $item['title'] }}</a>
        </div>
    @endforeach
    {{--<div class="option-item two columns">--}}
        {{--<a href="{{ URL::route('company.internships.trash') }}">Obrisano</a>--}}
    {{--</div>--}}
    {{--<div class="option-item two columns">--}}
        {{--<a href="{{ URL::route('company.internships.management') }}">Prakse</a>--}}
    {{--</div>--}}
    {{--<div class="option-item two columns">--}}
        {{--<a href="{{ URL::route('company.mentors.management') }}">Mentori</a>--}}
    {{--</div>--}}
    {{--<div class="option-item two columns">--}}
        {{--<a class="active" href="#">Česta pitanja</a>--}}
    {{--</div>--}}
    {{--<div class="option-item two columns">--}}
        {{--<a href="{{ URL::route('company.feedback.management') }}">Prakse kod nas</a>--}}
    {{--</div>--}}
    @if(isset($btn) && $btn['has'])
        <div class="pull-right">
            <button class="btn btn-success icon" onclick="location.href='{{ URL::route($btn['route']) }}'">
                @if($btn['icon'])
                <span class="icon-plus"></span> {{ $btn['title'] }}
                @else
                    {{ $btn['title'] }}
                @endif
            </button>
        </div>
    @endif
</div>