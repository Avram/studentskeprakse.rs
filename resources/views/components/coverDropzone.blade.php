<form id="drop-zone-form" action="/" method="POST">
    <div class="twelve columns" id="cover-drop-zone">
        <div class="icon-images">
            <div>Prevuci fotografiju</div>
        </div>
        <input type="file" id="{{ $name }}" name="{{ $name }}" accept="image/jpeg,image/png,image/gif"/>
    </div>
</form>

@section('js')
    @parent
    <script>
        $(document).ready(function () {
            var fd = new FormData();

            $('#{{ $name }}').bind('dragenter', function () {
                $(this).parent().addClass('dragenter');
                $('.icon-images div').text('Ispusti fotografiju!');
            });

            $('#{{ $name }}').bind('dragleave', function () {
                $('.icon-images div').text('Prevuci fotografiju');
                $(this).parent().removeClass('dragenter');
            });

            $('#{{ $name }}').on('change', function () {
                if ($(this).val() !== "") {
                    fd.append('file', $('#{{ $name }}')[0].files[0]);
                    fd.append('_token', "{{ csrf_token() }}");
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                            $result = JSON.parse(xhr.responseText);
                            if ($result.status === "OK") {
                                $('.cover-image > img').attr({
                                    'src': $result.image
                                });
                                $('.icon-images div').text('Prevuci fotografiju');
                                $('#{{ $name }}').parent().removeClass('dragenter');
                            } else {
                                $('.icon-images div').text('Prevuci fotografiju');
                                $('#{{ $name }}').parent().removeClass('dragenter');
                                alert($result.message);
                            }
                        } else if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 422) {
                            flashError('Ovaj tip fajla nije dozvoljen!');
                        }
                    };
                    xhr.open("POST", "{{ URL::route('ajax.user.uploadCover') }}");
                    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                    xhr.send(fd);
                }
            });
        });
    </script>
@endsection