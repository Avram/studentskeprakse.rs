<div class="full-width-tabs">
    <ul class="nav nav-tabs">
        @if(isset($settings) && $settings)
            @foreach($items as $item)
                <li class="@if(Request::url() === URL::route('company.'.$item['route'], $item['routeParams'])) active @endif">
                    @if(isset($item['routeParams']))
                        <a href="{{ URL::route('company.'.$item['route'], $item['routeParams']) }}">{{ $item['title'] }}</a>
                    @else
                        <a href="{{ URL::route('company.'.$item['route']) }}">{{ $item['title'] }}</a>
                    @endif
                </li>
            @endforeach
        @else
            @foreach($items as $item)
                {{--<li id="tab-{{$item['route']}}" class="internship-tab @if($item['active']) active @endif">--}}
                <li id="tab-{{$item['route']}}" class="internship-tab @if($item['active']) active @endif">
                    @if(isset($item['routeParams']))
                        <a href="#{{ $item['route'] }}" onclick="show('{{$item['route']}}')">{{ $item['title'] }}</a>
                    @else
                        {{--<a href="{{ URL::route('company.'.$item['route']) }}">{{ $item['title'] }}</a>--}}
                        <a href="#{{ $item['route'] }}" onclick="show('{{$item['route']}}')">{{ $item['title'] }}</a>
                    @endif
                </li>
            @endforeach
        @endif
    </ul>
</div>

@section('js')
    @parent
    <script>
        function show(index) {
            var tabs = document.getElementsByClassName('tab-wrapper');
            var tabsLen = tabs.length;
            for (var i=0; i< tabsLen; i++) {
                tabs[i].style.display=''
            }
            document.getElementById('tab-wrapper-'+index).style.display='inline-block';

            document.getElementsByClassName('internship-tab active')[0].className = 'internship-tab';
            document.getElementById('tab-'+index).className = 'internship-tab active'
        }

        $(document).ready(function() {
            $tabs = $("li[id^=tab-]");
            hash = location.hash;
            if(hash !== '') {
                show(hash.replace(/#/, ''));
                    hash = '';
            }

            window.onhashchange = function () {
                $hash = location.hash;
                console.log($hash);

                $tabs.each(function() {
                    if($(this).children('a').attr('href') === $hash){
                        show($hash.replace(/#/, ''));
                    }
                });

            };
        });
    </script>
@endsection