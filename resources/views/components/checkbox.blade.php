<div class="row form-element">
    @include('components.checkboxInline', ['id' => $id, 'label' => $label, 'name' => $name, 'value' => $value, 'class' => $class, 'checked' => $checked])
</div>