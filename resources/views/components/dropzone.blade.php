<form id="drop-zone-form" action="/" method="POST">
    <div class="twelve columns" id="drop-zone">
        <div class="icon-images">
            <div>Prevuci fotografiju</div>
        </div>
        <input type="file" id="{{ $name }}" name="{{ $name }}" accept="image/jpeg,image/png,image/gif"/>
    </div>
</form>

@section('js')
    @parent
    <script>
        $(document).ready(function () {
            $input = $('#{{ $name }}');
            var fd = new FormData();

            $('#{{ $name }}').bind('dragenter', function () {
                $(this).parent().addClass('dragenter');
                $('.icon-images div').text('Ispusti fotografiju!');
            });

            $('#{{ $name }}').bind('dragleave', function () {
                $('.icon-images div').text('Prevuci fotografiju');
                $(this).parent().removeClass('dragenter');
            });


            $('#{{ $name }}').on('change', function () {
                console.log('asd');
                if ($(this).val() !== "") {
                    fd.append('file', $('#{{ $name }}')[0].files[0]);
                    fd.append('_token', "{{ csrf_token() }}");
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                            $result = JSON.parse(xhr.responseText);
                            if ($result.status === "OK") {
                                $image = $('<div></div>').attr({
                                    'class': 'office-img four columns'
                                }).data('id', $result.id).append(
                                        $('<a></a>').attr({
                                            'href': $result.image
                                        }).append(
                                                $('<img>').attr({
                                                    'src': $result.thumb
                                                })
                                        )
                                ).append(
                                        $('<div></div>').attr({
                                            'class': 'img-options'
                                        }).append(
                                                $('<span></span>').attr({
                                                    'data-id': $result.id,
                                                    'onclick': 'deleteImage($(this));'
                                                }).css({
                                                    'cursor': 'pointer'
                                                }).html("&times;")
                                        )
                                );
                                $('#images').append($image);
                                $('.icon-images div').text('Prevuci fotografiju');
                                $('#{{ $name }}').parent().removeClass('dragenter');
                            } else {
                                $('.icon-images div').text('Prevuci fotografiju');
                                $('#{{ $name }}').parent().removeClass('dragenter');
                                vex.dialog.alert($result.message);
                            }
                        } else if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 422) {
                            flashError('Ovaj tip fajla nije dozvoljen!');
                        }
                    };
                    xhr.open("POST", "{{ URL::route('ajax.offices.uploadImage') }}");
                    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                    xhr.send(fd);
                }
            });
        });

        function deleteImage($element) {
            var id = $element.attr('data-id');

            vex.dialog.confirm({
                message: "Da li ste sigurni da želite da obrišete ovu fotografiju?",
                callback: function (value) {
                    if (value) {
                        var fd = new FormData();
                        var xhr = new XMLHttpRequest();
                        fd.append('_token', "{{ csrf_token() }}");
                        fd.append('id', id);
                        xhr.onreadystatechange = function () {
                            if (xhr.readyState == XMLHttpRequest.DONE) {
                                $result = JSON.parse(xhr.responseText);
                                if ($result.status === "OK") {
                                    $element.parent().parent().fadeOut(function () {
                                        $(this).remove();
                                    });
                                }
                            }
                        };

                        xhr.open("POST", "{{ URL::route('ajax.offices.removeImage') }}");
                        xhr.send(fd);
                    } else {
                        return;
                    }
                }
            });
        }
    </script>
@endsection