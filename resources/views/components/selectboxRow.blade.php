<div class="row twelve columns form-element inner-addon left-addon">
    {{--@if(isset($default))--}}
    {{--@include('components/selectboxInline', ['columns' => isset($columns) ? $columns : 'twelve', 'name' => $name, 'default' => $default, 'items' => $items, 'multiple' => $multiple])--}}
    {{--@else--}}
    {{--@include('components/selectboxInline', ['columns' => isset($columns) ? $columns : 'twelve', 'name' => $name, 'items' => $items, 'multiple' => $multiple])--}}
    {{--@endif--}}
    @if(isset($label))
        <label for="{{ $misc['id'] }}">{{ $label }}</label>
    @endif

    @include('components.selectboxInline', [
        'attrs' => isset($attrs) ? $attrs : null,
        'class' => isset($class) ? $class : null,
        'columns' => isset($columns) ? $columns : 'twelve',
        'default' => isset($default) ? $default : null,
        'items' => $items,
        'multiple' => isset($multiple) ? $multiple : null,
        'name' => $name,
        'selected' => isset($selected) ? $selected : null
    ])
</div>
{{--@include('components.selectboxInline', [--}}
{{--'attrs' => isset($attrs) ? $attrs : null,--}}
{{--'class' => isset($class) ? $class : null,--}}
{{--'columns' => isset($columns) ? $columns : 'twelve',--}}
{{--'default' => isset($default) ? $default : null,--}}
{{--'items' => $items,--}}
{{--'multiple' => isset($multiple) ? $multiple : null,--}}
{{--'name' => $name,--}}
{{--'selected' => isset($selected) ? $selected : null--}}
{{--])--}}