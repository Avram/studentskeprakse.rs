@if(isset($form) && !$form)
@else
<form id="logo-drop-zone-form" action="/" method="POST">
@endif
    <div class="twelve columns" id="logo-drop-zone">
        <div class="icon-images">
        </div>
        <input type="file" id="{{ $name }}" name="{{ $name }}" accept="image/jpeg,image/png,image/gif"/>
    </div>
@if(isset($form) && !$form)
@else
</form>
@endif

@if(isset($js) && !$js)
@else
@section('js')
    @parent
    <script>
        $(document).ready(function () {
            $input = $('#{{ $name }}');
            var fd = new FormData();

            $('#{{ $name }}').on('change', function () {
                if ($(this).val() !== "") {
                    fd.append('file', $('#{{ $name }}')[0].files[0]);
                    fd.append('_token', "{{ csrf_token() }}");
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                            $result = JSON.parse(xhr.responseText);
                            console.log($result);
                            if ($result.status === "OK") {
                                $('.company-logo > img').attr({
                                    'src': $result.image
                                });
                            }
                        } else if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 422) {
                            flashError('Ovaj tip fajla nije dozvoljen!');
                        }
                    };
                    xhr.open("POST", "{{ URL::route('ajax.user.uploadLogo') }}");
                    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                    xhr.send(fd);
                }
            });
        });
    </script>
@endsection
@endif