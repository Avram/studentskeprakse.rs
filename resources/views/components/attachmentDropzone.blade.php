<div class="nine columns" id="attachment-drop-zone">
    <div class="icon-images">
        <span>Prevuci fajl</span>
    </div>
    <input type="file" id="{{ $name }}" name="{{ $name }}" accept="image/jpeg,image/png,image/gif,application/pdf"/>
</div>

@section('js')
    @parent
    <script>
        $(document).ready(function () {
            $('#{{ $name }}').bind('dragenter', function () {
                $(this).parent().addClass('dragenter');
                $('.icon-images span').text('Ispusti fajl!');
            });

            $('#{{ $name }}').bind('dragleave', function () {
                $('.icon-images span').text('Prevuci fajl');
                $(this).parent().removeClass('dragenter');
            });


            $('#{{ $name }}').on('change', function () {
                if ($(this).val() !== "") {
                    $('.icon-images span').text($(this).val());
                    $('#{{ $name }}').parent().removeClass('dragenter');
                }
            });
        });
    </script>
@endsection