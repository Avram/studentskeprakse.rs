<select class="{{ $columns }} columns {{ $errors->has($name) ? ' has-error' : '' }} @if(isset($class)) {{ $class }} @endif"
        name="{{ $name }}" @if(isset($attrs)) {{ $attrs }} @endif>
    @if(isset($default))
        <option selected disabled>{{ $default }}</option>
    @endif

    @if(isset($multiple))
        @if(isset($selected))
            @foreach($items as $key => $title)
                <option @if(in_array($key, $selected)) selected @endif value="{{ $key }}">{{ $title }}</option>
            @endforeach
        @endif
    @else
        @foreach($items as $key => $title)
            <option @if(isset($selected) && ($selected === $key)) selected
                    @endif value="{{ $key }}">{{ $title }}</option>
        @endforeach
    @endif
</select>