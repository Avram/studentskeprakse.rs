<div class="custom-checkbox @if(isset($class)) {{ $class }} @endif">
    <input name="{{ isset($name) ? $name : $id }}" type="checkbox" class="checkbox" @if(isset($dataId))data-id="{{ $dataId }}"@endif id="{{ $id }}"
           value="{{ isset($value) ? $value : 'on' }}" @if(isset($checked) && $checked) checked @endif/>
    <label for="{{ $id }}">{{ $label }}</label>
</div>