@extends('layouts.app')

@section('content')

    <form method="POST" action="">
        {!! csrf_field() !!}
        <input type="text" name="name" value="{{ $category->getName() }}">
        <input type="text" name="order" value="{{ $category->getOrder() }}">
        <input type="text" name="parent_id" value="{{ $category->parent_id }}">
        <input type="submit" value="Submit">
    </form>

@endsection