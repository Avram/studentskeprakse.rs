@extends('layouts.app')

@section('content')

    <ul>
        @foreach($categories as $category)
            <li> {{ $category->name  }} |<a href="{{ URL::route('admin.category.update', [$category->id]) }}"><small>(update)</small></a> <a href="{{ URL::route('admin.category.delete', [$category->id]) }}"><small>(delete)</small></a> </li>
        @endforeach

    </ul>

@endsection