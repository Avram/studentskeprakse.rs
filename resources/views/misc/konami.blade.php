<div id="konami"
     style="z-index: 9999999999; display:none; position: fixed; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0,0,0,.4); text-align: center; vertical-align: middle">
    <img src="http://labs.devana.rs/wp-content/uploads/2016/02/labs_devana_featured-1.jpg"/>
</div>

<script>
    var konamiCode = function (combination, callback) {
        var lastCorrectInput = -1,
                isActive = 0,
                o = {};
        if (typeof combination === "function") {
            callback = combination;
        }
        if (Object.prototype.toString.call(combination) !== "[object Array]") {
            combination = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65];
        }
        o.start = function () {
            if (isActive) {
                return;
            }
            isActive = 1;
            document.onkeyup = function (e) {
                var code;
                if (!isActive) {
                    return;
                }
                code = window.event ? window.event.keyCode : e.which;
                if (combination[++lastCorrectInput] === code) {
                    if (lastCorrectInput === combination.length - 1) {
                        if (callback && typeof(callback) === "function") {
                            callback();
                        }
                    }
                    return;
                }
                lastCorrectInput = -1;
            };
            return o;
        };
        o.stop = function () {
            isActive = 0;
            return o;
        };
        return o;
    };

    konamiCode(function () {
        $('body').css('overflow', 'hidden');
        console.log($('#konami').height() / 2 - 300);
        $('#konami > img').css('position', 'absolute')
                .css('top', ($('#konami').height() / 2 - 300) + 'px')
                .css('left', ($('#konami').width() / 2 - 430) + 'px');
        $('#konami').fadeIn().append('<iframe id="ytvid" width="0" height="0" src="https://www.youtube.com/embed/CGvGdOjnTIA?autoplay=1" frameborder="0" allowfullscreen></iframe>');
        $('*').on('click', function () {
            $('#konami').fadeOut();
            $('body').css('overflow', 'visible');
            $('#ytvid').remove();
        });
    }).start();


</script>