@extends('layouts.app')

@section('content')
    <div class="container">
        @include('components.managementNav', [
            'items' => [
                [
                    'title' => 'Prakse',
                    'active' => false,
                    'route' => 'company.internships.management'
                ],
                [
                    'title' => 'Mentori',
                    'active' => false,
                    'route' => 'company.mentors.management'
                ],
                [
                    'title' => 'Česta pitanja',
                    'active' => false,
                    'route' => 'company.faq.management'
                ],
                [
                    'title' => 'Prakse kod nas',
                    'active' => false,
                    'route' => 'company.feedback.management'
                ],
                [
                    'title' => 'Obrisano',
                    'active' => true,
                    'route' => 'company.internships.trash'
                ]
            ],
            'btn' => [
                'has' => false,
                'route' => 'internship.create',
                'icon' => true,
                'title' => 'Napravi'
            ]
        ])
        <div class="row twelve columns">
            @foreach($internships as $internship)
                @include('internship.trashListItem', [
                    'user' => $user->slug,
                    'slug' => $internship->slug,
                    'title' => $internship->name,
                    'city' => $internship->city->name,
                    'id' => $internship->id,
                ])
            @endforeach
        </div>
    </div>
@endsection