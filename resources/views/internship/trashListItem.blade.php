<div class="panel item">
    <div class="row header">
        <h3 class="pull-left">
            <a href="{{ URL::route('company.internship', [$user, $slug]) }}">{{ $title }}</a>
        </h3>
        <div class="company pull-right">
        </div>
    </div>
    <div class="row meta">
        <a class="city pull-left" href="#">{{ $city }}</a>
        <div class="options pull-right">
            <a href="{{ URL::route('internship.softRestore', [$slug]) }}"><span class="icon-reply"></span></a>
            <a href="{{ URL::route('internship.delete', [$slug]) }}"><span class="icon-trash"></span></a>
        </div>
    </div>
</div>