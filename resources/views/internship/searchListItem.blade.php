<a href="{{ URL::route('company.internship', [$user, $slug]) }}">
    <div class="panel item">
        <div class="logo pull-left">
            <img src="{{ $imagesource }}" alt="" class="logo-image">
        </div>
        <div class="header pull-left">
            <span class="company">{{ $company }}</span>
            <br>
            <span class="title" href="#">{{ $title }}</span>
        </div>
        <div class="pull-right location icon icon-pin">
            <span>{{ $city }}</span>
        </div>
    </div>
</a>