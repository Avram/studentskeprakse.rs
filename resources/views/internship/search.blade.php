@extends('layouts.app')

@section('content')
    <div class="row twelve columns">
        @include('internship/searchSidebar', ['categories' => $searchCategories])
        @include('internship/searchList', ['internships' => $internships])
    </div>
@endsection

@section('js')
    @parent
    <script>
        Array.prototype.remove = function () {
            var what, a = arguments, L = a.length, ax;
            while (L && this.length) {
                what = a[--L];
                while ((ax = this.indexOf(what)) !== -1) {
                    this.splice(ax, 1);
                }
            }
            return this;
        };

        $(document).ready(function () {
            var searchData = {
//                misc: [],
                categories: [],
                cities: [],
                term: '',
                sort: '',
                offset: 0
            };

            $('#loadMore').on('click', function () {
                searchData.offset = $('.list-wrapper').children().length;
                requestSearch(false);
            });

            $("input[type='checkbox']").on('change', function () {
                $element = $(this);

                if ($element.is(':checked')) {
                    if ($element.attr('id').indexOf("category-") >= 0) {
                        searchData.categories.push($element.data('id'));
                    } else if ($element.attr('id').indexOf("city-") >= 0) {
                        searchData.cities.push($element.data('id'));
                    } else {
                        searchData.misc.push($element.attr('id'));
                    }
                } else {
                    if ($element.attr('id').indexOf("category-") >= 0) {
                        searchData.categories.remove($element.data('id'));
                    } else if ($element.attr('id').indexOf("city-") >= 0) {
                        searchData.cities.remove($element.data('id'));
                    } else {
                        searchData.misc.remove($element.attr('id'));
                    }
                }

                requestSearch();
            });

            $("select").on('change', function () {
                $element = $(this);
                if ($element.val() !== searchData.sort) {
                    searchData.sort = $element.val();
                    requestSearch();
                }
            });

            $("input.search.search-input").on('keyup', function (e) {
                $element = $(this);
                if (e.keyCode === 13) {
                    if ($element.val() !== searchData.term) {
                        searchData.term = $element.val();
                        requestSearch();
                    }
                }
            });

            $("input.search.search-input").on('focusout', function () {
                $element = $(this);
                if ($element.val() !== searchData.term) {
                    searchData.term = $element.val();
                    requestSearch();
                }
            });

            function requestSearch(clear) {
                if(clear === undefined) { clear = true; searchData.offset = 0;}

                var xhr = new XMLHttpRequest();
                var fd = new FormData();

                fd.append('_token', '{{ csrf_token() }}');
                fd.append('searchData', JSON.stringify(searchData));

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == XMLHttpRequest.DONE) {
                        $response = JSON.parse(xhr.responseText);

                        if($response.length < 10) {
                            $('#loadMore').hide();
                        } else {
                            $('#loadMore').show();
                        }

                        $list = $('.list-wrapper');
                        if(clear) {
                            $list.empty();
                        }

                        $($response).each(function () {
                            $this = this;

                            $item = $('<a></a>').attr({href: $this.slug}).append(
                                    $('<div></div>').attr({class: 'panel item'}).append(
                                            $('<div></div>').attr({class: 'logo pull-left'}).append(
                                                    $('<img>').attr({
                                                        src: $this.user.logo,
                                                        alt: '',
                                                        class: 'logo-image'
                                                    })
                                            )
                                    ).append(
                                            $('<div></div>').attr({class: 'header pull-left'}).append(
                                                    $('<span></span>').attr({class: 'company'}).text($this.user.name)
                                            ).append(
                                                    $('<br>')
                                            ).append(
                                                    $('<span></span>').attr({class: 'title'}).text($this.name)
                                            )
                                    ).append(
                                            $('<div></div>').attr({class: 'location pull-right'}).append(
                                                    $('<span></span>').text($this.city.name)
                                            )
                                    )
                            );
                            $list.append($item);
                            console.log($item);
                        });
                    }
                };
                xhr.open("POST", '{{ URL::route('home.search') }}');
                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                xhr.send(fd);
            }
        });
    </script>
@endsection