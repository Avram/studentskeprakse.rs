@extends('layouts.app')

@section('content')
    <div class="container">
        @include('components.managementNav', [
            'items' => [

                [
                    'title' => 'Prakse',
                    'active' => true,
                    'route' => 'company.internships.management'
                ],
                [
                    'title' => 'Mentori',
                    'active' => false,
                    'route' => 'company.mentors.management'
                ],
                [
                    'title' => 'Česta pitanja',
                    'active' => false,
                    'route' => 'company.faq.management'
                ],
                [
                    'title' => 'Prakse kod nas',
                    'active' => false,
                    'route' => 'company.feedback.management'
                ],
                [
                    'title' => 'Obrisano',
                    'active' => false,
                    'route' => 'company.internships.trash'
                ]
            ],
            'btn' => [
                'has' => true,
                'route' => 'internship.create',
                'icon' => true,
                'title' => 'Postavi'
            ]
        ])
        <div class="row twelve columns">
            @foreach($internships as $internship)
                @include('internship.managementListItem', [
                    'user' => $user->slug,
                    'slug' => $internship->slug,
                    'title' => $internship->name,
                    'city' => $internship->city->name,
                    'id' => $internship->id,
                    'status' => $internship->status
                ])
            @endforeach
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script>
        $(document).ready(function () {
            $('.onoffswitch-checkbox').on('change', function () {
                $id = $(this).data('id');

                var xhr = new XMLHttpRequest();
                var fd = new FormData();
                fd.append('_token', '{{ csrf_token() }}');
                fd.append('id', $id);

                xhr.open('POST', '{{ URL::route('ajax.internship.toggleStatus') }}');
                xhr.send(fd);
            });
        });
    </script>
@endsection