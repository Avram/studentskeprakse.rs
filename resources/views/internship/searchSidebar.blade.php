<div class="row three columns">
    {{--<div class="twelve columns filter panel">--}}
        {{--@foreach($searchFilters as $filter)--}}
            {{--@include('components.checkbox', $filter)--}}
        {{--@endforeach--}}
    {{--</div>--}}
    {{--<div class="row cutouts twelve columns">--}}
        {{--<div class="cut-out left"></div>--}}
        {{--<div class="cut-out right"></div>--}}
    {{--</div>--}}
    {{--<div class="twelve columns filter panel">--}}
        {{--@foreach($searchCategories as $category)--}}
            {{--@include('components.checkbox', [--}}
                {{--'id' => 'category-'.$category->id,--}}
                {{--'dataId' => $category->id,--}}
                {{--'label' => $category->name,--}}
                {{--'name' => 'category[]',--}}
                {{--'value' => $category->id,--}}
                {{--'class' => '',--}}
                {{--'checked' => '',--}}
            {{--])--}}
        {{--@endforeach--}}
    {{--</div>--}}

    <div class="twelve columns filter form-element inner-addon right-addon">
        <span class="icon icon-search"></span>
        <input type="text" class="twelve columns icon search search-input" placeholder="Traži"/>
    </div>

    {{--<div class="twelve columns filter-panel filter">--}}
        {{--Grad--}}
        {{--<span class="icon icon-pin pull-right"></span>--}}
    {{--</div>--}}
    <div class="twelve columns filter form-element inner-addon right-addon">
        <span class="icon icon-pin"></span>
        <input type="text" class="twelve columns icon search search-city" placeholder="Grad"/>
    </div>
    <div class="twelve columns filter-panel scrollable">
        @foreach($searchCities as $city)
            <div class="twelve columns filter-checkbox">
                @include('components.checkbox', [
                    'id' => 'city-'.$city->id,
                    'dataId' => $city->id,
                    'label' => $city->name,
                    'name' => 'city[]',
                    'value' => $city->id,
                    'class' => '',
                    'checked' => '',
                ])
            </div>
        @endforeach
    </div>

    {{--<div class="twelve columns filter-panel categories filter">--}}
        {{--Kategorije--}}
    {{--</div>--}}
    <div class="twelve columns filter form-element">
        <input type="text" class="twelve columns search search-category" placeholder="Kategorije"/>
    </div>
    <div class="twelve columns filter-panel scrollable">
        @foreach($categories as $category)
            <div class="twelve columns filter-checkbox">
                @include('components.checkbox', [
                    'id' => 'category-'.$category->id,
                    'dataId' => $category->id,
                    'label' => $category->name,
                    'name' => 'category[]',
                    'value' => $category->id,
                    'class' => '',
                    'checked' => '',
                ])
            </div>
        @endforeach
    </div>
</div>
@section('js')
    @parent

    <script>
        $(document).ready(function () {
            $cityInput = $('.search-city');
            $categoryInput = $('.search-category');
            $cityItems = $('input[id^=city-]');
            $categoryItems = $('input[id^=category-]');
            $cityValues = {};
            $categoryValues = {};

            $cityItems.each(function() {
                $item = $(this);
                $value = $item.siblings().text();
                $id = $item.data('id');
                $cityValues[$id] = $value;
            });
            $categoryItems.each(function() {
                $item = $(this);
                $value = $item.siblings().text();
                $id = $item.data('id');
                $categoryValues[$id] = $value;
            });

            $cityInput.on('keyup', function (e) {
                showSearched($cityItems, $cityInput.val(), $cityValues);
            });

            $cityInput.on('focusout', function () {
                showSearched($cityItems, $cityInput.val(), $cityValues);
            });

            $categoryInput.on('keyup', function (e) {
                showSearched($categoryItems, $categoryInput.val(), $categoryValues);
            });

            $categoryInput.on('focusout', function () {
                showSearched($categoryItems, $categoryInput.val(), $categoryValues);
            });

            function showSearched(items, term, values) {
                items.each(function () { $(this).parent().parent().parent().hide(); });
                items.each(function () {
                    $item = $(this);
                    $value = values[$item.data('id')];
                    if($value.toLowerCase().indexOf(term.toLowerCase()) >= 0) {
                        $item.parent().parent().parent().show();
                    }
                });
            }
        });
    </script>
@endsection