<div class="eight columns panel content">
    <div class="twelve columns">
        <div class="full-width-tabs">
            <ul class="nav nav-tabs">
                <li>
                    <a href="{{ URL::route('company.internships', ['username' => $user->slug]) }}">Prakse</a>
                </li>
                <li>
                    <a href="{{ URL::route('company.about', ['username' => $user->slug]) }}">O nama</a>
                </li>
                <li>
                    <a href="{{ URL::route('company.faqs', ['username' => $user->slug]) }}">Praksa kod nas</a>
                </li>
                <li>
                    <a href="{{ URL::route('company.offices', ['username' => $user->slug]) }}">Kancelarije</a>
                </li>
                <li>
                    <a href="{{ URL::route('company.attachments', ['username' => $user->slug]) }}">Prilozi</a>
                </li>
                <li>
                    <a href="{{ URL::route('company.faqs', ['username' => $user->slug]) }}">Česta pitanja</a>
                </li>
            </ul>
        </div>
        <div class="main-content">
            <h3 class="internship-title">{{ $internship->name }}</h3>
            <p class="internship-description">
                {{ $internship->content }}
            </p>
            <div class="meta-info">
                <div class="meta-item">
                    <div class="icon-calendar meta-icon"></div>
                    <div class="meta-value">{{ $internship->duration }}</div>
                </div>
                <div class="meta-item">
                    <div class="icon-watch meta-icon"></div>
                    <div class="meta-value">{{ $internship->work_hours }}</div>
                </div>
                <div class="meta-item">
                    <div class="icon-dollar meta-icon"></div>
                    <div class="meta-value">{{ $internship->salary }}</div>
                </div>
                <div class="meta-item">
                    <div class="icon-person meta-icon"></div>
                    <div class="meta-value">
                        @foreach($internship->mentors as $mentor)
                            {{ $mentor->name }}<br/>
                        @endforeach
                    </div>
                </div>
                <div class="meta-item">
                    <div class="icon-bowl meta-icon"></div>
                    <div class="meta-value">{{ $internship->food }}</div>
                </div>
            </div>
            <div class="buttons mail">
                <div class="row twelve columns">
                    <button class="btn btn-info">Prijavi se</button>
                </div>
            </div>
        </div>
    </div>
</div>