<div class="panel item">
    <div class="row header">
        <h3 class="pull-left">
            <a href="{{ URL::route('company.internship', [$user, $slug]) }}">{{ $title }}</a>
        </h3>
        <div class="company pull-right">
            <span id="status"></span>
            <div class="onoffswitch">
                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" data-id="{{ $id }}" id="switch-{{ $id }}" @if($status == \StudentskePrakse\Model\Internship::STATUS_PUBLISHED) checked @endif >
                <label class="onoffswitch-label" for="switch-{{ $id }}">
                    <span class="onoffswitch-inner"></span>
                    <span class="onoffswitch-switch"></span>
                </label>
            </div>
        </div>
    </div>
    <div class="row meta">
        <a class="city pull-left" href="#">{{ $city }}</a>
        <div class="options pull-right">
            <a href="{{ URL::route('internship.duplicate', [$slug]) }}"><span class="icon-plus"></span></a>
            <a href="{{ URL::route('internship.softDelete', [$slug]) }}"><span class="icon-trash"></span></a>
            <a href="{{ URL::route('company.internship.edit', [$user, $slug]) }}"><span class="icon-pen"></span></a>
            <span class="icon-seen">
                <span class="tooltip">1234 pregleda</span>
            </span>
        </div>
    </div>
</div>