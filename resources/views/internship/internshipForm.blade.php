@extends('layouts.app')

@section('content')
    <div class="container panel internship-create">
        <form class="internship-form" method="POST" action="">
            {!! csrf_field() !!}

            <div class="row"><!-- Internship Title -->
                <div class="eleven columns form-element">
                    <label for="name">Naziv prakse</label>
                    <input type="text" class="twelve columns {{ $errors->has('name') ? ' has-error' : '' }}" name="name"
                           value="@if($errors->any()){{ old('name') }}@else{{ $internship->getName() }}@endif"
                           placeholder="Naslov prakse" required="required">
                </div>
            </div><!-- Internship Title End -->

            <div class="row">
                <div class="eleven columns form-element"><!-- Internship Category -->
                    <label for="category_id">Kategorija</label>
                    @include('components.selectboxInline', [
                        'columns' => 'twelve category',
                        'name' => 'category_id',
                        'default' => 'Kategorija',
                        'items' => $categories,
                        'selected' => $selected_category
                    ])
                </div><!-- Internship Category End-->

            </div>

            <div class="row"><!-- Internship Description -->
                <div class="eleven columns form-element">
                    <label for="content">Opis</label>
                    <textarea class="twelve columns{{ $errors->has('content') ? ' has-error' : '' }}" placeholder="Opis"
                              name="content"
                              required="required">@if($errors->any()){{ old('content') }}@else{{ $internship->getContent() }}@endif</textarea>
                </div>
            </div><!-- Internship Description End -->

            <div class="row"><!-- Internship Skills -->
                <div class="eleven columns form-element">
                    <label for="content">Potrebne veštine</label>
                    <textarea class="twelve columns" placeholder="Potrebne veštine"
                              name="required_skills">@if($errors->any()){{ old('required_skills') }}@else{{ $internship->getRequiredSkills() }}@endif</textarea>
                </div>
            </div><!-- Internship Skills End -->

            <div class="row">

                <div class="six columns form-element joined-elements">
                    <label for="work_dwm">Trajanje</label>
                    <input type="text" class="six columns joined-left" name="duration"
                           value="{{ $internship->getDuration() }}" placeholder="Broj">
                    @include('components.selectboxInline', [
                        'columns' => 'five',
                        'class' => 'joined-right',
                        'name' => 'work_dwm',
                        'items' => [
                            'days' => 'Dana',
                            'weeks' => 'Nedelja',
                            'months' => 'Meseci'
                        ],
                        'selected' => $internship->getDurationType()
                    ])
                </div>
                <div class="five columns form-element">
                    <label for="work_hours">Nedeljno</label>
                    <input type="text" class="twelve columns {{ $errors->has('work_hours') ? ' has-error' : '' }}"
                           name="work_hours"
                           value="{{ $internship->getWorkHours() }}"
                           placeholder="Broj radnih sati nedeljno">
                </div>
            </div>
            <div class="row">

                <div class="six columns form-element">
                    <label for="content">Plata</label>
                    <input type="text" id="salary" name="salary" class="twelve columns pull-left"
                           placeholder="Plata (RSD)"
                           value="{{ $internship->getSalary() }}"/>
                </div>
                <div class="five columns form-element">
                    <label for="content">Mentori</label>
                    @include('components.selectboxInline', [
                        'columns' => 'twelve',
                        'name' => 'mentors[]',
                        'class' => 'multi-mentors',
                        'attrs' => 'multiple="multiple"',
                        'items' => $mentors,
                        'selected' => $selected_mentors,
                        'multiple' => true
                    ])
                </div>
            </div>
            <div class="row food">
                <div class="five columns form-element"><!-- Internship City -->
                    <label for="city_id">Grad</label>
                    @include('components.selectboxInline', [
                        'columns' => 'twelve city',
                        'name' => 'city_id',
                        'default' => 'Grad',
                        'items' => $cities,
                        'selected' => $selected_city
                    ])
                </div><!-- Internship City End -->
                <div class="foodCheckboxes">
                    <div class="two columns">
                        @include('components.checkboxInline', ['id' => 'breakfast', 'label' => 'Doručak', 'name' => 'food[]', 'value' => 'breakfast', 'checked' => $internship->hasMeal('breakfast') ])
                    </div>
                    <div class="two columns">
                        @include('components.checkboxInline', ['id' => 'lunch', 'label' => 'Ručak', 'name' => 'food[]', 'value' => 'lunch', 'checked' => $internship->hasMeal('lunch') ])
                    </div>
                    <div class="two columns">
                        @include('components.checkboxInline', ['id' => 'dinner', 'label' => 'Večera', 'name' => 'food[]', 'value' => 'dinner', 'checked' => $internship->hasMeal('dinner') ])
                    </div>
                </div>
            </div>
            <div class="row submit">
                <button type="submit" class="two columns btn btn-info">Postavi</button>
            </div>
        </form>
        @include('errors.formErrors', ['errors' => $errors])
    </div>
@endsection


@section('css')
    <link href="{{ asset('build/css/select2.min.css') }}" rel="stylesheet">
@endsection
@section('js')
    <script src="{{ asset('build/js/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".multi-mentors").select2();
            $(".category").select2();
            $(".city").select2();

            $("#none").change(function () {
                if ($(this).is(':checked')) {
                    $("#breakfast").attr('disabled', true).attr('checked', false);
                    $("#lunch").attr('disabled', true).attr('checked', false);
                    $("#dinner").attr('disabled', true).attr('checked', false);
                } else {
                    $("#breakfast").attr('disabled', false);
                    $("#lunch").attr('disabled', false);
                    $("#dinner").attr('disabled', false);
                }
            });

            $('#notpaid').change(function () {
                if ($(this).is(':checked')) {
                    $('#salary').val('').attr('disabled', true).addClass('disabled');
                } else {
                    $('#salary').attr('disabled', false).removeClass('disabled');
                }
            });
        });
    </script>
@endsection