@extends('layouts.app')

@section('content')

    @foreach($internships as $internship)
        <div>
            <h1><a href="{{ URL::route('admin.internship.single', [$internship->id]) }}">{{ $internship->name }}</a>
            </h1>
            <p>
                {{ $internship->content }}
            </p>
            <span>actions: <a href="{{ URL::route('admin.internship.update', [$internship->id]) }}">
                    <small>(update)</small>
                </a> | <a href="{{ URL::route('admin.internship.delete', [$internship->id]) }}">
                    <small>(delete)</small>
                </a></span>
        </div>
        <hr />
    @endforeach

@endsection