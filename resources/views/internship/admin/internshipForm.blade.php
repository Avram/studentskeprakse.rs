@extends('layouts.app')

@section('content')

    <form method="POST" action="">
        {!! csrf_field() !!}
        @include('components/selectboxRow', [
            'items' => $users,
            'name' => 'user_id'
        ])

        {{--<select name="user_id">--}}
        {{--@foreach($users as $user)--}}
        {{--<option @if($internship->user_id === $user->id) selected @endif value="{{ $user->id }}" >{{ $user->getName() }}</option>--}}
        {{--@endforeach--}}
        {{--</select>--}}

        <br/>

        <input type="text" name="name" value="{{ $internship->getName() }}"><br />
        <textarea name="content">{{ $internship->getContent() }}</textarea><br />
        <textarea name="required_skills">{{ $internship->getRequiredSkills() }}</textarea><br />
        <input type="text" name="duration" value="{{ $internship->getDuration() }}"><br />
        <input type="text" name="salary" value="{{ $internship->getSalary() }}"><br />

        {{--@include('components/checkbox', ['id' => 'food1', 'label' => 'Doručak', 'name' => 'food[]', 'value' => 'breakfast', 'checked' => $internship->hasMeal('breakfast') ])--}}
        {{--@include('components/checkbox', ['id' => 'food2', 'label' => 'Ručak', 'name' => 'food[]', 'value' => 'lunch', 'checked' => $internship->hasMeal('lunch') ])--}}
        {{--@include('components/checkbox', ['id' => 'food3', 'label' => 'Večera', 'name' => 'food[]', 'value' => 'dinner', 'checked' => $internship->hasMeal('dinner') ])--}}


        <div class="three columns">
            @include('components.checkboxInline', ['id' => 'brekfast', 'label' => 'Doručak', 'name' => 'food[]', 'value' => 'breakfast', 'checked' => $internship->hasMeal('breakfast') ])
        </div>
        <div class="three columns">
            @include('components.checkboxInline', ['id' => 'lunch', 'label' => 'Ručak', 'name' => 'food[]', 'value' => 'lunch', 'checked' => $internship->hasMeal('lunch') ])
        </div>
        <div class="three columns">
            @include('components.checkboxInline', ['id' => 'dinner', 'label' => 'Večera', 'name' => 'food[]', 'value' => 'dinner', 'checked' => $internship->hasMeal('dinner') ])
        </div>

        <input type="text" name="work_hours" value="{{ $internship->getWorkHours() }}"><br />


        <select name="city_id">
            @foreach($cities as $city)
                <option @if($internship->city_id === $city->id) selected @endif value="{{ $city->id }}" >{{ $city->getName() }}</option>
            @endforeach
        </select><br />


        <select name="category_id">
            @foreach($categories as $category)
                <option @if($internship->category_id === $category->id) selected @endif value="{{ $category->id }}">{{ $category->getName() }}</option>
            @endforeach
        </select><br />

        @if(!emptyArray($mentors) || ($mentors instanceof \Illuminate\Database\Eloquent\Collection))
            @foreach($mentors as $mentor)
                <label><input type="checkbox" @if(in_array($mentor->id, $selected_mentors)) checked
                              @endif name="mentors[]" value="{{ $mentor->id }}">{{ $mentor->name }}</label><br/>
            @endforeach
        @endif
        <input type="text" name="status" value="status">
        <input type="submit" value="Submit">
    </form>

@endsection