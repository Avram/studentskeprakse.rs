@extends('layouts.app')

@section('content')

<h1>{{ $internship->name }}</h1>
<p>
    {{ $internship->content }}
</p>
@endsection