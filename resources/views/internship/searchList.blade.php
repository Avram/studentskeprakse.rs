<div class="row eight columns content">
    <div class="row twelve columns form-element inner-addon left-addon">
        @if (!Auth::guest())
        <div class="three columns">
            <a href="{{ URL::route('internship.create') }}" id="add-new" class="btn btn-success icon icon-plus">Postavi
                praksu</a>
        </div>
        @endif
        <div id="sort-picker" class="three columns">
            <select class="twelve columns">
                <option selected disabled>Sortiraj po</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
            </select>
        </div>
    </div>
    <div class="row twelve columns list-wrapper">
        @foreach($internships as $internship)
            @include('internship.searchListItem', [
                'user' => $internship->user->slug,
                'slug' => $internship->slug,
                'title' => $internship->name,
                'company' => $internship->user->name,
                'city' => $internship->city->name,
                'category' => $internship->category->name,
                'imagesource' => $internship->user->present()->logo()
            ])
        @endforeach
    </div>
    @if(count($internships) == 10)
        <div class="row twelve columns">
            <div class="load-more" id="loadMore">Učitaj još</div>
        </div>
    @endif
</div>