<form class="internship-form" method="POST" action="">
    {!! csrf_field() !!}

    <div class="row"><!-- Internship Title -->
        <div class="eleven columns form-element">
            <label for="name">Naziv prakse</label>
            <input type="text" id="name" class="twelve columns {{ $errors->has('name') ? ' has-error' : '' }}" name="name"
                   value="@if($errors->any()){{ old('name') }}@else{{ $internship->getName() }}@endif"
                   placeholder="Naslov prakse" required="required">
        </div>
    </div><!-- Internship Title End -->

    <div class="row">
        <div class="six columns form-element"><!-- Internship Category -->
            <label for="category_id">Kategorija</label>
            @include('components.selectboxInline', [
                'attrs' => 'id="category_id"',
                'columns' => 'twelve category',
                'name' => 'category_id',
                'default' => 'Kategorija',
                'items' => $categories,
                'selected' => $selected_category
            ])
        </div><!-- Internship Category End-->

        <div class="five columns form-element"><!-- Internship City -->
            <label for="city_id">Grad</label>
            @include('components.selectboxInline', [
                'attrs' => 'id="category_id"',
                'columns' => 'twelve city',
                'name' => 'city_id',
                'default' => 'Grad',
                'items' => $cities,
                'selected' => $selected_city
            ])
        </div><!-- Internship City End -->
    </div>

    <div class="row"><!-- Internship Description -->
        <div class="eleven columns form-element">
            <label for="content">Opis</label>
            <textarea id="content" class="twelve columns{{ $errors->has('content') ? ' has-error' : '' }}" placeholder="Opis"
                      name="content"
                      required="required">@if($errors->any()){{ old('content') }}@else{{ $internship->getContent() }}@endif</textarea>
        </div>
    </div><!-- Internship Description End -->

    <div class="row"><!-- Internship Skills -->
        <div class="eleven columns form-element">
            <label for="required_skills">Zahtevi</label>
            <textarea class="twelve columns" id="required_skills" placeholder="Potrebne veštine"
                      name="required_skills">@if($errors->any()){{ old('required_skills') }}@else{{ $internship->getRequiredSkills() }}@endif</textarea>
        </div>
    </div><!-- Internship Skills End -->

    <div class="row">
        <div class="six columns form-element joined-elements">
            <label for="duration">Trajanje</label>
            <input type="text" id="duration" class="six columns joined-left" name="duration"
                   value="{{ $internship->getDuration() }}" placeholder="Broj">
            @include('components.selectboxInline', [
                'columns' => 'five',
                'class' => 'joined-right',
                'name' => 'work_dwm',
                'items' => [
                    'days' => 'Dana',
                    'weeks' => 'Nedelja',
                    'months' => 'Meseci'
                ],
                'selected' => $internship->getDurationType()
            ])
        </div>
        <div class="five columns form-element">
            <label for="work_hours">Nedeljno</label>
            <input type="text" id="work_hours" class="twelve columns {{ $errors->has('work_hours') ? ' has-error' : '' }}"
                   name="work_hours"
                   value="{{ $internship->getWorkHours() }}"
                   placeholder="Broj radnih sati nedeljno">
        </div>
    </div>
    <div class="row">
        <div class="six columns form-element">
            <label for="salary">Plata</label>
            <input type="text" id="salary" name="salary" class="twelve columns" placeholder="Plata (RSD)"
                   value="{{ $internship->getSalary() }}"/>
        </div>
        <div class="five columns form-element">
            <label for="mentors">Mentori</label>
            @include('components.selectboxInline', [
                'columns' => 'twelve',
                'name' => 'mentors[]',
                'class' => 'multi-mentors',
                'attrs' => 'multiple="multiple" id="mentors"',
                'items' => $mentors,
                'selected' => $selected_mentors,
                'multiple' => true
            ])
        </div>
    </div>
    <div class="row food">
        <div class="three columns">
            @include('components.checkboxInline', ['id' => 'breakfast', 'label' => 'Doručak', 'name' => 'food[]', 'value' => 'breakfast', 'checked' => $internship->hasMeal('breakfast') ])
        </div>
        <div class="three columns">
            @include('components.checkboxInline', ['id' => 'lunch', 'label' => 'Ručak', 'name' => 'food[]', 'value' => 'lunch', 'checked' => $internship->hasMeal('lunch') ])
        </div>
        <div class="three columns">
            @include('components.checkboxInline', ['id' => 'dinner', 'label' => 'Večera', 'name' => 'food[]', 'value' => 'dinner', 'checked' => $internship->hasMeal('dinner') ])
        </div>
    </div>
    <div class="row submit">
        <button type="submit" class="btn btn-info">Sačuvaj</button>
        <button type="button" class="btn btn-danger pull-right"
                onclick="location.href='{{ URL::route('company.internship', [$user->getSlug(), $internship->getSlug()]) }}';">
            Otkaži
        </button>
    </div>
</form>
@include('errors.formErrors', ['errors' => $errors])


@section('css')
    <link href="{{ asset('build/css/select2.min.css') }}" rel="stylesheet">
@endsection
@section('js')
    @parent
    <script src="{{ asset('build/js/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".multi-mentors").select2();
            $(".category").select2();
            $(".city").select2();
        });

//        function show(index) {
//            var tabs = document.getElementsByClassName('tab-wrapper');
//            var tabsLen = tabs.length;
//            for (var i=0; i< tabsLen; i++) {
//                tabs[i].style.display=''
//            }
//            document.getElementById('tab-wrapper-'+index).style.display='block';
//
//            document.getElementsByClassName('internship-tab active')[0].className = 'internship-tab';
//            document.getElementById('tab-'+index).className = 'internship-tab active'
//        }
    </script>
@endsection