<div class="twelve columns attachment-item">
    <div class="five columns mag-pop-ifrm">
        <a class="popup-{{ $attachment->getVideoType() }}" href="{{ $attachment->getUrl() }}">
            <img class="attachment-image" src="{{ url($attachment->present()->attachment()) }}" alt="">
        </a>
    </div>
    <div class="seven columns attachment-meta">
        <h4 class="attachment-title mag-pop-ifrm">
            <a class="popup-{{ $attachment->getVideoType() }}"
               href="{{ $attachment->getUrl() }}">{{ $attachment->getTitle() }}</a>
        </h4>
        <div class="twelve columns attachment-description">
            {!! $attachment->present()->autoLinkValue() !!}
        </div>
    </div>
    @if(Auth::check() && (Auth::user()->id === $user->id))
    <div class="attachment-options">
        <span data-id="{{ $attachment->id }}" onclick="deleteAttachment($(this))">&times;</span>
    </div>
    @endif
</div>