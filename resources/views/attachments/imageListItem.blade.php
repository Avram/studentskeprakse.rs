<div class="twelve columns attachment-item">
    <div class="five columns mag-pop-img">
        <a href="{{ url($attachment->present()->attachment()) }}">
            <img class="attachment-image" src="{{ url($attachment->present()->attachmentThumbnail()) }}" alt="">
        </a>
    </div>
    <div class="seven columns attachment-meta">
        <div class="twelve columns attachment-description">
            {!! $attachment->present()->autoLinkValue() !!}
        </div>
    </div>
    @if(Auth::check() && (Auth::user()->id === $user->id))
    <div class="attachment-options">
        <span data-id="{{ $attachment->id }}" onclick="deleteAttachment($(this))">&times;</span>
    </div>
    @endif
</div>