<div class="twelve columns attachment-item">
    <div class="five columns pdf-back mag-pop-ifrm">
        <a href="{{ url($attachment->present()->attachment()) }}">
            <div class="icon-pdf-2"></div>
        </a>
    </div>
    <div class="seven columns attachment-meta">
        @if(!isset($attachment->value))
            <h4 class="attachment-title mag-pop-ifrm">
                <a href="{{ url($attachment->present()->attachment()) }}">
                    {{ $attachment->present()->pdfNiceFileName() }}
                </a>
            </h4>
            {{--@endif--}}
        @else
            <div class="twelve columns attachment-description">
                {!! $attachment->present()->autoLinkValue() !!}
            </div>
        @endif
    </div>
    @if(Auth::check() && (Auth::user()->id === $user->id))
    <div class="attachment-options">
        <span data-id="{{ $attachment->id }}" onclick="deleteAttachment($(this))">&times;</span>
    </div>
    @endif
</div>