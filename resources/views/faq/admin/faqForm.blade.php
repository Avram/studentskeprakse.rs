@extends('layouts.app')

@section('content')

    <form method="POST" action="">
        {!! csrf_field() !!}
        <input type="text" name="question" value="{{ $faq->getQuestion() }}"><br/>
        <textarea name="answer">{{ $faq->getAnswer() }}</textarea><br/>
        <input type="submit" value="Submit">
    </form>

@endsection