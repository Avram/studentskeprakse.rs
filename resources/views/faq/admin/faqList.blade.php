@extends('layouts.app')

@section('content')

    <ul>
        @foreach($faqs as $faq)
            <li> {{ $faq->getQuestion()  }} |<a href="{{ URL::route('admin.faq.update', [$faq->getId()]) }}">
                    <small>(update)</small>
                </a> <a href="{{ URL::route('admin.faq.delete', [$faq->getId()]) }}">
                    <small>(delete)</small>
                </a></li>
        @endforeach

    </ul>

@endsection