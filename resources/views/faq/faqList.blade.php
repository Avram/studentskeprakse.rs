@extends('layouts.app')

@section('content')
    <div class="container">
        @include('components.managementNav', [
            'items' => [
                [
                    'title' => 'Prakse',
                    'active' => false,
                    'route' => 'company.internships.management'
                ],
                [
                    'title' => 'Mentori',
                    'active' => false,
                    'route' => 'company.mentors.management'
                ],
                [
                    'title' => 'Česta pitanja',
                    'active' => true,
                    'route' => 'company.faq.management'
                ],
                [
                    'title' => 'Prakse kod nas',
                    'active' => false,
                    'route' => 'company.feedback.management'
                ],
                [
                    'title' => 'Obrisano',
                    'active' => false,
                    'route' => 'company.internships.trash'
                ]
            ],
            'btn' => [
                'has' => true,
                'route' => 'company.faq.management.create',
                'icon' => true,
                'title' => 'Napravi'
            ]
        ])
        <div class="row twelve columns">
            @foreach($faqs as $faq)
                @include('faq.faqListItem', [
                    'question' => $faq->question,
                    'answer' => $faq->answer,
                    'id' => $faq->id
                ])
            @endforeach
        </div>
    </div>
@endsection