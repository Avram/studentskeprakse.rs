@extends('layouts.app')

@section('content')
    <div class="container panel internship-create">
        <form class="internship-form" method="POST" action="">
            {!! csrf_field() !!}
            <div class="row">
                <div class="eleven columns form-element">
                    <textarea class="twelve columns" placeholder="Pitanje"
                              name="question">{{ $faq->getQuestion() }}</textarea>
                </div>
            </div>
            <div class="row">
                <div class="eleven columns form-element">
                    <textarea class="twelve columns" placeholder="Odgovor"
                              name="answer">{{ $faq->getAnswer() }}</textarea>
                </div>
            </div>
            <div class="row submit">
                <button type="submit" class="two columns btn btn-info">Sačuvaj</button>
            </div>
        </form>
    </div>
@endsection