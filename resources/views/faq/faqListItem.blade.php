<div class="panel item">
    <div class="row header">
        <h3 class="pull-left">
            {{ $question }}
        </h3>
    </div>
    <div class="row meta">
        {{ $answer }}
        <div class="options pull-right">
            <a href="{{ URL::route('company.faq.management.delete', [$id]) }}"><span class="icon-trash"></span></a>
            <a href="{{ URL::route('company.faq.management.update', [$id]) }}"><span class="icon-pen"></span></a>
        </div>
    </div>
</div>