@extends('layouts.app')

@section('content')
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-8 col-md-offset-2">--}}
    {{--<div class="panel panel-default">--}}
    {{--<div class="panel-heading">Register</div>--}}
    {{--<div class="panel-body">--}}
    {{--<form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">--}}
    {{--{!! csrf_field() !!}--}}

    {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
    {{--<label class="col-md-4 control-label">Name</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input type="text" class="form-control" name="name" value="{{ old('name') }}">--}}

    {{--@if ($errors->has('name'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('name') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
    {{--<label class="col-md-4 control-label">E-Mail Address</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input type="email" class="form-control" name="email" value="{{ old('email') }}">--}}

    {{--@if ($errors->has('email'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('email') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
    {{--<label class="col-md-4 control-label">Password</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input type="password" class="form-control" name="password">--}}

    {{--@if ($errors->has('password'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('password') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">--}}
    {{--<label class="col-md-4 control-label">Confirm Password</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input type="password" class="form-control" name="password_confirmation">--}}

    {{--@if ($errors->has('password_confirmation'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('password_confirmation') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">--}}
    {{--<label class="col-md-4 control-label">User Type</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input type="text" class="form-control" name="type">--}}

    {{--@if ($errors->has('type'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('type') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group{{ $errors->has('employees') ? ' has-error' : '' }}">--}}
    {{--<label class="col-md-4 control-label">Employees</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input type="text" class="form-control" name="employees">--}}

    {{--@if ($errors->has('employees'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('employees') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group{{ $errors->has('founded') ? ' has-error' : '' }}">--}}
    {{--<label class="col-md-4 control-label">Founded</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input type="text" class="form-control" name="founded">--}}

    {{--@if ($errors->has('founded'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('founded') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">--}}
    {{--<label class="col-md-4 control-label">Address</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input type="text" class="form-control" name="address">--}}

    {{--@if ($errors->has('address'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('address') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">--}}
    {{--<label class="col-md-4 control-label">Description</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<textarea class="form-control" name="description"></textarea>--}}

    {{--@if ($errors->has('description'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('description') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">--}}
    {{--<label class="col-md-4 control-label">Website url</label>--}}

    {{--<div class="col-md-6">--}}
    {{--<input type="text" class="form-control" name="url">--}}

    {{--@if ($errors->has('url'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('url') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<div class="col-md-6 col-md-offset-4">--}}
    {{--<button type="submit" class="btn btn-primary">--}}
    {{--<i class="fa fa-btn fa-user"></i>Register--}}
    {{--</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}


    <div class="container">
        <div class="panel register six columns offset-by-three">
            <div class="row twelve columns">
                <div class="logo">
                    <img src="{{ asset('images/logo-dark.png') }}">
            </div>
        </div>
            @include('errors.formErrors')
            <form action="{{ URL::route("auth.postRegister") }}" method="POST">
                {!! csrf_field() !!}
                <div class="form-element">
                    <div class="row twelve columns">
                        <input type="text" class="twelve columns{{ $errors->has('name') ? ' has-error' : '' }}"
                               name="name" value="{{ old('name') }}"
                               placeholder="Ime kompanije" required/>
                    </div>
                </div>
                <div class="form-element">
                    <div class="row twelve columns">
                        <input type="email" class="twelve columns{{ $errors->has('email') ? ' has-error' : '' }}"
                               name="email" value="{{ old('email') }}"
                               placeholder="Email" required/>
                    </div>
                </div>
                <div class="form-element">
                    <div class="row twelve columns">
                        <input type="password" class="twelve columns{{ $errors->has('password') ? ' has-error' : '' }}"
                               name="password" placeholder="Lozinka" required/>
                    </div>
                </div>

                <div class="row twelve columns">
                    @include('components.recaptcha')
                </div>

                <div class="row twelve columns">
                    <button class="btn btn-info twelve columns">Registruj se</button>
                </div>

                <div class="row login twelve columns">
                    <span>Već imate nalog? <a href="{{ URL::route('auth.login') }}">Uloguj se</a></span>
                </div>
            </form>
    </div>
</div>
@endsection

@section('js')
    @parent
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection