@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel login six columns offset-by-three">
        <div class="row twelve columns">
            <div class="logo">
                <img src="{{ asset('images/logo-dark.png') }}">
            </div>
        </div>
        @if(Session::has('account-not-verified-id'))
        <div class="row twelve columns account-inactive">
            <p>Vaš nalog još uvek nije verifikovan. Proverite da li ste dobili link za verifikaciju
                naloga na vašu e-mail adresu.
            <p>
            <p><a href="{{ URL::route('auth.emailResend', [Session::get('account-not-verified-id')]) }}">Pošalji
                    ponovo link za verifikaciju.</a></p>
        </div>
        @endif
        @include('errors.formErrors')
        <form action="{{ URL::route("auth.postLogin") }}" method="POST" class="login-form">
            {!! csrf_field() !!}
            <div class="form-element">
                <div class="row twelve columns">
                    <input type="email" required class="twelve columns {{ $errors->has('email') ? ' has-error' : '' }}"
                           name="email" value="{{ old('email') }}"
                           placeholder="Email"/>
                </div>
            </div>
            <div class="form-element">
                <div class="row twelve columns">
                    <input type="password" required
                           class="twelve columns {{ $errors->has('password') ? ' has-error' : '' }}"
                           name="password" placeholder="Lozinka"/>
                </div>
            </div>
            <div class="form-element">
                @include('components/checkbox', [
                    'id' => 'remember-me',
                    'label' => 'Zapamti me',
                    'name' => 'remember',
                    'value' => 'rememberme',
                    'class' => '',
                    'checked' => ''
                ])
            </div>

            <div class="row twelve columns">
                <button class="btn btn-info twelve columns">Uloguj se</button>
            </div>

            <div class="row register twelve columns">
                <span>Nemate nalog? <a href="{{ URL::route('auth.register') }}">Registruj se</a></span>
            </div>
        </form>
    </div>
</div>
@endsection
