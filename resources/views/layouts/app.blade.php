<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('meta')

    <title>Studentske Prakse</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>
    {{--<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700&subset=latin,latin-ext' rel='stylesheet'--}}
          {{--type='text/css'>--}}
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    @yield('css')
    <link href="{{ asset('build/css/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('build/css/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('build/css/vex.css') }}" rel="stylesheet">
    <link href="{{ asset('build/css/vex-theme-default.css') }}" rel="stylesheet">
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    <style>
        body {
            font-family: 'Open Sans', sans-serif;
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
<nav class="navbar navbar-fixed-top">
    <div class="container">
        <div class="navbar-header three columns">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('images/logo-dark.png') }}">
            </a>
        </div>

        <div class="collapse navbar-collapse eight columns" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/') }}" class="active">Prakse</a></li>
                <li><a href="{{ url('/') }}">Saveti</a></li>
                @if(!Auth::guest())
                    <li><a href="{{ URL::route('company.internships.management') }}">Moje prakse</a></li>
                    <li><a href="{{ URL::route('company.settings.about') }}">Podešavanja</a></li>
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ URL::route('auth.register') }}">Postavi</a></li>
                    <li><a href="{{ URL::route('auth.login') }}">Uloguj se</a></li>
                @else
                    {{--<li><a href="{{ URL::route('internship.create') }}">Postavi</a></li>--}}
                    <li><a href="{{ URL::route('auth.logout') }}">Izloguj se</a></li>
                @endif
                {{--<li class="dropdown">--}}
                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">--}}
                {{--{{ Auth::user()->name }} <span class="caret"></span>--}}
                {{--</a>--}}

                {{--<ul class="dropdown-menu" role="menu">--}}
                {{--<li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}
                {{--@endif--}}
            </ul>
        </div>
            </div>
</nav>

<div class="container main-container">
    @yield('content')
</div>

<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{{ asset('build/js/vex.combined.min.js') }}"></script>
<script src="{{ asset('build/js/toastr.min.js') }}"></script>
<script src="{{ asset('build/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script>vex.defaultOptions.className = 'vex-theme-default'</script>
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
<script>
    toastr.options = {
        closeButton: true,
        newestOnTop: true,
        timeOut: false,
        extendedTimeOut: false
    };

    $(document).ready(function () {
        $('.scrollable').mCustomScrollbar({
            theme: "dark-thin",
            scrollInertia: 0
        });
    });
</script>
@include('misc.flash')
@include('misc.konami')
@yield('js')
</body>
</html>
