@extends('layouts.app')

@section('content')

    <div class="row twelve columns">
        @include('user.profile.profileCoverImage')
        @include('user.profile.profileSidebar', ['user' => $user, 'internships' => $internships])

        @yield('userContent')
    </div>

@endsection