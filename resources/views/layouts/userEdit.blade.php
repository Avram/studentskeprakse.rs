@extends('layouts.app')

@section('content')

    <div class="row twelve columns">
        @include('user.profile.profileCoverImage')
        @include('user.profile.profileEditSidebar', ['user' => $user])

        <div class="eight columns panel content">
            <div class="twelve columns">
                @include('components.userNav', [
                    'items' => [
                        [
                            'route' => 'internship.edit',
                            'routeParams' => ['username' => $user->slug, 'internship' => $internship->slug],
                            'title' => 'Praksa'
                        ],
                        [
                            'route' => 'about.edit',
                            'routeParams' => ['username' => $user->slug, 'internship' => $internship->slug],
                            'title' => 'O nama'
                        ],
                        [
                            'route' => 'feedback.edit',
                            'routeParams' => ['username' => $user->slug, 'internship' => $internship->slug],
                            'title' => 'Prakse kod nas'
                        ],
                        [
                            'route' => 'internship',
                            'routeParams' => ['username' => $user->slug, 'internship' => $internship->slug, '#offices'],
                            'title' => 'Kancelarije'
                        ],
                        [
                            'route' => 'internship',
                            'routeParams' => ['username' => $user->slug, 'internship' => $internship->slug, '#attachments'],
                            'title' => 'Prilozi'
                        ],
                        [
                            'route' => 'faq.edit',
                            'routeParams' => ['username' => $user->slug, 'internship' => $internship->slug],
                            'title' => 'Česta pitanja'
                        ]
                    ],
                    'user' => $user,
                    'settings' => true
                ])
                <div class="main-content">
                    @yield('userEditContent')
                </div>
            </div>
        </div>
    </div>

@endsection