@extends('layouts.app')

@section('content')

    <ul>
        @foreach($posts as $post)
            <li> {{ $post->title  }} |<a href="{{ URL::route('admin.post.update', [$post->id]) }}">
                    <small>(update)</small>
                </a> <a href="{{ URL::route('admin.post.delete', [$post->id]) }}">
                    <small>(delete)</small>
                </a></li>
        @endforeach

    </ul>

@endsection