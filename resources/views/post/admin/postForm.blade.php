@extends('layouts.app')

@section('content')

    <form method="POST" action="">
        {!! csrf_field() !!}
        <input type="text" name="title" value="{{ $post->getTitle() }}">
        <textarea name="content">{{ $post->getContent() }}</textarea>
        <input type="submit" value="Submit">
    </form>

@endsection