<!DOCTYPE html>
<html>
    <head>
        <title>Vraćamo se uskoro</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:100&subset=latin,latin-ext" rel="stylesheet"
              type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Roboto';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Vraćamo se uskoro.</div>
            </div>
        </div>
    </body>
</html>
