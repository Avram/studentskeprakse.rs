@extends('layouts.app')

@section('content')
    <div class="container">
        @include('components.managementNav', [
            'items' => [
                [
                    'title' => 'Prakse',
                    'active' => false,
                    'route' => 'company.internships.management'
                ],
                [
                    'title' => 'Mentori',
                    'active' => false,
                    'route' => 'company.mentors.management'
                ],
                [
                    'title' => 'Česta pitanja',
                    'active' => false,
                    'route' => 'company.faq.management'
                ],
                [
                    'title' => 'Prakse kod nas',
                    'active' => true,
                    'route' => 'company.feedback.management'
                ],
                [
                    'title' => 'Obrisano',
                    'active' => false,
                    'route' => 'company.internships.trash'
                ]
            ],
            'btn' => [
                'has' => false,
            ]
        ])
        <div class="row twelve columns">
            @foreach($feedbacks as $feedback)
                @include('feedback.feedbackListItem', [
                    'name' => $feedback->name,
                    'feedback' => $feedback->feedback,
                    'reply' => $feedback->reply,
                    'id' => $feedback->id
                ])
            @endforeach
        </div>
    </div>
@endsection