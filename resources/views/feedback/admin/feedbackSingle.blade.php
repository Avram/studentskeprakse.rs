@extends('layouts.app')

@section('content')

    <h1>{{ $post->title }}</h1>
    <p>
        {{ $post->content }}
    </p>

    @foreach($post->comments as $comment)
        <div>
            <div>actions: <a href="{{ URL::route('admin.comment.delete', [$comment->id]) }}">
                    <small>(delete)</small>
                </a></div>
            <div> {{ $comment->name }}</div>
            <div> {{ $comment->content }}</div>
        </div>
    @endforeach

@endsection