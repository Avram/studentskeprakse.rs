@extends('layouts.app')

@section('content')

    <form method="POST" action="">
        {!! csrf_field() !!}
        <input type="text" name="feedback_name" value="{{ $feedback->getName() }}"><br/>
        <textarea name="feedback">{{ $feedback->getFeedback() }}</textarea><br/>
        <textarea name="feedback_reply">{{ $feedback->getReply() }}</textarea><br/>
        <input type="submit" value="Submit">
    </form>

@endsection