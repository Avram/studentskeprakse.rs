@extends('layouts.app')

@section('content')

    <ul>
        @foreach($feedbacks as $feedback)
            <li> {{ $feedback->getName()  }} |<a href="{{ URL::route('admin.feedback.update', [$feedback->getId()]) }}">
                    <small>(update)</small>
                </a> <a href="{{ URL::route('admin.feedback.delete', [$feedback->getId()]) }}">
                    <small>(delete)</small>
                </a></li>
        @endforeach

    </ul>

@endsection