<div class="panel item">
    <div class="row header">
        <h3 class="pull-left">
            {{ $name }}
        </h3>
    </div>
    <div class="row meta">
        <div class="pull-left">
            <p>{{ $feedback }}</p>
            <p><i>{{ $reply }}</i></p>
        </div>
        <div class="options pull-right">
            <a href="{{ URL::route('company.feedback.management.update', [$id]) }}"><span class="icon-reply"></span></a>
            <a href="{{ URL::route('company.feedback.management.delete', [$id]) }}"><span class="icon-trash"></span></a>
        </div>
    </div>
</div>