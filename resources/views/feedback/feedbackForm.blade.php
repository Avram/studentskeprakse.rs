@extends('layouts.app')

@section('content')
    <div class="container panel internship-create">
        <form class="internship-form" method="POST" action="">
            {!! csrf_field() !!}
            <div class="feedbackItem">
                <div class="row">
                    <blockquote class="feedbackContent">
                        <p>{{ $feedback->getFeedback() }}</p>
                    </blockquote>
                </div>
                <div class="row">
                    <cite class="feedbackName">{{ $feedback->getName() }}</cite>
                </div>
            </div>
            <div class="row">
                <div class="eleven columns form-element">
                    <textarea class="twelve columns" placeholder="Odgovor"
                              name="reply">{{ $feedback->getReply() }}</textarea>
                </div>
            </div>
            <div class="row submit">
                <button type="submit" class="two columns btn btn-info">Sačuvaj</button>
            </div>
        </form>
    </div>
@endsection