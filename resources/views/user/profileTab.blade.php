<h3 class="internship-title">{{ $internship->name }}</h3>
<p class="internship-description">
    {!! $internship->present()->content() !!}
</p>
@if(!empty($internship->getRequiredSkills()))
    <h4 class="internship-title">Potrebne veštine</h4>
    {!!  $internship->present()->requiredSkills() !!}
@endif
<div class="meta-info">
    <div class="meta-item">
        <div class="icon-calendar meta-icon"></div>
        <div class="meta-value">{{ $internship->present()->duration() }}</div>
    </div>
    <div class="meta-item">
        <div class="icon-watch meta-icon"></div>
        <div class="meta-value">{{ $internship->present()->workingHours() }}</div>
    </div>
    @if($internship->hasSalary())
        <div class="meta-item">
            <div class="icon-dollar meta-icon"></div>
            <div class="meta-value">{{ $internship->present()->salary() }}</div>
        </div>
    @endif
    {{----}}
    {{--@if($internship->mentors()->count() > 0)--}}
    {{--<div class="meta-item">--}}
    {{--<div class="icon-person meta-icon"></div>--}}
    {{--<div class="meta-value">{!! $internship->present()->mentors()  !!}</div>--}}
    {{--</div>--}}
    {{--@endif--}}
    {{----}}
    @if($internship->hasMeals())
        <div class="meta-item">
            <div class="icon-bowl meta-icon"></div>
            <div class="meta-value">{!! $internship->present()->meals() !!}</div>
        </div>
    @endif
</div>
<div class="buttons mail">
    @if(Auth::check() && (Auth::user()->id === $user->id))
        <div class="row twelve columns">
            <button class="btn btn-info" onclick="location.href = '{{ URL::route('company.internship.edit', ['username' => $user->slug, 'internship' => $internship->slug]) }}';">Izmeni</button>
        </div>
    @else
        <div class="row twelve columns">
            <button class="btn btn-info">Prijavi se</button>
        </div>
    @endif
</div>

@section('js')
    @parent
    <script>
        function show(index) {
            var tabs = document.getElementsByClassName('tab-wrapper');
            var tabsLen = tabs.length;
            for (var i=0; i< tabsLen; i++) {
                tabs[i].style.display=''
            }
            document.getElementById('tab-wrapper-'+index).style.display='block';

            document.getElementsByClassName('internship-tab active')[0].className = 'internship-tab';
            document.getElementById('tab-'+index).className = 'internship-tab active'
        }
    </script>
@endsection