@extends('layouts.userEdit')

@section('userEditContent')
    <form action="" method="POST">
        {!! csrf_field() !!}

        <div class="row">
            <div class="twelve columns form-element">
                <label for="question">Pitanje</label>
                <textarea id="question" class="twelve columns{{ $errors->has('question') ? ' has-error' : '' }}"
                      name="question" placeholder="Pitanje"
                      required="required">@if($errors->any()){{ old('question') }}@endif</textarea>
            </div>
        </div>

        <div class="row">
            <div class="twelve columns form-element">
                <label for="answer">Odgovor</label>
                <textarea id="answer" class="twelve columns{{ $errors->has('answer') ? ' has-error' : '' }}"
                          name="answer" placeholder="Odgovor"
                          required="required">@if($errors->any()){{ old('answer') }}@endif</textarea>
            </div>
        </div>

        <div class="row submit">
            <button type="submit" class="btn btn-info pull-right">Sačuvaj</button>
        </div>
    </form>

    <div class="faqs">
        <div class="row">
            @foreach($faqs as $faq)
                {{--@if(isset($faq->answer))--}}
                    @include('user.faqsItem', [
                        'question' => $faq->question,
                        'answer' => $faq->answer,
                        'username' => $user->slug,
                        'internship' => $internship->slug,
                        'id' => $faq->id
                    ])
                {{--@endif--}}
            @endforeach
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script>
        $(document).ready(function () {

            $('[id^=reply-]').click(function() {
                $id = $(this).data('id');
                $('.reply-form-'+$id).toggle();
            });

            $('[id^=cancel-]').click(function () {
                $id = $(this).data('id');
                $('.reply-form-'+$id+' textarea').val('');
                $('.reply-form-'+$id).toggle();
            });

            $('[id^=submit-]').click(function () {
                $id = $(this).data('id');
                $reply = $('.reply-form #reply').val();
                $form = $('.reply-form-' + $id);
                $parent = $form.parent();
                $fd = new FormData();

                $fd.append('_token', '{!! csrf_token() !!}');
                $fd.append('reply', $reply);
                $fd.append('id', $id);

                xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                        $response = JSON.parse(xhr.responseText);
                        if($response.status == "OK") {
                            $siblings = $form.siblings();
                            $siblings.remove();
                            $form.remove();
                            $parent.append($('<p></p>').attr({class: 'faqAnswer'}).text($response.reply));
                        }
                    }
                };
                xhr.open("POST", "{{ URL::route('ajax.faq.reply') }}");
                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                xhr.send($fd);
            });
        });
    </script>
@endsection