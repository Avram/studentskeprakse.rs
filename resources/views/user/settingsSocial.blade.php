@extends('layouts.user')

@section('userContent')
    <div class="eight columns panel content">
        <div class="twelve columns">
            @include('components.userNav',
            [
                'items' => [
                    [
                        'route' => 'settings.about',
                        'active' => false,
                        'title' => 'Ukratko o kompaniji'
                    ],
                    [
                        'route' => 'settings.social',
                        'active' => true,
                        'title' => 'Social'
                    ],
                    [
                        'route' => 'settings.profile',
                        'active' => false,
                        'title' => 'Profile'
                    ]
                ],
                'settings' => true
            ])

            <div class="twelve columns main-content settings">
                @include('user.settingsSocialForm', ['user' => $user])
            </div>
        </div>
    </div>
@endsection