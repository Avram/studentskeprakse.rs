<form action="" method="POST">
    {!! csrf_field() !!}
    <div class="row form-element">
        <input type="number" name="founded" class="six columns" value="{{ $user->getFounded() }}" min="1800"
               max="{{ date('Y') }}" placeholder="Godina osnivanja"/>
        @include('components.selectboxInline',[
            'columns' => 'six',
            'class' => 'categories-search',
            'default' => 'Oblast',
            'items' => $categories,
            'name' => 'categoryId',
            'selected' => $selectedCategory
        ])
    </div>

    <div class="row form-element">
        <textarea class="twelve columns" placeholder="Opis firme"
                  name="description">{{ $user->getDescription() }}</textarea>
    </div>

    <div class="row form-element">
        <textarea class="twelve columns" placeholder="Opis tima"
                  name="teamDescription">{{ $user->getTeamDescription() }}</textarea>
    </div>

    <div class="row form-element">
        <input type="text" name="employees" class="twelve columns" value="{{ $user->getEmployees() }}"
               placeholder="Broj zaposlenih"/>
    </div>

    <div class="row form-element">
        <input type="text" name="address" class="six columns" value="{{ $user->getAddress() }}" placeholder="Adresa"/>
        @include('components.selectboxInline',[
            'columns' => 'six',
            'class' => 'cities-search',
            'default' => 'Grad',
            'items' => $cities,
            'name' => 'cityId',
            'selected' => $selectedCity
        ])
    </div>

    <button type="submit" class="pull-right btn btn-info">Sačuvaj</button>

</form>

@section('css')
    <link href="{{ asset('build/css/select2.min.css') }}" rel="stylesheet">
@endsection
@section('js')
    @parent
    <script src="{{ asset('build/js/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.categories-search').select2();
            $('.cities-search').select2();
        });
    </script>
@endsection