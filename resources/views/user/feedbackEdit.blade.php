@extends('layouts.userEdit')

@section('userEditContent')
    <form action="" method="POST">
        {!! csrf_field() !!}

        <div class="row">
            <div class="twelve columns form-element">
                <label for="team_description">Opis</label>
            <textarea id="team_description" class="twelve columns{{ $errors->has('team_description') ? ' has-error' : '' }}"
                      name="team_description" placeholder="O atmosferi, timu,..."
                      required="required">@if($errors->any()){{ old('team_description') }}@else{{ $user->getTeamDescription() }}@endif</textarea>
            </div>
        </div>

        <div class="row submit">
            <button type="submit" class="btn btn-info pull-right">Sačuvaj</button>
        </div>
    </form>

    @foreach($feedbacks as $feedback)
        @include('user.feedbackItem', [
            'feedback' => $feedback->feedback,
            'name' => $feedback->name,
            'reply' => $feedback->reply,
            'id' => $feedback->id,
            'username' => $user->slug,
            'internship' => $internship->slug
            ])
    @endforeach

@endsection

@section('js')
    @parent
    <script>
        $(document).ready(function () {

            $('[id^=reply-]').click(function() {
                $id = $(this).data('id');
                $('.reply-form-'+$id).toggle();
            });

            $('[id^=cancel-]').click(function () {
                $id = $(this).data('id');
                $('.reply-form-'+$id+' textarea').val('');
                $('.reply-form-'+$id).toggle();
            });

            $('[id^=submit-]').click(function () {
                $id = $(this).data('id');
                $reply = $('.reply-form #reply').val();
                $form = $('.reply-form-' + $id);
                $parent = $form.parent();
                $fd = new FormData();

                $fd.append('_token', '{!! csrf_token() !!}');
                $fd.append('reply', $reply);
                $fd.append('id', $id);

                xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                        $response = JSON.parse(xhr.responseText);
                        if($response.status == "OK") {
                            $siblings = $form.siblings();
                            $siblings.remove();
                            $form.remove();
                            $parent.append($('<p></p>').attr({class: 'feedbackReply'}).text($response.reply));
                        }
                    }
                };
                xhr.open("POST", "{{ URL::route('ajax.feedback.reply') }}");
                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                xhr.send($fd);
            });
        });
    </script>
@endsection