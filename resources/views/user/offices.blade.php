
<div class="offices">
    @if(Auth::check() && (Auth::user()->id === $user->id))
        @include('components.dropzone', ['name' => 'image', 'username' => $user->slug])
    @else
        <div id="big-image">
            @foreach($offices as $image)
                <a class="office-img-big" id="big-image-{{ $image->id }}" data-id="{{ $image->id }}" href="{{ url($image->present()->attachment()) }}">
                    <img class="office-img big" src="{{ url($image->present()->attachmentThumbnail()) }}" alt="">
                </a>
            @endforeach
        </div>
    @endif
    <div id="images">
        @foreach($offices as $image)
                <img onclick="showBigImg({{ $image->id }})" class="office-img" src="{{ url($image->present()->attachmentThumbnail()) }}" alt="">

                @if(Auth::check() && (Auth::user()->id === $user->id))
                <div class="img-options">
                    <span data-id="{{ $image->id }}" onclick="deleteImage($(this));">
                        &times;
                    </span>
                </div>
                @endif
        @endforeach
    </div>
</div>

@section('css')
    @parent
    <link href="{{ asset('build/css/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('build/css/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('build/css/slick-theme.css') }}" rel="stylesheet">
@endsection
@section('js')
    @parent
    <script src="{{ asset('build/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('build/js/jquery-sortable-min.js') }}"></script>
    <script src="{{ asset('build/js/slick.min.js') }}"></script>
    <script>
        $('#big-image').magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
                enabled: true
            }
        });
        document.getElementsByClassName('office-img-big')[0].className = 'office-img-big active'
        $('#images').slick({
            infinite: false,
            variableWidth: true,
            speed: 200,
            slidesToShow: 3,
            slidesToScroll: 2
        });
        @if(Auth::check() && (Auth::user()->id === $user->id))
        var group = $('#images').sortable({
            vertical: false,
            itemSelector: '.office-img',
            placeholder: '<div class="item-placeholder four columns"><span class="icon-images"></span></div>',
            onDrop: function ($item, container, _super) {
                var data = group.sortable("serialize").get();
                $item[0].style = "";
                $($item[0]).removeClass('dragged');

                var jsonString = JSON.stringify(data, null, ' ');

                var fd = new FormData();
                var xhr = new XMLHttpRequest();

                fd.append('_token', '{{ csrf_token() }}');
                fd.append('order', jsonString);

                xhr.open("POST", "{{ URL::route('ajax.offices.updateOrder') }}");
                xhr.send(fd);
            }
        });
        @endif

        function showBigImg (id) {
            if(document.getElementsByClassName('office-img-big active').length)
                    document.getElementsByClassName('office-img-big active')[0].className = 'office-img-big'
            document.getElementById('big-image-'+id).className = 'office-img-big active'
            return false;
        }
    </script>
@endsection