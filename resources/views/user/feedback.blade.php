@if(Auth::check() && (Auth::user()->id === $user->id))
    <div class="row twelve columns">
        <button class="btn btn-info"
                onclick="location.href = '{{ URL::route('company.feedback.edit', ['username' => $user->slug, 'internship' => $internship->slug]) }}';">
            Izmeni
        </button>
    </div>
@endif

<div class="feedback">
    <p id="team-description">{{ $user->team_description }}</p>
    <hr>
    @foreach($feedback as $item)
        @include('user.feedbackItem', [
            'id' => $item->id,
            'feedback' => $item->feedback,
            'name' => $item->name,
            'reply' => $item->reply,
            'username' => $user->slug,
            'internship' => $internship->slug
        ])
    @endforeach
    {{--@if(Auth::guest() || (Auth::user()->id !== $user->id))--}}
    @if(Auth::guest())
    <hr>
    <div class="feedback-form">
        <form action="{{ URL::route('company.feedback.post', [$user->getSlug(), $internship->getSlug()]) }}"
              method="POST">
            {!! csrf_field() !!}
            <div class="row form-element">
                <input class="twelve columns" type="text" placeholder="Ime i prezime" name="name"/>
            </div>
            <div class="row form-element">
                <textarea name="feedback" class="twelve columns" placeholder="Ostavi komentar"></textarea>
            </div>
            <div class="row twelve columns">
                @include('components.recaptcha', ['id' => 'feedback'])
            </div>
            <div class="row">
                <button class="btn btn-info twelve columns">Pošalji</button>
            </div>
        </form>
    </div>
    @endif
</div>