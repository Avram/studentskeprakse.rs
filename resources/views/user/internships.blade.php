@extends('layouts.user')

@section('userContent')
<div class="eight columns panel content">
    <div class="twelve columns">
        @include('components.userNav',
        [
            'items' => [
                [
                    'route' => 'internships',
                    'active' => true,
                    'title' => 'Prakse'
                ],
                [
                    'route' => 'about',
                    'active' => false,
                    'title' => 'O nama'
                ],
                [
                    'route' => 'feedback',
                    'active' => false,
                    'title' => 'Prakse kod nas'
                ],
                [
                    'route' => 'offices',
                    'active' => false,
                    'title' => 'Kancelarije'
                ],
                [
                    'route' => 'attachments',
                    'active' => false,
                    'title' => 'Prilozi'
                ],
                [
                    'route' => 'faqs',
                    'active' => false,
                    'title' => 'Česta pitanja'
                ]
            ],
            'user' => $user
        ])
        <div class="twelve columns main-content internships">
            @if(Auth::check() && (Auth::user()->id === $user->id))
                <div class="twelve columns create">
                    <button onclick="location.href='{{ URL::route('internship.create') }}'"
                            class="btn btn-success icon pull-right"><span class="icon-plus"></span> Postavi
                    </button>
                </div>
            @endif
            @foreach($internships as $item)
                @include('user.internshipsItem', [
                    'user' => $item->user->slug,
                    'slug' => $item->slug,
                    'title' => $item->name,
                    'company' => $item->user->name,
                    'city' => $item->city->name,
                    'category' => $item->category->name,
                ])
            @endforeach
        </div>
    </div>
</div>
@endsection