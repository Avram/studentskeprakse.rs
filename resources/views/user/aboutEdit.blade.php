@extends('layouts.userEdit')

@section('userEditContent')
    <form class="about-form" method="POST" action="" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <div class="row">
            <div class="twelve columns form-element">
                <label for="description">Ukratko o kompaniji</label>
            <textarea id="description" class="twelve columns{{ $errors->has('description') ? ' has-error' : '' }}"
                      name="description"
                      required="required">@if($errors->any()){{ old('description') }}@else{{ $user->getDescription() }}@endif</textarea>
            </div>
        </div>


        <div class="row">
            <div class="form-element logo-lable">
                <label for="logo-edit">Logo</label>
            </div>
        </div>
        <div class="row">
            <div class="three columns company-logo edit-logo">
                @if($user->getLogo())
                    <img src="{{ url($user->present()->logo()) }}"/>
                @else
                    <img src="{{ URL::route('company.profile.dynamicLogo', [$user->getSlug()]) }}"/>
                @endif
                @include('components.logoDropzone', ['name' => 'logo-edit', 'username' => $user->slug, 'form' => false, 'js' => false])
            </div>
            <div class="three columns logo-button">
                <label for="logo-edit">
                    <button type="button" class="btn btn-info" onclick="$('#logo-edit').click()">Izaberi logo</button>
                </label>
            </div>

        </div>

        <div class="row">
            <div class="twelve columns form-element">
                <div class="pull-left six columns">
                    <label for="employees">Broj zaposlenih</label>
                    <input type="text" id="employees" class="twelve columns{{ $errors->has('employees') ? ' has-error' : '' }}" name="employees" value="@if($errors->any()){{ old('employees') }}@else{{ $user->getEmployees() }}@endif">
                </div>
                <div class="pull-right six columns">
                    <label for="founded">Godina osnivanja</label>
                    <input type="number" id="founded" name="founded" class="twelve columns{{ $errors->has('founded') ? ' has-error' : '' }}" value="@if($errors->any()){{ old('founded') }}@else{{ $user->getFounded() }}@endif" min="1800" max="{{ date('Y') }}">
                </div>
            </div>
        </div>

        <div class="row">
            @include('components.selectboxRow', [
                'default' => 'Kategorija',
                'name' => 'category_id',
                'attrs' => 'id=category_id',
                'label' => 'Kategorija',
                'misc' => ['id' => 'category_id'],
                'items' => $categories,
                'selected' => $selected_category
            ])
        </div>

        <div class="row">
            <div class="twelve columns form-element">
                <div class="pull-left six columns">
                    <label for="address">Adresa</label>
                    <input type="text" id="address" name="address" class="twelve columns{{ $errors->has('address') ? ' has-error' : '' }}" value="@if($errors->any()){{ old('address') }}@else{{ $user->getAddress() }}@endif"/>
                </div>
                <div class="pull-right six columns">
                    <label for="city_id">Grad</label>
                    @include('components.selectboxInline',[
                        'columns' => 'twelve',
                        'attrs' => 'id=city_id',
                        'class' => 'cities-search',
                        'default' => 'Grad',
                        'items' => $cities,
                        'name' => 'city_id',
                        'selected' => $selected_city
                    ])
                </div>
            </div>
        </div>

        <div class="row">
            <div class="twelve columns form-element">
                <label for="url">Websajt URL</label>
                <input type="url" id="url" class="twelve columns{{ $errors->has('url') ? ' has-error' : '' }}"
                       name="url" value="@if($errors->any()){{ old('url') }}@else{{ $user->getUrl() }}@endif">
            </div>
        </div>

        <div class="row">
            <div class="twelve columns form-element">
                <label for="facebook_url">Facebook URL</label>
                <input type="url" id="facebook_url"
                       class="twelve columns{{ $errors->has('facebook_url') ? ' has-error' : '' }}" name="facebook_url"
                       value="@if($errors->any()){{ old('facebook_url') }}@else{{ $user->getFacebookUrl() }}@endif">
            </div>
        </div>

        <div class="row">
            <div class="twelve columns form-element">
                <label for="twitter_url">Twitter URL</label>
                <input type="url" id="twitter_url"
                       class="twelve columns{{ $errors->has('twitter_url') ? ' has-error' : '' }}" name="twitter_url"
                       value="@if($errors->any()){{ old('twitter_url') }}@else{{ $user->getTwitterUrl() }}@endif">
            </div>
        </div>

        <div class="row">
            <div class="twelve columns form-element">
                <label for="linkedin_url">LinkedIn URL</label>
                <input type="url" id="linkedin_url"
                       class="twelve columns{{ $errors->has('linkedin_url') ? ' has-error' : '' }}" name="linkedin_url"
                       value="@if($errors->any()){{ old('linkedin_url') }}@else{{ $user->getLinkedinUrl() }}@endif">
            </div>
        </div>

        <div class="row submit aboutBtns">
            <button type="submit" class="btn btn-info">Sačuvaj</button>
            <button type="button" class="btn btn-danger pull-right"
                    onclick="location.href='{{ URL::route('company.internship', [$user->getSlug(), $internship->getSlug(), '#about']) }}';">
                Otkaži
            </button>
        </div>
    </form>
    @include('errors.formErrors', ['errors' => $errors])
@endsection

@section('css')
    <link href="{{ asset('build/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    @parent
    <script src="{{ asset('build/js/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $("#category_id").select2();
            $("#city_id").select2();
            $("#logo-edit").change(function(){
                readURL(this);
            });


            function readURL(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('.edit-logo > img').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
        });
    </script>
@endsection