<div>
    <a href="{{ URL::route('company.internship', [$user, $slug]) }}">
        <div class="panel item">
            <div class="row header">
                <h3 class="pull-left">
                    <span href="">{{ $title }}</span>
                </h3>
                <div class="company pull-right">{{ $company }}</div>
            </div>
            <div class="row meta">
                <span class="city pull-left" href="#">{{ $city }}</span>
                <div class="category pull-right">{{ $category }}</div>
            </div>
        </div>
    </a>
</div>