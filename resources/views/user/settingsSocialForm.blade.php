<form action="" method="POST">
    {!! csrf_field() !!}
    <div class="row form-element">
        <input type="text" name="url" class="twelve columns" value="{{ $user->getUrl() }}" placeholder="Link ka blogu"/>
    </div>

    <div class="row form-element">
        <input type="text" name="facebookUrl" class="twelve columns" value="{{ $user->getFacebookUrl() }}"
               placeholder="Link ka Facebook stranici"/>
    </div>

    <div class="row form-element">
        <input type="text" name="twitterUrl" class="twelve columns" value="{{ $user->getTwitterUrl() }}"
               placeholder="Link ka Twitter stranici"/>
    </div>

    <div class="row form-element">
        <input type="text" name="linkedinUrl" class="twelve columns" value="{{ $user->getLinkedinUrl() }}"
               placeholder="Link ka Linkedin stranici"/>
    </div>

    <button type="submit" class="pull-right btn btn-info">Sačuvaj</button>

</form>