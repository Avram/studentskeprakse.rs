@extends('layouts.app')

@section('content')

    <form method="POST" action="">
        {!! csrf_field() !!}
        <input type="text" required name="name" value="{{ $user->getName() }}"><br/>
        <input type="text" required name="email" value="{{ $user->getEmail() }}"><br/>
        <input type="text" name="employees" value="{{ $user->getEmployees() }}"><br/>
        <input type="number" max="{{ date('Y') }}" name="founded" value="{{ $user->getFounded() }}"><br/>
        <input type="text" name="address" value="{{ $user->getAddress() }}"><br/>
        <textarea name="description">{{ $user->getDescription() }}</textarea><br/>
        <input type="url" name="url" value="{{ $user->getUrl() }}"><br/>
        <input type="url" name="facebook_url" value="{{ $user->getFacebookUrl() }}"><br/>
        <input type="url" name="twitter_url" value="{{ $user->getTwitterUrl() }}"><br/>
        <input type="url" name="linkedin_url" value="{{ $user->getLinkedinUrl() }}"><br/>
        <input type="password" name="password" value=""><br/>
        <input type="password" name="password_confirmation" value=""><br/>

        <select name="type">
            @foreach($userTypes as $type)
                <option @if($user->type === $type) selected @endif value="{{ $type }}">{{ ucfirst($type) }}</option>
            @endforeach
        </select><br/>

        <select name="city_id">
            @foreach($cities as $city)
                <option @if($user->city_id === $city->id) selected
                        @endif value="{{ $city->getId() }}">{{ $city->getName() }}</option>
            @endforeach
        </select><br/>

        <select name="category_id">
            @foreach($categories as $category)
                <option @if($user->category_id === $category->id) selected
                        @endif value="{{ $category->getId() }}">{{ $category->getName() }}</option>
            @endforeach
        </select><br/>

        <input type="submit" value="Sačuvaj">
    </form>

@endsection