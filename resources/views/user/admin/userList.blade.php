@extends('layouts.app')

@section('content')

    <ul>
        @foreach($users as $user)
            <li> {{ $user->getName() }} |<a href="{{ URL::route('admin.user.update', [$user->getId()]) }}">
                    <small>(update)</small>
                </a> <a href="{{ URL::route('admin.user.delete', [$user->id]) }}">
                    <small>(delete)</small>
                </a></li>
        @endforeach

    </ul>

@endsection