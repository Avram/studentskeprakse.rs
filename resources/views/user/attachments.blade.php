<div class="attachments">
    @if(Auth::check() && (Auth::user()->id === $user->id))
        <div class="twelve columns">
            @include('user.attachmentForm')
        </div>

        <div class="twelve  columns">
            <hr>
        </div>


    @endif

    <div class="twelve columns">
        @foreach($attachments as $attachment)
            @switch($attachment->type)
            @case(\StudentskePrakse\Model\Attachment::TYPE_LINK)
            @include('attachments.linkListItem', ['attachment' => $attachment, 'user' => $user])
            @break
            @case(\StudentskePrakse\Model\Attachment::TYPE_IMAGE)
            @include('attachments.imageListItem', ['attachment' => $attachment, 'user' => $user])
            @break
            @case(\StudentskePrakse\Model\Attachment::TYPE_TEXT)
            @include('attachments.textListItem', ['attachment' => $attachment, 'user' => $user])
            @break
            @case(\StudentskePrakse\Model\Attachment::TYPE_PDF)
            @include('attachments.pdfListItem', ['attachment' => $attachment, 'user' => $user])
            @break
            @case(\StudentskePrakse\Model\Attachment::TYPE_VIDEO)
            @include('attachments.videoListItem', ['attachment' => $attachment, 'user' => $user])
            @break
            @default
            @break
            @endswitch
        @endforeach
    </div>
</div>

@section('css')
    @parent
    <link href="{{ asset('build/css/magnific-popup.css') }}" rel="stylesheet">
@endsection

@section('js')
    @parent
    <script src="{{ asset('build/js/jquery.magnific-popup.min.js') }}"></script>
    <script>
        $('.mag-pop-img').magnificPopup({
            delegate: 'a',
            type: 'image'
        });

        $('.mag-pop-ifrm').magnificPopup({
            delegate: 'a',
            type: 'iframe'
        });
    </script>
    <script>
        function deleteAttachment($element) {
            var id = $element.attr('data-id');

            vex.dialog.confirm({
                message: 'Da li ste sigurni da želite da obrišete ovaj prilog?',
                callback: function (value) {
                    if (value) {
                        var fd = new FormData();
                        var xhr = new XMLHttpRequest();

                        fd.append('_token', "{{ csrf_token() }}");
                        fd.append('id', id);
                        xhr.onreadystatechange = function () {
                            if (xhr.readyState == XMLHttpRequest.DONE) {
                                $result = JSON.parse(xhr.responseText);
                                if ($result.status === "OK") {
                                    $element.parent().parent().fadeOut(function () {
                                        $(this).remove();
                                    });
                                }
                            }
                        };

                        xhr.open("POST", "{{ URL::route('ajax.attachments.removeAttachment') }}");
                        xhr.send(fd);
                    } else {
                        return;
                    }
                }
            });
        }
    </script>
@endsection