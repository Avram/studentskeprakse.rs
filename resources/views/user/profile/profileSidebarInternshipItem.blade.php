{{--{{ dd(Request::url() === URL::route('company.internship', [$user->slug, $internship->slug])) }}--}}
<a href="{{ URL::route('company.internship', [$user->slug, $internship->slug]) }}">
    <div class="row twelve columns @if((isset($useActive) && $useActive) && (Request::url() === URL::route('company.internship', [$user->slug, $internship->slug])))active @endif internship-item">
        {{ $internship->name }}
    </div>
</a>
