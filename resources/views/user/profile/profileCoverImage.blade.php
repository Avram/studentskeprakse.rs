<div class="twelve columns cover-image">
    <div class="cover-blackbox"></div>
    @if($user->getCover())
        <div style="background: url( {{ url($user->present()->cover()) }}) "> </div>
    @else
        <div style="background: url( {{ asset('images/place.png') }}) "> </div>
    @endif
    @if(Auth::check() && (Auth::user()->id === $user->id))
        @include('components.coverDropzone', ['name' => 'cover-image'])
    @endif
</div>