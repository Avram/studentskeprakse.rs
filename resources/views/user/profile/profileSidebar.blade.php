<div class="row three columns company-panel">
    <div class="backpanel"></div>
    <div class="twelve columns filter panel">
        <div class="company-logo">
            @if($user->getLogo())
                <img src="{{ url($user->present()->logo()) }}"/>
            @else
                <img src="{{ URL::route('company.profile.dynamicLogo', [$user->getSlug()]) }}"/>
            @endif
            {{--            <img src="{{ asset('images/logo_placeholder.png') }}"/>--}}
            @if(Auth::check() && (Auth::user()->id === $user->id))
                @include('components.logoDropzone', ['name' => 'logo-image', 'username' => $user->slug])
            @endif
        </div>
        <div class="row twelve columns company-info">
            <div class="row twelve columns company-name">
                {{ $user->name }}
            </div>
            <div class="row twelve columns company-address">
                <span class="icon-pin"></span> {{ $user->address }}
            </div>
        </div>
    </div>
    <div class="row cutouts twelve columns">
        <div class="cut-out left"></div>
        <div class="cut-out right"></div>
    </div>
    <div class="twelve columns filter panel meta-panel">
        <div class="row twelve columns meta-item">
            <div class="meta-title pull-left">Zaposleni</div>
            <div class="meta-value pull-right">
                @if($user->employees)
                    {{ $user->employees }}
                @elseif((Auth::check() && (Auth::user()->id === $user->id)))
                    <a href="{{ URL::route('company.about.edit', ['username' => $user->slug, 'internship' => $internship->slug]) }}">upiši</a>
                @else
                    -
                @endif
            </div>
        </div>
        <div class="row twelve columns meta-item">
            <div class="meta-title pull-left">Osnovana</div>
            <div class="meta-value pull-right">{{ $user->founded }}</div>
        </div>
        <div class="row twelve columns meta-item">
            <div class="meta-title pull-left">Oblast</div>
            @if(isset($user->category_id))
                <div class="meta-value pull-right">{{ $user->category->name }}</div>
            @endif
        </div>
    </div>

    <div class="twelve columns filter-panel categories filter">
        {{ $user->name }} prakse
    </div>
    <div class="twelve columns filter-panel internships">
        @foreach($internships as $internship)
            @include('user.profile.profileSidebarInternshipItem', ['internship' => $internship, 'user' => $user, 'useActive' => true])
        @endforeach
    </div>

    @if(count($similar) > 0)
        <div class="twelve columns filter-panel categories filter">
            Slične prakse
        </div>
        <div class="twelve columns filter-panel internships">
            @foreach($similar as $item)
                @include('user.profile.profileSidebarInternshipItem', ['internship' => $item, 'user' => $item->user])
            @endforeach
        </div>
    @endif
</div>