<div class="row three columns company-panel">
    <div class="backpanel"></div>
    <div class="twelve columns filter panel">
        <div class="company-logo">
            @if($user->getLogo())
                <img src="{{ url($user->present()->logo()) }}"/>
            @else
                <img src="{{ URL::route('company.profile.dynamicLogo', [$user->getSlug()]) }}"/>
            @endif
            {{--            <img src="{{ asset('images/logo_placeholder.png') }}"/>--}}
            @if(Auth::check() && (Auth::user()->id === $user->id))
                @include('components.logoDropzone', ['name' => 'logo-image', 'username' => $user->slug])
            @endif
        </div>
        <div class="row twelve columns company-info">
            <div class="row twelve columns company-name">
                {{ $user->name }}
            </div>
            <div class="row twelve columns company-address">
                <span class="icon-pin"></span> {{ $user->address }}
            </div>
        </div>
    </div>
    <div class="row cutouts twelve columns">
        <div class="cut-out left"></div>
        <div class="cut-out right"></div>
    </div>
    <div class="twelve columns filter panel meta-panel">
        <div class="row twelve columns meta-item">
            <div class="meta-title pull-left">Zaposleni</div>
            <div class="meta-value pull-right">{{ $user->employees }}</div>
        </div>
        <div class="row twelve columns meta-item">
            <div class="meta-title pull-left">Osnovana</div>
            <div class="meta-value pull-right">{{ $user->founded }}</div>
        </div>
        <div class="row twelve columns meta-item">
            <div class="meta-title pull-left">Oblast</div>
            @if(isset($user->category_id))
                <div class="meta-value pull-right">{{ $user->category->name }}</div>
            @endif
        </div>
    </div>
</div>