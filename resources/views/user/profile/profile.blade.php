@extends('layouts.user')

@section('userContent')

    <div class="eight columns panel content">
        <div class="twelve columns">
            @include('components.userNav', [
                'items' => [
                    [
                        'route' => 'internship',
                        'routeParams' => ['username' => $user->slug, 'internship' => $internship->slug],
                        'active' => true,
                        'title' => 'Praksa'
                    ],
                    [
                        'route' => 'about',
                        'active' => false,
                        'title' => 'O nama'
                    ],
                    [
                        'route' => 'feedback',
                        'active' => false,
                        'title' => 'Prakse kod nas'
                    ],
                    [
                        'route' => 'offices',
                        'active' => false,
                        'title' => 'Kancelarije'
                    ],
                    [
                        'route' => 'attachments',
                        'active' => false,
                        'title' => 'Prilozi'
                    ],
                    [
                        'route' => 'faqs',
                        'active' => false,
                        'title' => 'Česta pitanja'
                    ]
                ],
                'user' => $user
            ])
            <div class="main-content">
                <div class="tab-wrapper" id="tab-wrapper-internship" style="display: block;">
                    @if($internship->isDummy())
                        Trenutno nemate napravljenu praksu. <br>
                        <a class="btn btn-info" href="{{ URL::route('internship.create') }}">Napravi</a>
                    @else
                        @include('user.profileTab', ['internship' => $internship])
                    @endif
                </div>
                <div class="tab-wrapper" id="tab-wrapper-about">
                    @include('user.about', ['user' => $user])
                </div>
                <div class="tab-wrapper" id="tab-wrapper-feedback">
                    @include('user.feedback', ['user' => $user, 'feedback' => $feedback])
                </div>
                <div class="tab-wrapper" id="tab-wrapper-offices">
                    @include('user.offices', ['offices' => $images])
                </div>
                <div class="tab-wrapper" id="tab-wrapper-attachments">
                    @include('user.attachments', ['attachments' => $attachments])
                </div>
                <div class="tab-wrapper" id="tab-wrapper-faqs">
                    @include('user.faqs', ['faqs' => $faqs])
                </div>
            </div>
        </div>
    </div>

@endsection


@section('js')
    @parent
    <script>
        function captcha () {
            grecaptcha.render('feedback', {'sitekey': '{{ $reCaptchaSiteKey }}'});
            grecaptcha.render('faqs', {'sitekey': '{{ $reCaptchaSiteKey }}'});
        };
    </script>
    <script src='https://www.google.com/recaptcha/api.js?onload=captcha&render=explicit' async defer></script>
@endsection