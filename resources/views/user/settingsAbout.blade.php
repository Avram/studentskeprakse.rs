@extends('layouts.user')

@section('userContent')
    <div class="eight columns panel content">
        <div class="twelve columns">
            @include('components.userNav',
            [
                'items' => [
                    [
                        'route' => 'settings.about',
                        'active' => true,
                        'title' => 'Ukratko o kompaniji'
                    ],
                    [
                        'route' => 'settings.social',
                        'active' => false,
                        'title' => 'Social'
                    ],
                    [
                        'route' => 'settings.profile',
                        'active' => false,
                        'title' => 'Profile'
                    ]
                ],
                'settings' => true
            ])

            <div class="twelve columns main-content settings">
                @include('user.settingsAboutForm', ['user' => $user, 'categories' => $categories, 'selectedCategory' => $selectedCategory, 'cities' => $cities, 'selectedCity' => $selectedCity])
            </div>
        </div>
    </div>
@endsection