@if(Auth::check() && (Auth::user()->id === $user->id))
    <div class="row twelve columns">
        <button class="btn btn-info"
                onclick="location.href = '{{ URL::route('company.faq.edit', ['username' => $user->slug, 'internship' => $internship->slug]) }}';">
            Izmeni
        </button>
    </div>
@endif

<div class="faqs">
    <div class="row">
        @foreach($faqs as $faq)
            @if(isset($faq->answer))
                @include('user.faqsItem', [
                    'question' => $faq->question,
                    'answer' => $faq->answer,
                ])
            @endif
        @endforeach
    </div>
    {{--@if(Auth::guest() || (Auth::user()->id !== $user->id))--}}
    @if(Auth::guest())
    <hr>
    <form action="{{ URL::route('company.faq.post', [$user->getSlug(), $internship->getSlug()]) }}" method="POST">
        {!! csrf_field() !!}
        <div class="row twelve columns">
            <div class="twelve columns form-element faqQuestionInput">
                 <textarea type="text" class="twelve columns form-element" name="question" placeholder="Postavite nam pitanje"></textarea>
                <div class="recaptcha three columns push-left">
                    @include('components.recaptcha', ['id' => 'faqs'])
                </div>
                <button type="submit" class="btn btn-info three columns">Pošalji</button>
            </div>
        </div>
    </form>
    @endif
</div>