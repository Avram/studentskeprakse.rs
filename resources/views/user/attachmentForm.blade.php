<form action="{{ URL::route('company.attachments.post', [$user->slug, $internship->slug]) }}" method="POST"
      enctype="multipart/form-data">
    {!! csrf_field() !!}
    <div class="row form-element">
        <textarea class="twelve columns" name="description" placeholder="Opis"></textarea>
    </div>

    @include('components.attachmentDropzone', ['name' => 'attachment'])

    <button type="submit" class="three columns btn btn-info">Sačuvaj</button>
</form>