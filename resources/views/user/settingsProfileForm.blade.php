<form action="" method="POST">
    {!! csrf_field() !!}
    <div class="row form-element">
        <input type="text" name="name" class="twelve columns" value="{{ $user->getName() }}" placeholder="Naziv firme"/>
    </div>

    <div class="row form-element">
        <input type="email" name="email" class="twelve columns" value="{{ $user->getEmail() }}" placeholder="Email"/>
    </div>

    <div class="row form-element">
        <input type="password" name="old-pass" class="twelve columns" placeholder="Stara lozinka"/>
    </div>

    <div class="row form-element">
        <input type="password" name="new-pass" class="twelve columns" placeholder="Nova lozinka"/>
    </div>

    <div class="row form-element">
        <input type="password" name="new-pass_confirmation" class="twelve columns" placeholder="Ponovljena lozinka"/>
    </div>

    <button type="submit" class="pull-right btn btn-info">Sačuvaj</button>

</form>