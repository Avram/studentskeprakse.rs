<div class="feedbackItem">
    <div class="row">
        <blockquote class="feedbackContent">
            <p>{{$feedback}}</p>
        </blockquote>
    </div>
    <div class="row">
        <cite class="feedbackName">{{$name}}</cite>
    </div>
    <div class="row">
        @if($reply !== null)
            <p class="feedbackReply">{{ $reply }}</p>
        @elseif((Request::url() === URL::route('company.feedback.edit', [$username, $internship])))
            <a id="reply-{{ $id }}" data-id="{{ $id }}"><b>Odgovori <span class="icon icon-reply"></span></b></a>
            {{--<a id="report" data-id="{{ $id }}"><b>Prijavi <span class="icon icon-reply"></span></b></a>--}}
            <div class="row reply-form reply-form-{{ $id }}">
                <form action="" method="POST" id="reply-form-{{ $id }}" onsubmit="return false;">
                    <div class="row form-element">
                        <textarea class="twelve columns" name="reply" id="reply" placeholder="Odgovor"></textarea>
                    </div>
                    <div class="row form-element">
                        <button type="button" id="cancel-{{ $id }}" data-id="{{ $id }}" class="btn btn-danger pull-right">Otkaži</button>
                        <button type="submit" id="submit-{{ $id }}" data-id="{{ $id }}" class="btn btn-info pull-right">Odgovori</button>
                    </div>
                </form>
            </div>
        @endif
    </div>
</div>