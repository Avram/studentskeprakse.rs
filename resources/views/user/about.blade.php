@if(Auth::check() && (Auth::user()->id === $user->id))
    <div class="row twelve columns">
        <button class="btn btn-info"
                onclick="location.href = '{{ URL::route('company.about.edit', ['username' => $user->slug, 'internship' => $internship->slug]) }}';">
            Izmeni
        </button>
    </div>

@endif

<div class="about">
    {!! $user->present()->description() !!}
</div>

<div class="row twelve columns">
    <div class="three columns">
        <a href="{{ $user->getUrl() }}" target="_blank">st<img src="" alt=""></a>
    </div>
    <div class="three columns">
        <a href="{{ $user->getFacebookUrl() }}" target="_blank">fb<img src="" alt=""></a>
    </div>
    <div class="three columns">
        <a href="{{ $user->getTwitterUrl() }}" target="_blank">tw<img src="" alt=""></a>
    </div>
    <div class="two columns">
        <a href="{{ $user->getLinkedinUrl() }}" target="_blank">li<img src="" alt=""></a>
    </div>
</div>