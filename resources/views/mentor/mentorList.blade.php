@extends('layouts.app')

@section('content')
    <div class="container">
        @include('components.managementNav', [
            'items' => [
                [
                    'title' => 'Prakse',
                    'active' => false,
                    'route' => 'company.internships.management'
                ],
                [
                    'title' => 'Mentori',
                    'active' => true,
                    'route' => 'company.mentors.management'
                ],
                [
                    'title' => 'Česta pitanja',
                    'active' => false,
                    'route' => 'company.faq.management'
                ],
                [
                    'title' => 'Prakse kod nas',
                    'active' => false,
                    'route' => 'company.feedback.management'
                ],
                [
                    'title' => 'Obrisano',
                    'active' => false,
                    'route' => 'company.internships.trash'
                ]
            ],
            'btn' => [
                'has' => true,
                'route' => 'company.mentors.management.create',
                'icon' => true,
                'title' => 'Napravi'
            ]
        ])
        <div class="row twelve columns">
            @foreach($mentors as $mentor)
                @include('mentor.mentorListItem', [
                    'title' => $mentor->name,
                    'description' => $mentor->description,
                    'id' => $mentor->id
                ])
            @endforeach
        </div>
    </div>
@endsection