@extends('layouts.app')

@section('content')
    <div class="container panel internship-create">
        <form class="internship-form" method="POST" action="">
            {!! csrf_field() !!}
            <div class="row">
                <div class="eleven columns form-element">
                    <input type="text" class="twelve columns" name="name" value="{{ $mentor->getName() }}"
                           placeholder="Ime i prezime">
                </div>
            </div>
            <div class="row">
                <div class="eleven columns form-element">
                    <textarea class="twelve columns" placeholder="Opis"
                              name="description">{{ $mentor->getDescription() }}</textarea>
                </div>
            </div>
            <div class="row">
                <div class="eleven columns form-element">
                    <input type="url" class="twelve columns joined-left" name="url" value="{{ $mentor->getUrl() }}"
                           placeholder="Blog stranica">
                </div>
            </div>
            <div class="row">
                <div class="eleven columns form-element">
                    <input type="url" class="twelve columns joined-left" name="facebook_url"
                           value="{{ $mentor->getFacebookUrl() }}" placeholder="Facebook stranica">
                </div>
            </div>
            <div class="row">
                <div class="eleven columns form-element">
                    <input type="url" class="twelve columns joined-left" name="twitter_url"
                           value="{{ $mentor->getTwitterUrl() }}" placeholder="Twitter stranica">
                </div>
            </div>
            <div class="row">
                <div class="eleven columns form-element">
                    <input type="url" class="twelve columns joined-left" name="linkedin_url"
                           value="{{ $mentor->getLinkedinUrl() }}" placeholder="Linkedin stranica">
                </div>
            </div>
            <div class="row submit">
                <button type="submit" class="two columns btn btn-info">Sačuvaj</button>
            </div>
        </form>
    </div>
@endsection