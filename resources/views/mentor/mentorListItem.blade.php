<div class="panel item">
    <div class="row header">
        <h3 class="pull-left">
            {{ $title }}
        </h3>
    </div>
    <div class="row meta">
        <div class="pull-left">
            {{ $description }}
        </div>
        <div class="options pull-right">
            <a href="{{ URL::route('company.mentors.management.delete', [$id]) }}"><span class="icon-trash"></span></a>
            <a href="{{ URL::route('company.mentors.management.update', [$id]) }}"><span class="icon-pen"></span></a>
        </div>
    </div>
</div>