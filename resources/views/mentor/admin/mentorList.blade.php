@extends('layouts.app')

@section('content')

    <ul>
        @foreach($mentors as $mentor)
            <li> {{ $mentor->getName()  }} |<a href="{{ URL::route('admin.mentor.update', [$mentor->getId()]) }}">
                    <small>(update)</small>
                </a> <a href="{{ URL::route('admin.mentor.delete', [$mentor->getId()]) }}">
                    <small>(delete)</small>
                </a></li>
        @endforeach

    </ul>

@endsection