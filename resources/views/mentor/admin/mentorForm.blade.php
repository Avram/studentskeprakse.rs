@extends('layouts.app')

@section('content')

    <form method="POST" action="">
        {!! csrf_field() !!}
        <input type="text" name="name" value="{{ $mentor->getName() }}"><br/>
        <textarea name="description">{{ $mentor->getDescription() }}</textarea><br/>
        <input type="url" name="url" value="{{ $mentor->getUrl() }}"><br/>
        <input type="url" name="facebook_url" value="{{ $mentor->getFacebookUrl() }}"><br/>
        <input type="url" name="twitter_url" value="{{ $mentor->getTwitterUrl() }}"><br/>
        <input type="url" name="linkedin_url" value="{{ $mentor->getLinkedinUrl() }}"><br/>
        <input type="submit" value="Submit">
    </form>

@endsection