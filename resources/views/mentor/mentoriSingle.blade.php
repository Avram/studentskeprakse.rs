@extends('layouts.app')

@section('content')

    <h1>{{ $post->title }}</h1>
    <p>
        {{ $post->content }}
    </p>

    <form method="POST" action="{{ URL::route('comment.create', [$post->id]) }}">
        {!! csrf_field() !!}
        <input type="text" name="name"><br/>
        <textarea name="content"></textarea><br/>
        <input type="submit" value="Submit">
    </form>
    <hr>
    @foreach($post->comments as $comment)
        <div>
            <div> name: {{ $comment->name }}</div>
            <div> content: {{ $comment->content }}</div>
            <hr>
        </div>
    @endforeach


@endsection