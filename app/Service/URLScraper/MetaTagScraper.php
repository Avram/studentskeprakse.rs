<?php

namespace StudentskePrakse\Service\URLScraper;


abstract class MetaTagScraper extends BaseURLScraper
{

    public function getMetaContentByAttr($attributeName, $attributeNameValue)
    {
        $metaNodeList = $this->domCrawler->filter('meta');
//@TODO for each
        for ($i = 0; $i < $metaNodeList->count(); $i++) {
            $meta = $metaNodeList->getNode($i);
            $attr = $meta->getAttribute($attributeName);

            if ($attr === $attributeNameValue) {
                return $meta->getAttribute('content');
            }

        }

        return NULL;
    }
}