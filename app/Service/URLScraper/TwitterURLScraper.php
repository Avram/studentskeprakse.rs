<?php

namespace StudentskePrakse\Service\URLScraper;

class TwitterURLScraper extends MetaTagScraper
{

    public function extractTitle()
    {
        return $this->getMetaContentByAttr('name', 'twitter:title');
    }

    public function extractImageUrl()
    {
        return $this->getMetaContentByAttr('name', 'twitter:image:src');
    }

    public function extractDescription()
    {
        return $this->getMetaContentByAttr('name', 'twitter:description');
    }
}