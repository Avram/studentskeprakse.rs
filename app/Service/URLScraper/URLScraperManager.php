<?php

namespace StudentskePrakse\Service\URLScraper;


class URLScraperManager
{
    /**
     * @var array
     */
    protected $scrapersArray = [];

    public function __construct(array $scrapers)
    {
        $this->addScrapers($scrapers);
    }

    public function addScraper(BaseURLScraper $scraper)
    {
        $this->scrapersArray[] = $scraper;
    }

    public function addScrapers(array $scrapers)
    {
        foreach ($scrapers as $scraper) {
            $this->addScraper($scraper);
        }
    }

    public function extractTitle()
    {
        foreach ($this->scrapersArray as $scraper) {
            /** @var BaseURLScraper $scraper */
            $title = $scraper->extractTitle();
            if ($title !== null) {
                return $title;
            }
        }
    }

    public function extractImage()
    {
        foreach ($this->scrapersArray as $scraper) {
            /** @var BaseURLScraper $scraper */
            $image = $scraper->extractImageUrl();
            if ($image !== null) {
                return $image;
            }
        }
    }

    public function extractDescription()
    {
        foreach ($this->scrapersArray as $scraper) {
            /** @var BaseURLScraper $scraper */
            $description = $scraper->extractDescription();
            if ($description !== null) {
                return $description;
            }
        }
    }

    public function setURL($url)
    {
        $testURL = filter_var($url, FILTER_VALIDATE_URL);

        if (!$testURL) {
            throw new \InvalidArgumentException('$url is not valid URL!');
        }

        $html = file_get_contents($url);

        foreach ($this->scrapersArray as $scraper) {
            /** @var BaseURLScraper $scraper */
            $scraper->setURL($url);
            $scraper->loadHTML($html);
        }
    }

    public function getURLPreview()
    {
        $output                = [];
        $output['title']       = $this->extractTitle();
        $output['image']       = $this->extractImage();
        $output['description'] = $this->extractDescription();

        return $output;
    }


}