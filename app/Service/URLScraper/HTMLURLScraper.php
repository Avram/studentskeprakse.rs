<?php

namespace StudentskePrakse\Service\URLScraper;

class HTMLURLScraper extends BaseURLScraper
{

    /**
     * @return null|string
     */
    public function extractTitle()
    {
        $title = $this->domCrawler->filter('title');
        if ($title->count() === 0) {
            return null;
        }

        return $title->text();
    }

    /**
     * @return null|string
     */
    public function extractImageUrl()
    {
        $image = $this->domCrawler->filter('img');
        if ($image->count() > 0) {
            return $this->makeImageURL($image->getNode(0)->getAttribute('src'));
        }

        return null;
    }

    /**
     * @return null
     */
    public function extractDescription()
    {
        $paragraphs = $this->domCrawler->filter('p');

        if ($paragraphs->count() === 0) {
            return null;
        }

        $paragraphTexts = array_filter(array_map(function (\DOMElement $paragraph = null) {
            if ($paragraph === null) {
                return null;
            }

            return $paragraph->textContent;
        }, $paragraphs->getIterator()->getArrayCopy()));

        if (empty($paragraphTexts)) {
            return null;
        }

        // Sort the strings by their length.
        usort($paragraphTexts, function ($a, $b) {
            return strlen($b) - strlen($a);
        });

        return $paragraphTexts[0];

    }

    /**
     * @param $uri
     *
     * @return string
     */
    protected function makeImageURL($uri)
    {

        $testURI = filter_var($uri, FILTER_VALIDATE_URL);

        if ($testURI) {
            return $uri;
        }

        if (strpos($uri, '/') === 0) {
            $urlArray = parse_url($this->url);
            return $urlArray['scheme'].'://'.$urlArray['host'].$uri;
        }

        $urlArray = explode('/', $this->url);
        array_pop($urlArray);
        return implode('/', $urlArray).'/'.$uri;
    }

}