<?php

namespace StudentskePrakse\Service\URLScraper;

use Symfony\Component\DomCrawler\Crawler;

abstract class BaseURLScraper
{
    /**
     * @var Crawler
     */
    protected $domCrawler;

    /**
     * @var string
     */
    protected $url;

    public abstract function extractTitle();

    public abstract function extractImageUrl();

    public abstract function extractDescription();

    /**
     * @param $url
     */
    public function setURL($url)
    {
        $this->url = $url;
    }

    /**
     * @param $html
     */
    public function loadHTML($html)
    {
        $this->domCrawler = new Crawler();
        $this->domCrawler->addHtmlContent($html);
    }
}