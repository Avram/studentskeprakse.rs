<?php

namespace StudentskePrakse\Service\URLScraper;

class FacebookURLScraper extends MetaTagScraper
{

    public function extractTitle()
    {
        return $this->getMetaContentByAttr('property', 'og:title');
    }

    public function extractImageUrl()
    {
        return $this->getMetaContentByAttr('property', 'og:image');
    }

    public function extractDescription()
    {
        return $this->getMetaContentByAttr('property', 'og:description');
    }
}