<?php

namespace StudentskePrakse\Service\URLScraper;

class GooglePlusURLScraper extends MetaTagScraper
{

    public function extractTitle()
    {
        return $this->getMetaContentByAttr('itemprop', 'name');
    }

    public function extractImageUrl()
    {
        return $this->getMetaContentByAttr('itemprop', 'image');
    }

    public function extractDescription()
    {
        return $this->getMetaContentByAttr('itemprop', 'description');
    }
}