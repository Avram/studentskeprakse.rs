<?php namespace StudentskePrakse\Service;

use StudentskePrakse\Model\User;

class DynamicLogo
{

    public function makeLogo(User $user)
    {
        $name = $user->getName();

        $hex = md5($name);

        $r = hexdec(substr($hex, 0, 2));
        $g = hexdec(substr($hex, 2, 2));
        $b = hexdec(substr($hex, 4, 2));

        $img  = imagecreatetruecolor(150, 150);
        $back = imagecolorallocate($img, $r, $g, $b);
        $fore = imagecolorallocate($img, 255, 255, 255);
        imagefill($img, 1, 1, $back);

        $font = resource_path('assets/fonts/Roboto-Regular.ttf');

        $firstLetter = substr(strtoupper($name), 0, 1);

        $bbox   = imagettfbbox(35, 0, $font, $firstLetter);
        $width  = $bbox[4] - $bbox[6];
        $height = $bbox[5] - $bbox[3];

        imagettftext($img, 35, 0, 75 - round($width / 2), 75 - round($height / 2), $fore, $font, $firstLetter);

        return $img;
    }


}