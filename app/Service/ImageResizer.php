<?php namespace StudentskePrakse\Service;

class ImageResizer
{

    /** @var resource $image */
    protected $image = null;

    /** @var string $outputFileName */
    protected $outputFileName = null;

    /**
     * ImageResizer constructor.
     *
     * @param string $filename
     */
    public function __construct($filename = null)
    {
        if (!empty($filename)) {
            $this->loadImageFromFile($filename);
            $this->outputFileName = $filename;
        }
    }

    /**
     * @param string $filename
     */
    public function loadImageFromFile($filename)
    {
        $this->image = $this->imageCreateFromFile($filename);
    }

    /**
     * @param string $filename
     */
    public function setOutputFileName($filename)
    {
        $this->outputFileName = $filename;
    }

    /**
     * @return string
     */
    public function getOutputFilename()
    {
        return $this->outputFileName;
    }

    /**
     * @param string $imageString
     */
    public function loadImageFromString($imageString)
    {
        $img = imagecreatefromstring($imageString);
        if (!is_resource($img)) {
            throw new \InvalidArgumentException("Provided string is not a valid binary image data!");
        }

        $this->image = $img;
    }

    /**
     * @param integer $thumb_width
     * @param integer $thumb_height
     *
     * @return resource
     * @throws \Exception
     */
    public function imageResizeCrop($thumb_width, $thumb_height = 0)
    {
        if (empty($this->image)) {
            throw new \Exception("No image loaded!");
        }

        //if thumb height is 0, auto-calculate it!
        if ($thumb_height == 0) {
            $xy           = $this->imageCalculateSize($thumb_width);
            $thumb_height = $xy[1];
        }

        $width  = imagesx($this->image);
        $height = imagesy($this->image);

        $original_aspect = $width / $height;
        $thumb_aspect    = $thumb_width / $thumb_height;

        if ($original_aspect >= $thumb_aspect) {
            // If image is wider than thumbnail (in aspect ratio sense)
            $new_height = $thumb_height;
            $new_width  = $width / ($height / $thumb_height);
        } else {
            // If the thumbnail is wider than the image
            $new_width  = $thumb_width;
            $new_height = $height / ($width / $thumb_width);
        }

        $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

        // Resize and crop
        imagecopyresampled($thumb,
            $this->image,
            0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
            0 - ($new_height - $thumb_height) / 2, // Center the image vertically
            0, 0,
            $new_width, $new_height,
            $width, $height);

        $this->saveImageToFile($this->getOutputFilename());

        return $this->image = $thumb;

    }

    /**
     * @param integer $width
     * @param integer $height
     *
     * @return resource
     * @throws \Exception
     */
    public function imageResize($width, $height = 0)
    {
        if (empty($this->image)) {
            throw new \Exception("No image loaded!");
        }

        $srcWidth  = imagesx($this->image);
        $srcHeight = imagesy($this->image);

        list($dstWidth, $dstHeight) = $this->imageCalculateSize($width, $height);

        //resize
        $dstImage = imagecreatetruecolor($dstWidth, $dstHeight);
        imagecopyresampled($dstImage, $this->image, 0, 0, 0, 0, $dstWidth, $dstHeight, $srcWidth, $srcHeight);

        $this->saveImageToFile($this->getOutputFilename());

        return $this->image = $dstImage;
    }

    /**
     * @param integer $width
     * @param integer $height
     *
     * @return resource
     * @throws \Exception
     */
    public function imageResizeTransparent($width, $height = 0)
    {
        if (empty($this->image)) {
            throw new \Exception("No image loaded!");
        }

        $srcWidth  = imagesx($this->image);
        $srcHeight = imagesy($this->image);

        list($dstWidth, $dstHeight) = $this->imageCalculateSize($width, $height);

        //resize
        $dstImage = imagecreatetruecolor($dstWidth, $dstHeight);
        imagealphablending($dstImage, false);
        imagesavealpha($dstImage, true);
        $transparent = imagecolorallocatealpha($dstImage, 255, 255, 255, 127);
        imagefilledrectangle($dstImage, 0, 0, $dstWidth, $dstHeight, $transparent);

        imagecopyresampled($dstImage, $this->image, 0, 0, 0, 0, $dstWidth, $dstHeight, $srcWidth, $srcHeight);

        $this->saveImageToFile($this->getOutputFilename());

        return $this->image = $dstImage;
    }

    /**
     * @param integer $dst_w
     * @param integer $dst_h
     *
     * @return array
     * @throws \Exception
     */
    protected function imageCalculateSize($dst_w, $dst_h = 0)
    {

        if (empty($this->image)) {
            throw new \Exception("No image loaded!");
        }

        if (is_resource($this->image)) {
            $w = imagesx($this->image);
            $h = imagesy($this->image);
        } else {
            list($w, $h) = getimagesize($this->image);
        }

        if ($w == 0 and $h == 0) return array(0, 0);
        if ($dst_w == 0 and $dst_h == 0) return array($w, $h);

        if ($dst_w == 0) {
            //autocalculate width
            $retH       = $dst_h;
            $proportion = $h / $dst_h;
            $retW       = round($w / $proportion);
        } else if ($dst_h == 0) {
            //autocalculate height
            $retW       = $dst_w;
            $proportion = $w / $dst_w;
            $retH       = round($h / $proportion);
        } else {
            $retW = $dst_w;
            $retH = $dst_h;
        }

        return array($retW, $retH);
    }

    /**
     * @param string $file
     *
     * @return resource
     */
    protected function imageCreateFromFile($file)
    {
        $info = pathinfo($file);
        $ext  = isset($info['extension']) ? strtolower($info['extension']) : false;
        if (!file_exists($file)) {
            $ext = false;
        } //function returns false
        switch ($ext) {
            case 'jpg' :
                $image = imagecreatefromjpeg($file);
                break;
            case 'jpeg' :
                $image = imagecreatefromjpeg($file);
                break;
            case 'gif' :
                $image = imagecreatefromgif($file);
                break;
            case 'png' :
                $image = imagecreatefrompng($file);
                break;
            case 'xmp' :
                $image = imagecreatefromxpm($file);
                break;
            case 'xbm' :
                $image = imagecreatefromxbm($file);
                break;
            case 'wbmp' :
                $image = imagecreatefromwbmp($file);
                break;
            default :
                $image = false;
        }
        return $image;
    }

    /**
     * @param string  $file
     * @param integer $jpegQuality
     *
     * @return bool
     */
    public function saveImageToFile($file, $jpegQuality = 90)
    {
        $arr    = explode('.', $file);
        $ext    = end($arr);
        $ext    = strtolower($ext);
        $return = true;

        switch ($ext) {
            case 'jpg' :
            case 'jpeg' :
                imagejpeg($this->image, $file, $jpegQuality);
                break;
            case 'gif' :
                imagegif($this->image, $file);
                break;
            case 'png' :
                imagepng($this->image, $file);
                break;
            case 'wbmp' :
                imagewbmp($this->image, $file);
                break;
            default :
                $return = false;
        }

        return $return;
    }

    /**
     * @param string  $type
     * @param boolean $sendHeader
     */
    public function imageToStream($type = 'jpg', $sendHeader = true)
    {
        if ($sendHeader) {
            header("Content-Type: image/".($type == 'jpg') ? 'jpeg' : $type);
        }

        switch ($type) {
            case 'jpg' :
            case 'jpeg' :
                imagejpeg($this->image);
                break;
            case 'gif' :
                imagegif($this->image);
                break;
            case 'png' :
                imagepng($this->image);
                break;
            case 'wbmp' :
                imagewbmp($this->image);
                break;
            default :
                throw new \InvalidArgumentException("Unknown image type. Supported types are: jpg, gif, png and wbmp");
        }
    }


}