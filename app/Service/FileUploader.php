<?php namespace StudentskePrakse\Service;

use Carbon\Carbon;
use StudentskePrakse\Exceptions\InvalidAttachmentException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    /**
     * @var Carbon
     */
    protected $carbon;

    /**
     * @var UploadedFile
     */
    protected $file;

    /**
     * @var string
     */
    protected $filename;

    /**
     * @var bool
     */
    protected $randomize = true;

    /**
     * @var string
     */
    protected $baseDir = 'uploads';

    public function __construct()
    {
        $this->carbon = Carbon::now();
    }

    /**
     * @return array
     */
    public function getAllowedMimes()
    {
        return ['image/jpeg', 'image/png', 'image/gif'];
    }

    /**
     * @param Carbon $carbon
     */
    public function setCarbonObject(Carbon $carbon)
    {
        $this->carbon = $carbon;
    }

    /**
     * @param UploadedFile $file
     */
    public function setUploadedFile(UploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * @param string $filename
     */
    public function setFileName($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @param string $baseDir
     */
    public function setUploadDir($baseDir = 'uploads')
    {
        $this->baseDir = $baseDir;
    }

    /**
     * @param bool $randomize
     */
    public function randomizeName($randomize = true)
    {
        $this->randomize = $randomize;
    }

    /**
     * @return bool
     */
    public function isValidMime()
    {
        return in_array($this->file->getMimeType(), $this->getAllowedMimes());
    }

    /**
     * @return UploadedFile
     */
    public function file()
    {
        return $this->file;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return $this->file->isValid() && $this->isValidMime();
    }

    public function getUploadDir()
    {
        return $this->baseDir;
    }

    /**
     * @return string
     */
    public function getUploadDirPath()
    {
        return $this->getUploadDir().DIRECTORY_SEPARATOR.$this->carbon->year.DIRECTORY_SEPARATOR.$this->carbon->month;
    }

    /**
     * @return File
     * @throws InvalidAttachmentException
     */
    public function upload()
    {
        $uploadDir = $this->getUploadDirPath();
        $fileName  = $this->file->getClientOriginalName();
        $extension = $this->file->getClientOriginalExtension();
        $bareName  = str_replace_last('.'.$extension, '', $fileName);

        $asFileName = ($this->filename == null) ? str_slug($bareName) : $this->filename;

        if ($this->randomize) {
            $asFileName .= '-'.rand(11111, 99999);
        }

        $asFileName .= '.'.$extension;

        if (!$this->isValidMime()) {
            throw new InvalidAttachmentException("Uploaded file is not of allowed type!");
        }

        if (!$this->file->isValid()) {
            throw new InvalidAttachmentException("Uploaded file is not valid!");
        }

        if (!is_dir($uploadDir)) {
            mkdir($uploadDir, 0777, true);
        }

        return $this->file->move($uploadDir, $asFileName);

    }
}