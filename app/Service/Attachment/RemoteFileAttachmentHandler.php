<?php namespace StudentskePrakse\Service\Attachment;

use StudentskePrakse\Exceptions\InvalidAttachmentException;
use StudentskePrakse\Model\Attachment;
use StudentskePrakse\Service\AttachmentUploader;
use StudentskePrakse\Service\ImageResizer;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class RemoteFileAttachmentHandler implements AttachmentHandlerInterface
{

    /**
     * @var AttachmentUploader
     */
    private $uploader;
    /**
     * @var ImageResizer
     */
    private $resizer;

    /**
     * FileAttachment constructor.
     *
     * @param AttachmentUploader $uploader
     * @param ImageResizer       $resizer
     */
    public function __construct(AttachmentUploader $uploader, ImageResizer $resizer)
    {
        $this->uploader = $uploader;
        $this->resizer  = $resizer;
    }

    /**
     * @param string|UploadedFile $source
     * @param array|null          $extras
     *
     * @return Attachment
     * @throws InvalidAttachmentException
     */
    public function handle($source, array $extras = null)
    {
        $attachment = new Attachment();
        $mime       = $extras['mime'];

        switch ($mime) {
            case 'image/jpeg':
                $ext = 'jpg';
                break;
            case 'image/png':
                $ext = 'png';
                break;
            case 'image/gif':
                $ext = 'gif';
                break;
            case 'image/svg+xml':
                $ext = 'svg';
                break;
            default:
                $ext = 'jpg';
        }

        $uploadDir = $this->uploader->getUploadDirPath();
        $baseName  = md5($source).'.'.$ext;
        file_put_contents($uploadDir.DIRECTORY_SEPARATOR.$baseName, fopen($source, 'r'));

        $attachment->setAttachment($baseName);

        if (strpos($mime, 'image') === 0) {
            //image
            $attachment->setType(Attachment::TYPE_IMAGE);
            $this->resizer->loadImageFromFile($uploadDir.DIRECTORY_SEPARATOR.$baseName);
            $this->resizer->setOutputFileName($uploadDir.DIRECTORY_SEPARATOR.'thumb-'.$baseName);
            $this->resizer->imageResizeCrop(300, 200);
        } elseif ($mime == 'application/pdf') {
            //pdf
            $attachment->setType(Attachment::TYPE_PDF);
        } else {
            throw new InvalidAttachmentException("Invalid file uploaded");
        }

        return $attachment;
    }
}