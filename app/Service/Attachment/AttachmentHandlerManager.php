<?php namespace StudentskePrakse\Service\Attachment;


use StudentskePrakse\Exceptions\InvalidAttachmentException;
use StudentskePrakse\Model\Attachment;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AttachmentHandlerManager
{

    /**
     * @var URLAttachmentHandler
     */
    private $URLAttachment;

    /**
     * @var FileAttachmentHandler
     */
    private $fileAttachment;
    /**
     * @var RemoteFileAttachmentHandler
     */
    private $remoteAttachment;

    /**
     * AttachmentManager constructor.
     *
     * @param FileAttachmentHandler       $fileAttachment
     * @param URLAttachmentHandler        $URLAttachment
     * @param RemoteFileAttachmentHandler $remoteAttachment
     */
    public function __construct(FileAttachmentHandler $fileAttachment, URLAttachmentHandler $URLAttachment, RemoteFileAttachmentHandler $remoteAttachment)
    {
        $this->fileAttachment   = $fileAttachment;
        $this->URLAttachment    = $URLAttachment;
        $this->remoteAttachment = $remoteAttachment;
    }

    /**
     * @param UploadedFile $file
     * @param string       $description
     *
     * @return Attachment
     * @throws InvalidAttachmentException
     */
    public function handleFile(UploadedFile $file, $description)
    {
        return $this->fileAttachment->handle($file, ['value' => $description]);
    }

    /**
     * @param $url
     *
     * @return Attachment
     */
    public function handleURL($url)
    {
        $headers     = get_headers($url, true);
        $parts       = explode(';', $headers['Content-Type']);
        $contentType = reset($parts);

        //allow linking of images/pdfs/etc...
        if ($contentType !== 'text/html') {
            return $this->remoteAttachment->handle($url, ['mime' => $contentType]);
        }

        return $this->URLAttachment->handle($url, ['mime' => $contentType]);
    }
}