<?php namespace StudentskePrakse\Service\Attachment;

use StudentskePrakse\Model\Attachment;
use StudentskePrakse\Service\AttachmentUploader;
use StudentskePrakse\Service\URLScraper\URLScraperManager;

class URLAttachment
{
    /**
     * @var AttachmentUploader
     */
    private $uploader;

    /**
     * @var URLScraperManager
     */
    private $scraper;

    /**
     * URLAttachment constructor.
     *
     * @param AttachmentUploader $uploader
     * @param URLScraperManager  $scraper
     */
    public function __construct(AttachmentUploader $uploader, URLScraperManager $scraper)
    {
        $this->uploader = $uploader;
        $this->scraper  = $scraper;
    }

    /**
     * @param $url
     *
     * @return Attachment
     */
    public function handle($url)
    {
        $attachment = new Attachment();
        $attachment->setURL($url);

        $this->scraper->setURL($url);

        $attachment->setType(Attachment::TYPE_LINK);
        $attachment->setTitle($this->scraper->extractTitle());
        $attachment->setValue($this->scraper->extractDescription());

        $uploadDir = $this->uploader->getUploadDirPath();

        if (!is_dir($uploadDir)) {
            mkdir($uploadDir, 0777, true);
        }

        $imageURL = $this->scraper->extractImage();
        $baseName = $this->safeDownloadImage($imageURL, $uploadDir);

        if ($baseName !== null) {
            $attachment->setAttachment($baseName);
        }

        $host = parse_url($url, PHP_URL_HOST);

        if (strpos($host, 'youtube.com') !== false || strpos($host, 'youtu.be') !== false) {
            //youtube
            $attachment->setType(Attachment::TYPE_VIDEO);

        } elseif (strpos($host, 'vimeo.com') !== false) {
            //vimeo
            $attachment->setType(Attachment::TYPE_VIDEO);
        }

        return $attachment;
    }

    /**
     * @param string $url
     * @param string $uploadDir
     *
     * @return null|string
     */
    protected function safeDownloadImage($url, $uploadDir)
    {
        $headers     = get_headers($url, true);
        $parts       = explode(';', $headers['Content-Type']);
        $contentType = reset($parts);

        if (strpos($contentType, 'image') !== 0) {
            return null;
        }

        $image    = file_get_contents($url);
        $parts    = explode('.', $url);
        $ext      = end($parts);
        $baseName = md5($url).'.'.$ext;
        file_put_contents($uploadDir.DIRECTORY_SEPARATOR.$baseName, $image);
        return $baseName;
    }
}