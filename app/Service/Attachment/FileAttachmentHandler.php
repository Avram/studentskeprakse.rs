<?php namespace StudentskePrakse\Service\Attachment;

use StudentskePrakse\Exceptions\InvalidAttachmentException;
use StudentskePrakse\Model\Attachment;
use StudentskePrakse\Service\AttachmentUploader;
use StudentskePrakse\Service\ImageResizer;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileAttachmentHandler implements AttachmentHandlerInterface
{

    /**
     * @var AttachmentUploader
     */
    private $uploader;
    /**
     * @var ImageResizer
     */
    private $resizer;

    /**
     * FileAttachment constructor.
     *
     * @param AttachmentUploader $uploader
     * @param ImageResizer       $resizer
     */
    public function __construct(AttachmentUploader $uploader, ImageResizer $resizer)
    {
        $this->uploader = $uploader;
        $this->resizer  = $resizer;
    }

    /**
     * @param UploadedFile $source
     * @param array|null   $extras
     *
     * @return Attachment
     * @throws InvalidAttachmentException
     */
    public function handle($source, array $extras = null)
    {
        $attachment = new Attachment();
        $mime       = $source->getMimeType();

        $this->uploader->setUploadedFile($source);
        $localFile = $this->uploader->upload();
        $attachment->setAttachment($localFile->getBasename());

        if (is_array($extras) && !empty($extras['value'])) {
            $attachment->setValue($extras['value']);
        }

        if (strpos($mime, 'image') === 0) {
            //image
            $attachment->setType(Attachment::TYPE_IMAGE);
            $this->resizer->loadImageFromFile($localFile->getPathname());
            $this->resizer->setOutputFileName($this->uploader->getUploadDirPath().DIRECTORY_SEPARATOR.'thumb-'.$localFile->getBasename());
            $this->resizer->imageResizeCrop(300, 200);
        } elseif ($mime == 'application/pdf') {
            //pdf
            $attachment->setType(Attachment::TYPE_PDF);
        } else {
            throw new InvalidAttachmentException("Invalid file uploaded");
        }

        return $attachment;
    }
}