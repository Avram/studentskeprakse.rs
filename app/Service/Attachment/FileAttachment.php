<?php namespace StudentskePrakse\Service\Attachment;

use StudentskePrakse\Http\Controllers\InvalidAttachmentException;
use StudentskePrakse\Model\Attachment;
use StudentskePrakse\Service\AttachmentUploader;
use StudentskePrakse\Service\ImageResizer;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileAttachment
{

    /**
     * @var AttachmentUploader
     */
    private $uploader;
    /**
     * @var ImageResizer
     */
    private $resizer;

    /**
     * FileAttachment constructor.
     *
     * @param AttachmentUploader $uploader
     * @param ImageResizer       $resizer
     */
    public function __construct(AttachmentUploader $uploader, ImageResizer $resizer)
    {
        $this->uploader = $uploader;
        $this->resizer  = $resizer;
    }

    /**
     * @param UploadedFile $file
     * @param string|null  $description
     *
     * @return Attachment
     * @throws InvalidAttachmentException
     * @throws \Exception
     */
    public function handle(UploadedFile $file, $description = null)
    {
        $attachment = new Attachment();
        $mime       = $file->getMimeType();

        $this->uploader->setUploadedFile($file);
        $localFile = $this->uploader->upload();
        $attachment->setAttachment($localFile->getBasename());

        if (!empty($description)) {
            $attachment->setValue($description);
        }

        if (strpos($mime, 'image') === 0) {
            //image
            $attachment->setType(Attachment::TYPE_IMAGE);
            $this->resizer->loadImageFromFile($localFile->getPathname());
            $this->resizer->imageResizeCrop(300, 200);
            $this->resizer->saveImageToFile($this->uploader->getUploadDirPath().DIRECTORY_SEPARATOR.'thumb-'.$localFile->getBasename());
        } elseif ($mime == 'application/pdf') {
            //pdf
            $attachment->setType(Attachment::TYPE_PDF);
        } else {
            throw new InvalidAttachmentException("Invalid file uploaded");
        }

        return $attachment;
    }
}