<?php namespace StudentskePrakse\Service\Attachment;


use StudentskePrakse\Http\Controllers\InvalidAttachmentException;
use StudentskePrakse\Model\Attachment;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AttachmentManager
{

    /**
     * @var URLAttachment
     */
    private $URLAttachment;

    /**
     * @var FileAttachment
     */
    private $fileAttachment;

    /**
     * AttachmentManager constructor.
     *
     * @param FileAttachment $fileAttachment
     * @param URLAttachment  $URLAttachment
     */
    public function __construct(FileAttachment $fileAttachment, URLAttachment $URLAttachment)
    {
        $this->fileAttachment = $fileAttachment;
        $this->URLAttachment  = $URLAttachment;
    }

    /**
     * @param UploadedFile $file
     * @param              $description
     *
     * @return Attachment
     * @throws InvalidAttachmentException
     */
    public function handleFile(UploadedFile $file, $description)
    {
        return $this->fileAttachment->handle($file, $description);
    }

    /**
     * @param $url
     *
     * @return Attachment
     */
    public function handleURL($url)
    {
        $headers     = get_headers($url, true);
        $parts       = explode(';', $headers['Content-Type']);
        $contentType = reset($parts);

        //don't allow linking of images/pdfs/etc...
        if ($contentType !== 'text/html') {
            //@TODO: handle download of images/pdfs
            $attachment = new Attachment();
            $attachment->setType(Attachment::TYPE_TEXT);
            $attachment->setValue($url);
            return $attachment;
        }

        return $this->URLAttachment->handle($url);
    }
}