<?php namespace StudentskePrakse\Service\Attachment;

use StudentskePrakse\Model\Attachment;
use StudentskePrakse\Service\AttachmentUploader;
use StudentskePrakse\Service\URLScraper\URLScraperManager;

class URLAttachmentHandler implements AttachmentHandlerInterface
{
    /**
     * @var AttachmentUploader
     */
    private $uploader;

    /**
     * @var URLScraperManager
     */
    private $scraper;

    /**
     * URLAttachment constructor.
     *
     * @param AttachmentUploader $uploader
     * @param URLScraperManager  $scraper
     */
    public function __construct(AttachmentUploader $uploader, URLScraperManager $scraper)
    {
        $this->uploader = $uploader;
        $this->scraper  = $scraper;
    }

    /**
     * @param string     $source
     * @param array|null $extras
     *
     * @return Attachment
     */
    public function handle($source, array $extras = null)
    {
        $attachment = new Attachment();
        $attachment->setURL($source);

        $this->scraper->setURL($source);

        $attachment->setType(Attachment::TYPE_LINK);
        $attachment->setTitle($this->scraper->extractTitle());
        $attachment->setValue($this->scraper->extractDescription());

        $uploadDir = $this->uploader->getUploadDirPath();

        if (!is_dir($uploadDir)) {
            mkdir($uploadDir, 0777, true);
        }

        $imageURL = $this->scraper->extractImage();
        $baseName = $this->safeDownloadImage($imageURL, $uploadDir);

        if ($baseName !== null) {
            $attachment->setAttachment($baseName);
        }

        $host = parse_url($source, PHP_URL_HOST);

        if (strpos($host, 'youtube.com') !== false || strpos($host, 'youtu.be') !== false) {
            //youtube
            $attachment->setType(Attachment::TYPE_VIDEO);

        } elseif (strpos($host, 'vimeo.com') !== false) {
            //vimeo
            $attachment->setType(Attachment::TYPE_VIDEO);
        }

        return $attachment;
    }

    /**
     * @param string $url
     * @param string $uploadDir
     *
     * @return null|string
     */
    protected function safeDownloadImage($url, $uploadDir)
    {
        $headers     = get_headers($url, true);
        $parts       = explode(';', $headers['Content-Type']);
        $contentType = reset($parts);

        if (strpos($contentType, 'image') !== 0) {
            return null;
        }

        switch ($contentType) {
            case 'image/jpeg':
                $ext = 'jpg';
                break;
            case 'image/png':
                $ext = 'png';
                break;
            case 'image/gif':
                $ext = 'gif';
                break;
            case 'image/svg+xml':
                $ext = 'svg';
                break;
            default:
                $ext = 'jpg';
        }

        $baseName = md5($url).'.'.$ext;
        file_put_contents($uploadDir.DIRECTORY_SEPARATOR.$baseName, fopen($url, 'r'));
        return $baseName;
    }
}