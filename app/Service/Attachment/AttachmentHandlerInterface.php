<?php namespace StudentskePrakse\Service\Attachment;

use StudentskePrakse\Model\Attachment;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface AttachmentHandlerInterface
{
    /**
     * @param string|UploadedFile $source
     * @param array|null          $extras
     *
     * @return Attachment
     */
    public function handle($source, array $extras = null);
}