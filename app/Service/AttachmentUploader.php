<?php namespace StudentskePrakse\Service;

class AttachmentUploader extends FileUploader
{
    /**
     * @return array
     */
    public function getAllowedMimes()
    {
        return array_merge(parent::getAllowedMimes(), ['application/pdf']);
    }
}