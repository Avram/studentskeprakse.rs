<?php namespace StudentskePrakse\Service;

use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use StudentskePrakse\Model\User;

class AppMailer
{
    /** @var Mailer $mailer */
    protected $mailer;

    /** @var string $from */
    protected $from = 'noreply@studentskeprakse.rs';

    /** @var string $to */
    protected $to;

    /** @var string $subject */
    protected $subject = 'Studentske Prakse';

    /** @var string $view */
    protected $view;

    /** @var array $data */
    protected $data = [];

    /**
     * AppMailer constructor.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
        $this->from   = env('MAIL_FROM', 'noreply@studentskeprakse.rs');
    }

    /**
     * @param User $user
     */
    public function sendActivationMail(User $user)
    {
        $this->to   = $user->getEmail();
        $this->view = 'auth.emails.confirm';
        $this->data = [
            'name'  => $user->getName(),
            'token' => $user->getVerificationToken(),
        ];

        $this->deliver();
    }

    /**
     * Send e-mail
     */
    protected function deliver()
    {
        $this->mailer->send($this->view, $this->data, function (Message $msg) {
            $msg->from($this->from, 'Studentske Prakse')
                ->to($this->to)
                ->subject($this->subject);
        });
    }
}