<?php

namespace StudentskePrakse\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use StudentskePrakse\Service\URLScraper\FacebookURLScraper;
use StudentskePrakse\Service\URLScraper\GooglePlusURLScraper;
use StudentskePrakse\Service\URLScraper\HTMLURLScraper;
use StudentskePrakse\Service\URLScraper\TwitterURLScraper;
use StudentskePrakse\Service\URLScraper\URLScraperManager;

class URLScraperProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(URLScraperManager::class, function (Application $app) {
            $scrapers = [
                new FacebookURLScraper(),
                new TwitterURLScraper(),
                new GooglePlusURLScraper(),
                new HTMLURLScraper()
            ];

            return new URLScraperManager($scrapers);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
