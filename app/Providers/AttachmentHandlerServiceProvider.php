<?php

namespace StudentskePrakse\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use StudentskePrakse\Service\Attachment\AttachmentHandlerManager;
use StudentskePrakse\Service\Attachment\FileAttachmentHandler;
use StudentskePrakse\Service\Attachment\RemoteFileAttachmentHandler;
use StudentskePrakse\Service\Attachment\URLAttachmentHandler;
use StudentskePrakse\Service\AttachmentUploader;
use StudentskePrakse\Service\ImageResizer;
use StudentskePrakse\Service\URLScraper\URLScraperManager;

class AttachmentHandlerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(AttachmentHandlerManager::class, function (Application $app) {
            $fileAttachment       = new FileAttachmentHandler(new AttachmentUploader(), new ImageResizer());
            $urlAttachment        = new URLAttachmentHandler(new AttachmentUploader(), $this->app->make(URLScraperManager::class));
            $remoteFileAttachment = new RemoteFileAttachmentHandler(new AttachmentUploader(), new ImageResizer());

            return new AttachmentHandlerManager($fileAttachment, $urlAttachment, $remoteFileAttachment);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
