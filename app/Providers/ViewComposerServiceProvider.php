<?php

namespace StudentskePrakse\Providers;

use Illuminate\Support\ServiceProvider;
use StudentskePrakse\Http\ViewComposers\SharedVariablesComposer;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', SharedVariablesComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
