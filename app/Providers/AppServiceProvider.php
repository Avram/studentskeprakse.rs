<?php

namespace StudentskePrakse\Providers;

use Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::extend(function ($val) {
            $value = preg_replace('/@switch\((.*)\)/', '<?php switch($1): ?>', $val);
            $value = preg_replace('/(\s*)@case\((.*)\)/', '<?php case $2: ?>', $value);
            $value = preg_replace('/@break/', '<?php break; ?>', $value);
            $value = preg_replace('/@default/', '<?php default: ?>', $value);
            $value = preg_replace('/@endswitch/', '<?php endswitch; ?>', $value);

//            dd($value);
            return $value;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
