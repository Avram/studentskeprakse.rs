<?php

namespace StudentskePrakse\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use StudentskePrakse\Service\Attachment\AttachmentManager;
use StudentskePrakse\Service\Attachment\FileAttachment;
use StudentskePrakse\Service\Attachment\URLAttachment;
use StudentskePrakse\Service\AttachmentUploader;
use StudentskePrakse\Service\ImageResizer;
use StudentskePrakse\Service\URLScraper\URLScraperManager;

class AttachmentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(AttachmentManager::class, function (Application $app) {
            $fileAttachment = new FileAttachment(new AttachmentUploader(), new ImageResizer());
            $urlAttachment  = new URLAttachment(new AttachmentUploader(), $this->app->make(URLScraperManager::class));

            return new AttachmentManager($fileAttachment, $urlAttachment);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
