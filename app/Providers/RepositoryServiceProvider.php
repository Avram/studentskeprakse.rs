<?php

namespace StudentskePrakse\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use StudentskePrakse\Model\Attachment;
use StudentskePrakse\Model\Category;
use StudentskePrakse\Model\City;
use StudentskePrakse\Model\Comment;
use StudentskePrakse\Model\FAQ;
use StudentskePrakse\Model\Feedback;
use StudentskePrakse\Model\Internship;
use StudentskePrakse\Model\Mentor;
use StudentskePrakse\Model\Post;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\AttachmentRepositoryInterface;
use StudentskePrakse\Repository\CategoryRepositoryInterface;
use StudentskePrakse\Repository\CityRepositoryInterface;
use StudentskePrakse\Repository\CommentRepositoryInterface;
use StudentskePrakse\Repository\EloquentAttachmentRepository;
use StudentskePrakse\Repository\EloquentCategoryRepository;
use StudentskePrakse\Repository\EloquentCityRepository;
use StudentskePrakse\Repository\EloquentCommentRepository;
use StudentskePrakse\Repository\EloquentFAQRepository;
use StudentskePrakse\Repository\EloquentFeedbackRepository;
use StudentskePrakse\Repository\EloquentInternshipRepository;
use StudentskePrakse\Repository\EloquentMentorRepository;
use StudentskePrakse\Repository\EloquentPostRepository;
use StudentskePrakse\Repository\EloquentUserRepository;
use StudentskePrakse\Repository\FAQRepositoryInterface;
use StudentskePrakse\Repository\FeedbackRepositoryInterface;
use StudentskePrakse\Repository\InternshipRepositoryInterface;
use StudentskePrakse\Repository\MentorRepositoryInterface;
use StudentskePrakse\Repository\PostRepositoryInterface;
use StudentskePrakse\Repository\UserRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(AttachmentRepositoryInterface::class, function (Application $app) {
            return new EloquentAttachmentRepository(new Attachment);
        });

        $this->app->bind(CategoryRepositoryInterface::class, function (Application $app) {
            return new EloquentCategoryRepository(new Category);
        });

        $this->app->bind(CityRepositoryInterface::class, function (Application $app) {
            return new EloquentCityRepository(new City);
        });

        $this->app->bind(CommentRepositoryInterface::class, function (Application $app) {
            return new EloquentCommentRepository(new Comment);
        });

        $this->app->bind(FAQRepositoryInterface::class, function (Application $app) {
            return new EloquentFAQRepository(new FAQ);
        });

        $this->app->bind(FeedbackRepositoryInterface::class, function (Application $app) {
            return new EloquentFeedbackRepository(new Feedback);
        });

        $this->app->bind(InternshipRepositoryInterface::class, function (Application $app) {
            return new EloquentInternshipRepository(new Internship);
        });

        $this->app->bind(MentorRepositoryInterface::class, function (Application $app) {
            return new EloquentMentorRepository(new Mentor);
        });

        $this->app->bind(PostRepositoryInterface::class, function (Application $app) {
            return new EloquentPostRepository(new Post);
        });

        $this->app->bind(UserRepositoryInterface::class, function (Application $app) {
            return new EloquentUserRepository(new User);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
