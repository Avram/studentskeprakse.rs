<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Exceptions\EntityNotFoundException;
use StudentskePrakse\Model\Comment;
use StudentskePrakse\Model\Post;

class EloquentCommentRepository implements CommentRepositoryInterface
{

    /**
     * @var Comment
     */
    private $comment;

    /**
     * EloquentCommentRepository constructor.
     *
     * @param Comment $comment
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @param Comment $comment
     */
    public function save(Comment $comment)
    {
        $comment->save();
    }

    /**
     * @return Collection|Comment[]
     */
    public function getAll()
    {
        return $this->comment->all();
    }

    /**
     * @param int $id
     *
     * @return Comment|null
     */
    public function find($id)
    {
        return $this->comment->query()->find($id);
    }

    /**
     * @param Comment $comment
     *
     * @throws \Exception
     */
    public function delete(Comment $comment)
    {
        $comment->delete();
    }

    /**
     * @param Comment $comment
     * @param Comment $parent
     */
    public function saveCommentToParent(Comment $comment, Comment $parent)
    {
        $parent->comments()->save($comment);
    }

    /**
     * @param Comment $comment
     * @param Post    $post
     */
    public function saveCommentToPost(Comment $comment, Post $post)
    {
        $post->comments()->save($comment);
    }

    /**
     * @param Comment $comment
     * @param int     $limit
     * @param int     $offset
     *
     * @return Collection|Comment[]
     */
    public function getCommentsForComment(Comment $comment, $limit = 100, $offset = 0)
    {
        return $comment->comments()
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param Post $post
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Post[]
     */
    public function getCommentsForPost(Post $post, $limit = 100, $offset = 0)
    {
        return $post->comments()
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param int $id
     *
     * @return Comment
     * @throws \StudentskePrakse\Exceptions\EntityNotFoundException
     */
    public function findOrFail($id)
    {
        $comment = $this->find($id);
        if ($comment === null) {
            throw new EntityNotFoundException("Comment not found.");
        }
        return $comment;
    }
}