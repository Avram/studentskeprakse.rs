<?php

namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Model\Category;
use StudentskePrakse\Model\City;
use StudentskePrakse\Model\User;

interface UserRepositoryInterface
{
    /**
     * @param User $user
     */
    public function save(User $user);

    /**
     * @return Collection|User[]
     */
    public function getAll();

    /**
     * @param int $id
     *
     * @return User|null
     */
    public function find($id);

    /**
     * @param $name
     *
     * @return User|null
     */
    public function findByName($name);

    /**
     * @param $email
     *
     * @return User|null
     */
    public function findByEmail($email);

    /**
     * @param int $id
     *
     * @return User
     */
    public function findOrFail($id);

    /**
     * @param User $user
     */
    public function delete(User $user);

    /**
     * @param string $slug
     *
     * @return User|null
     */
    public function findBySlug($slug);

    /**
     * @param string $slug
     *
     * @return User
     */
    public function findBySlugOrFail($slug);

    /**
     * @param $token
     *
     * @return User|null
     */
    public function findByToken($token);

    /**
     * @param $token
     *
     * @return User
     */
    public function findByTokenOrFail($token);

    /**
     * @param User     $user
     * @param Category $category
     *
     * @return mixed
     */
    public function saveUserToCategory(User $user, Category $category);

    /**
     * @param User $user
     * @param City $city
     *
     * @return mixed
     */
    public function saveUserToCity(User $user, City $city);

    /**
     * @param Category $category
     * @param int      $limit
     * @param int      $offset
     *
     * @return Collection|User[]
     */
    public function getUsersForCategory(Category $category, $limit = 100, $offset = 0);

    /**
     * @param City $city
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|User[]
     */
    public function getUsersForCity(City $city, $limit = 100, $offset = 0);
}