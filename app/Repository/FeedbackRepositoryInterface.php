<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Model\Feedback;
use StudentskePrakse\Model\User;

interface FeedbackRepositoryInterface
{
    /**
     * @param Feedback $feedback
     */
    public function save(Feedback $feedback);

    /**
     * @return Collection|Feedback[]
     */
    public function getAll();

    /**
     * @param int $id
     *
     * @return Feedback|null
     */
    public function find($id);

    /**
     * @param int $id
     *
     * @return Feedback
     */
    public function findOrFail($id);


    /**
     * @param Feedback $feedback
     */
    public function delete(Feedback $feedback);

    /**
     * @param Feedback $feedback
     * @param User     $user
     */
    public function saveFeedbackToUser(Feedback $feedback, User $user);

    /**
     * @param User $user
     * @param bool $includeUnanswered
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Feedback[]
     */
    public function getFeedbacksForUser(User $user, $includeUnanswered = false, $limit = 100, $offset = 0);

}