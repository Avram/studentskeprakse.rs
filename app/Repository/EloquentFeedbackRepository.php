<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Exceptions\EntityNotFoundException;
use StudentskePrakse\Model\Feedback;
use StudentskePrakse\Model\User;

class EloquentFeedbackRepository implements FeedbackRepositoryInterface
{
    /**
     * @var Feedback
     */
    private $feedback;

    /**
     * EloquentFeedbackRepository constructor.
     *
     * @param Feedback $feedback
     */
    public function __construct(Feedback $feedback)
    {
        $this->feedback = $feedback;
    }

    /**
     * @param Feedback $feedback
     */
    public function save(Feedback $feedback)
    {
        $feedback->save();
    }

    /**
     * @return Collection|Feedback[]
     */
    public function getAll()
    {
        return $this->feedback->all();
    }

    /**
     * @param int $id
     *
     * @return Feedback|null
     */
    public function find($id)
    {
        return $this->feedback->query()->find($id);
    }

    /**
     * @param Feedback $feedback
     *
     * @throws \Exception
     */
    public function delete(Feedback $feedback)
    {
        $feedback->delete();
    }

    /**
     * @param User $user
     * @param bool $includeUnanswered
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Feedback[]
     */
    public function getFeedbacksForUser(User $user, $includeUnanswered = false, $limit = 100, $offset = 0)
    {
        $feedbacks = $user->feedbacks();

        if (!$includeUnanswered) {
            $feedbacks->whereNotNull('reply');
        }

        return $feedbacks->orderBy('updated_at', 'DESC')
            ->skip($offset)
            ->take($limit)
            ->get();

    }

    /**
     * @param Feedback $feedback
     * @param User     $user
     */
    public function saveFeedbackToUser(Feedback $feedback, User $user)
    {
        $user->feedbacks()->save($feedback);
    }

    /**
     * @param int $id
     *
     * @return Feedback
     * @throws EntityNotFoundException
     */
    public function findOrFail($id)
    {
        $feedback = $this->find($id);
        if ($feedback === null) {
            throw new EntityNotFoundException("Feedback not found");
        }
        return $feedback;
    }
}