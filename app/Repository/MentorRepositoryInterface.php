<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04-Feb-16
 * Time: 4:29 PM
 */

namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Model\Internship;
use StudentskePrakse\Model\Mentor;
use StudentskePrakse\Model\User;

interface MentorRepositoryInterface
{
    /**
     * @param Mentor $mentor
     */
    public function save(Mentor $mentor);

    /**
     * @return Collection|Mentor[]
     */
    public function getAll();

    /**
     * @param int $id
     *
     * @return Mentor|null
     */
    public function find($id);

    /**
     * @param int $id
     *
     * @return Mentor
     */
    public function findOrFail($id);

    /**
     * @param Mentor $mentor
     */
    public function delete(Mentor $mentor);

    /**
     * @param User $user
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Mentor[]
     */
    public function getMentorsForUser(User $user, $limit = 100, $offset = 0);

    /**
     * @param Internship $internship
     *
     * @param int        $limit
     * @param int        $offset
     *
     * @return Collection|Mentor[]
     */
    public function getMentorsForInternship(Internship $internship, $limit = 100, $offset = 0);


    /**
     * @param Mentor $mentor
     * @param User   $user
     */
    public function saveMentorToUser(Mentor $mentor, User $user);

    /**
     * @param Mentor     $mentor
     * @param Internship $internship
     */
    public function saveMentorToInternship(Mentor $mentor, Internship $internship);

}