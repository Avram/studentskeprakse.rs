<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Model\Category;
use StudentskePrakse\Model\City;
use StudentskePrakse\Model\Internship;
use StudentskePrakse\Model\User;

interface InternshipRepositoryInterface
{
    /**
     * @param Internship $internship
     * @param array      $options
     *
     * @return
     */
    public function save(Internship $internship, array $options = []);

    /**
     * @return Collection|Internship[]
     */
    public function getAll();

    /**
     * @param int $id
     *
     * @return Internship|null
     */
    public function find($id);

    /**
     * @param int $id
     *
     * @return Internship
     */
    public function findOrFail($id);

    /**
     * @param string $slug
     * @param User   $user
     *
     * @return null|Internship
     */
    public function findBySlug($slug, User $user, $withTrashed = false);

    /**
     * @param string $slug
     * @param User   $user
     *
     * @return Internship
     */
    public function findBySlugOrFail($slug, User $user, $withTrashed = false);

    /**
     * @param Internship $internship
     */
    public function delete(Internship $internship);

    /**
     * @param Internship $internship
     */
    public function softDelete(Internship $internship);

    /**
     * @param Internship $internship
     */
    public function softRestore(Internship $internship);

    /**
     * @param Internship $internship
     * @param User       $user
     */
    public function saveInternshipToUser(Internship $internship, User $user);

    /**
     * @param Internship $internship
     * @param Category   $category
     */
    public function saveInternshipToCategory(Internship $internship, Category $category);

    /**
     * @param Internship $internship
     * @param City       $city
     */
    public function saveInternshipToCity(Internship $internship, City $city);

    /**
     * @param User $user
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Internship[]
     */
    public function getInternshipsForUser(User $user, $limit = 100, $offset = 0);

    /**
     * @param Category $category
     * @param int      $limit
     * @param int      $offset
     *
     * @return Collection|Internship[]
     */
    public function getInternshipsForCategory(Category $category, $limit = 100, $offset = 0);

    /**
     * @param City $city
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Internship[]
     */
    public function getInternshipsForCity(City $city, $limit = 100, $offset = 0);

    /**
     * @param array  $args
     *
     * @param int    $limit
     * @param int    $offset
     * @param string $orderBy
     * @param string $order
     *
     * @return Collection|Internship[]
     */
    public function searchInternships(array $args, $limit = 100, $offset = 0, $orderBy = 'created_at', $order = 'DESC');
}