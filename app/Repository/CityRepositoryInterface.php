<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Model\City;

interface CityRepositoryInterface
{


    /**
     * @return Collection|City[]
     */
    public function getAll();

    /**
     * @param int $id
     *
     * @return City|null
     */
    public function find($id);

    /**
     * @param int $id
     *
     * @return City
     */
    public function findOrFail($id);

    /**
     * @param string $name
     *
     * @return Collection|City[]
     */
    public function getByName($name);

    /**
     * @return Collection|City[]
     */
    public function getCitiesWithInternships();


}