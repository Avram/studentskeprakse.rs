<?php

namespace StudentskePrakse\Repository;


use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Model\Category;

interface CategoryRepositoryInterface
{
    /**
     * @param Category $category
     */
    public function save(Category $category);

    /**
     * @return Collection|Category[]
     */
    public function getAll();

    /**
     * @param int $id
     *
     * @return Category|null
     */
    public function find($id);

    /**
     * @param int $id
     *
     * @return Category
     */
    public function findOrFail($id);


    /**
     * @param Category $category
     */
    public function delete(Category $category);

    /**
     * @param Category $category
     * @param Category $parent
     */
    public function saveCategoryToParent(Category $category, Category $parent);

    /**
     * @param Category $category
     * @param int      $limit
     * @param int      $offset
     *
     * @return Collection|Category[]
     */
    public function getCategoryChildren(Category $category, $limit = 100, $offset = 0);

    /**
     * @return Collection|Category[]
     */
    public function getCategoriesWithInternships();
}