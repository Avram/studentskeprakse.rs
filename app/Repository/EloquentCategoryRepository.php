<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Exceptions\EntityNotFoundException;
use StudentskePrakse\Model\Category;
use StudentskePrakse\Model\Internship;

class EloquentCategoryRepository implements CategoryRepositoryInterface
{
    /**
     * @var Category
     */
    private $category;

    /**
     * EloquentCategoryRepository constructor.
     *
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @param Category $category
     */
    public function save(Category $category)
    {
        $category->save();
    }

    /**
     * @param Category $category
     *
     * @throws \Exception
     */
    public function delete(Category $category)
    {
        $category->delete();
    }

    /**
     * @return Collection|Category[]
     */
    public function getAll()
    {
        return $this->category->all();
    }

    /**
     * @param int $id
     *
     * @return Category|null
     */
    public function find($id)
    {
        return $this->category->query()->find($id);

    }

    /**
     * @param Category $category
     * @param Category $parent
     */
    public function saveCategoryToParent(Category $category, Category $parent)
    {
        $category->parent()->save($parent);
    }

    /**
     * @param Category $category
     * @param int      $limit
     * @param int      $offset
     *
     * @return Collection|Category[]
     */
    public function getCategoryChildren(Category $category, $limit = 100, $offset = 0)
    {
        return $category->children()
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param int $id
     *
     * @return Category
     * @throws EntityNotFoundException
     */
    public function findOrFail($id)
    {
        $category = $this->find($id);
        if ($category === null) {
            throw new EntityNotFoundException("Category not found.");
        }
        return $category;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategoriesWithInternships()
    {
        return $this->category->query()->whereIn('id', function ($query) {
            $query->distinct()->select('category_id')
                ->from(with(new Internship)->getTable())
                ->where('status', Internship::STATUS_PUBLISHED)
                ->whereNull('deleted_at');
        })->orderBy('name')
            ->get();
    }
}