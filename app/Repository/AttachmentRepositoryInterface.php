<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Model\Attachment;
use StudentskePrakse\Model\User;

interface AttachmentRepositoryInterface
{
    /**
     * @param Attachment $attachment
     */
    public function save(Attachment $attachment);

    /**
     * @return Collection|Attachment[]
     */
    public function getAll();

    /**
     * @param int $id
     *
     * @return Attachment|null
     */
    public function find($id);

    /**
     * @param int $id
     *
     * @return Attachment
     */
    public function findOrFail($id);

    /**
     * @param Attachment $attachment
     */
    public function delete(Attachment $attachment);

    /**
     * @param Attachment $attachment
     * @param User       $user
     */
    public function saveAttachmentForUser(Attachment $attachment, User $user);

    /**
     * @param User $user
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Attachment[]
     *
     */
    public function getAttachmentsForUser(User $user, $limit = 100, $offset = 0);

    /**
     * @param User $user
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Attachment[]
     */
    public function getOfficeImagesForUser(User $user, $limit = 100, $offset = 0);

}