<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Exceptions\EntityNotFoundException;
use StudentskePrakse\Model\Category;
use StudentskePrakse\Model\City;
use StudentskePrakse\Model\User;

class EloquentUserRepository implements UserRepositoryInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * EloquentUserRepository constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param User $user
     */
    public function save(User $user)
    {
        $user->save();
    }

    /**
     * @return Collection|User[]
     */
    public function getAll()
    {
        return $this->user->all();
    }

    /**
     * @param int $id
     *
     * @return User|null
     */
    public function find($id)
    {
        return $this->user->query()->find($id);
    }

    /**
     * @param $name
     *
     * @return User|null
     */
    public function findByName($name)
    {
        return $this->user->query()->where('name', $name)->first();
    }

    /**
     * @param $email
     *
     * @return User|null
     */
    public function findByEmail($email)
    {
        return $this->user->query()->where('email', $email)->first();
    }

    /**
     * @param User $user
     *
     * @throws \Exception
     */
    public function delete(User $user)
    {
        $user->delete();
    }

    /**
     * @param string $slug
     *
     * @return User|null
     */
    public function findBySlug($slug)
    {
        return $this->user->query()->where('slug', '=', $slug)->first();
    }

    /**
     * @param $token
     *
     * @return User|null
     */
    public function findByToken($token)
    {
        return $this->user->query()->where('token', '=', $token)->first();
    }

    /**
     * @param User     $user
     * @param Category $category
     *
     * @return mixed|void
     */
    public function saveUserToCategory(User $user, Category $category)
    {
        $category->users()->save($user);
    }

    /**
     * @param User $user
     * @param City $city
     *
     * @return mixed|void
     */
    public function saveUserToCity(User $user, City $city)
    {
        $city->users()->save($user);
    }

    /**
     * @param Category $category
     * @param int      $limit
     * @param int      $offset
     *
     * @return Collection|\StudentskePrakse\Model\User[]
     */
    public function getUsersForCategory(Category $category, $limit = 100, $offset = 0)
    {
        return $category->users()
            ->take($limit)
            ->skip($offset)
            ->get();
    }

    /**
     * @param City $city
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|User[]
     */
    public function getUsersForCity(City $city, $limit = 100, $offset = 0)
    {
        return $city->users()
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param int $id
     *
     * @return User
     * @throws EntityNotFoundException
     */
    public function findOrFail($id)
    {
        $user = $this->find($id);
        if ($user === null) {
            throw new EntityNotFoundException("User not found.");
        }
        return $user;
    }

    /**
     * @param string $slug
     *
     * @return User
     * @throws EntityNotFoundException
     */
    public function findBySlugOrFail($slug)
    {
        $user = $this->findBySlug($slug);
        if ($user === null) {
            throw new EntityNotFoundException("User not found.");
        }

        return $user;
    }

    /**
     * @param $token
     *
     * @return User
     * @throws EntityNotFoundException
     */
    public function findByTokenOrFail($token)
    {
        $user = $this->findByToken($token);
        if ($user === null) {
            throw new EntityNotFoundException("User not found.");
        }
        return $user;
    }
}