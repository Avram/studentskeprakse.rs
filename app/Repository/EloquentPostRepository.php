<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Exceptions\EntityNotFoundException;
use StudentskePrakse\Model\Post;
use StudentskePrakse\Model\User;

class EloquentPostRepository implements PostRepositoryInterface
{
    /**
     * @var Post
     */
    private $post;

    /**
     * EloquentPostRepository constructor.
     *
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * @param Post $post
     */
    public function save(Post $post)
    {
        $post->save();
    }

    /**
     * @param Post $post
     *
     * @throws \Exception
     */
    public function delete(Post $post)
    {
        $post->delete();
    }

    /**
     * @return Collection|Post[]
     */
    public function getAll()
    {
        return $this->post->all();
    }

    /**
     * @param int $id
     *
     * @return Post|null
     */
    public function find($id)
    {
        return $this->post->query()->find($id);
    }

    /**
     * @param string $slug
     *
     * @return Post|null
     */
    public function findBySlug($slug)
    {
        return $this->post->query()->where('slug', '=', $slug)->first();
    }

    /**
     * @param Post $post
     * @param User $user
     */
    public function savePostToUser(Post $post, User $user)
    {
        $user->posts()->save($post);
    }

    /**
     * @param User $user
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Post[]
     */
    public function getPostsForUser(User $user, $limit = 100, $offset = 0)
    {
        return $user->posts()
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param int $id
     *
     * @return Post
     * @throws EntityNotFoundException
     */
    public function findOrFail($id)
    {
        $post = $this->find($id);

        if ($post === null) {
            throw new EntityNotFoundException("Post not found.");
        }
        return $post;
    }

    /**
     * @param string $slug
     *
     * @return Post
     * @throws EntityNotFoundException
     */
    public function findBySlugOrFail($slug)
    {
        $post = $this->findBySlug($slug);
        if ($post === null) {
            throw new EntityNotFoundException("Post not found.");
        }
        return $post;
    }
}