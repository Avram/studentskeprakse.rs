<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Exceptions\EntityNotFoundException;
use StudentskePrakse\Model\Internship;
use StudentskePrakse\Model\Mentor;
use StudentskePrakse\Model\User;

class EloquentMentorRepository implements MentorRepositoryInterface
{
    /**
     * @var Mentor
     */
    private $mentor;

    /**
     * EloquentMentorRepository constructor.
     *
     * @param Mentor $mentor
     */
    public function __construct(Mentor $mentor)
    {
        $this->mentor = $mentor;
    }


    /**
     * @param Mentor $mentor
     */
    public function save(Mentor $mentor)
    {
        $mentor->save();
    }

    /**
     * @return Collection|Mentor[]
     */
    public function getAll()
    {
        return $this->mentor->all();
    }

    /**
     * @param int $id
     *
     * @return Mentor|null
     */
    public function find($id)
    {
        return $this->mentor->query()->find($id);
    }

    /**
     * @param Mentor $mentor
     */
    public function delete(Mentor $mentor)
    {
        $mentor->delete();
    }

    /**
     * @param Mentor $mentor
     * @param User   $user
     */
    public function saveMentorToUser(Mentor $mentor, User $user)
    {
        $user->mentors()->save($mentor);
    }

    /**
     * @param Mentor     $mentor
     * @param Internship $internship
     */
    public function saveMentorToInternship(Mentor $mentor, Internship $internship)
    {
        $internship->mentors()->save($mentor);
    }

    /**
     * @param User $user
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Mentor[]
     */
    public function getMentorsForUser(User $user, $limit = 100, $offset = 0)
    {
        return $user->mentors()
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param Internship $internship
     * @param int        $limit
     * @param int        $offset
     *
     * @return Collection|Mentor[]
     */
    public function getMentorsForInternship(Internship $internship, $limit = 100, $offset = 0)
    {
        return $internship->mentors()
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param int $id
     *
     * @return Mentor
     * @throws EntityNotFoundException
     */
    public function findOrFail($id)
    {
        $mentor = $this->find($id);
        if ($mentor === null) {
            throw new EntityNotFoundException("Mentor not found");
        }
        return $mentor;
    }
}