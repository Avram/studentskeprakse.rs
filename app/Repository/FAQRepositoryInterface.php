<?php
namespace StudentskePrakse\Repository;


use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Model\FAQ;
use StudentskePrakse\Model\Internship;
use StudentskePrakse\Model\User;

interface FAQRepositoryInterface
{
    /**
     * @param FAQ $faq
     */
    public function save(FAQ $faq);

    /**
     * @return Collection|FAQ[]
     */
    public function getAll();

    /**
     * @param int $id
     *
     * @return FAQ|null
     */
    public function find($id);

    /**
     * @param int $id
     *
     * @return FAQ
     */
    public function findOrFail($id);

    /**
     * @param FAQ $faq
     */
    public function delete(FAQ $faq);

    /**
     * @param FAQ  $faq
     * @param User $user
     */
    public function saveFAQtoUser(FAQ $faq, User $user);

    /**
     * @param FAQ        $faq
     * @param Internship $internship
     */
    public function saveFAQtoInternship(FAQ $faq, Internship $internship);

    /**
     * @param User $user
     * @param bool $includeUnanswered
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|FAQ[]
     */
    public function getFAQsForUser(User $user, $includeUnanswered = false, $limit = 100, $offset = 0);

    /**
     * @param Internship $internship
     * @param bool       $includeUnanswered
     * @param int        $limit
     * @param int        $offset
     *
     * @return Collection|FAQ[]
     */
    public function getFAQsForInternship(Internship $internship, $includeUnanswered = false, $limit = 100, $offset = 0);

    /**
     * @param FAQ $faq
     *
     * @return Internship|User
     */
    public function getOwner(FAQ $faq);
}