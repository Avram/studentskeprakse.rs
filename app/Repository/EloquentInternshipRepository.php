<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Exceptions\EntityNotFoundException;
use StudentskePrakse\Model\Category;
use StudentskePrakse\Model\City;
use StudentskePrakse\Model\Internship;
use StudentskePrakse\Model\User;

class EloquentInternshipRepository implements InternshipRepositoryInterface
{

    /**
     * @var Internship
     */
    private $internship;

    /**
     * EloquentInternshipRepository constructor.
     *
     * @param Internship $internship
     */
    public function __construct(Internship $internship)
    {
        $this->internship = $internship;
    }

    /**
     * @param Internship $internship
     * @param array      $options
     */
    public function save(Internship $internship, array $options = [])
    {
        $internship->save();
    }

    /**
     * @return Collection|Internship[]
     */
    public function getAll()
    {
        return $this->internship->all();
    }

    /**
     * @param int $id
     *
     * @return Internship|null
     */
    public function find($id)
    {
        return $this->internship->query()->find($id);
    }

    /**
     * @param Internship $internship
     *
     * @throws \Exception
     */
    public function delete(Internship $internship)
    {
        $internship->forceDelete();
    }

    /**
     * @param Internship $internship
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function softDelete(Internship $internship)
    {
        $internship->delete();
    }

    /**
     * @param Internship $internship
     */
    public function softRestore(Internship $internship)
    {
        $internship->restore();
    }

    /**
     * @param string $slug
     * @param User   $user
     * @param bool   $withTrashed
     *
     * @return null|Internship
     */
    public function findBySlug($slug, User $user, $withTrashed = false)
    {
        if ($slug === 'new-internship') {
            return Internship::dummy();
        }

        $query = $this->internship->query();
        $query->where('slug', $slug)
            ->where('user_id', $user->getId());
        if ($withTrashed) {
            $query->withTrashed();
        }

        return $query->first();
    }

    /**
     * @param Internship $internship
     * @param User       $user
     */
    public function saveInternshipToUser(Internship $internship, User $user)
    {
        $user->internships()->save($internship);
    }

    /**
     * @param Internship $internship
     * @param Category   $category
     */
    public function saveInternshipToCategory(Internship $internship, Category $category)
    {
        $category->internships()->save($internship);
    }

    /**
     * @param Internship $internship
     * @param City       $city
     */
    public function saveInternshipToCity(Internship $internship, City $city)
    {
        $city->internships()->save($internship);
    }

    /**
     * @param User $user
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Internship[]
     */
    public function getInternshipsForUser(User $user, $limit = 100, $offset = 0)
    {
        return $user->internships()
            ->orderBy('created_at', 'DESC')
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param Category $category
     * @param int      $limit
     * @param int      $offset
     *
     * @return Collection|Internship[]
     */
    public function getInternshipsForCategory(Category $category, $limit = 100, $offset = 0)
    {
        return $category->internships()
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param City $city
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Internship[]
     */
    public function getInternshipsForCity(City $city, $limit = 100, $offset = 0)
    {
        return $city->internships()
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param array  $args
     * @param int    $limit
     * @param int    $offset
     * @param string $orderBy
     * @param string $order
     *
     * @return Collection|Internship[]
     */
    public function searchInternships(array $args, $limit = 100, $offset = 0, $orderBy = 'created_at', $order = 'DESC')
    {
        /** @var Internship $query */
        $query = $this->internship->query();

        $conditions = isset($args['conditions']) ? $args['conditions'] : [];

        foreach ($args as $arg => $value) {
            switch ($arg) {
                case 'paid':
                    if ($value == true) {
                        $query->whereNotNull('salary');
                    }
                    break;
                case 'food':
                    if (empty($value)) {
                        break;
                    }

                    $value = is_array($value) ? $value : explode(',', $value);

                    foreach ($value as $bld) {
                        $query->where('food', 'LIKE', '%'.$bld.'%');
                    }

                    break;
                case 'status':
                    if (!empty($value)) {
                        $query->where('status', $value);
                    }
                    break;
                case 'user':
                    if (!empty($value)) {
                        $query->where('user_id', $value);
                    }
                    break;
                case 'city':
                    if (empty($value)) {
                        break;
                    }

                    if (!is_array($value)) {
                        $value = [$value];
                    }

                    if (isset($conditions['city']) && ($conditions['city'] == 'OR')) {
                        $query->orWhereIn('city_id', $value);
                    } else {
                        $query->whereIn('city_id', $value);
                    }
                    break;
                case 'category':
                    if (empty($value)) {
                        break;
                    }

                    if (!is_array($value)) {
                        $value = [$value];
                    }

//                    foreach ($value as $cat) {
//                        $cat          = (int)$cat;
//                        $category_ids = DB::select("SELECT id FROM categories WHERE id={$cat} OR parent_id={$cat}");
//                        $category_ids = array_map(function ($el) {
//                            return $el->id;
//                        }, $category_ids);
//                    }

                    if (isset($conditions['category']) && ($conditions['category'] == 'OR')) {
                        $query->orWhereIn('category_id', $value);
                    } else {
                        $query->whereIn('category_id', $value);
                    }

                    break;
                case 'withTrashed':
                    if ($value == true) {
                        $query->withTrashed();
                    }
                    break;
                case 'onlyTrashed':
                    if ($value == true) {
                        $query->onlyTrashed();
                    }
                    break;
                case 'term':
                    if (!empty($value)) {
                        $query->where('name', 'LIKE', '%'.$value.'%');
                        $query->orWhere('content', 'LIKE', '%'.$value.'%');
                        $query->orWhereHas('User', function ($query) use ($value) {
                            $query->where('name', 'LIKE', '%'.$value.'%');
                        });
                    }
                    break;
                case 'exclude':
                    if (empty($value)) {
                        break;
                    }

                    $query->whereNotIn('id', $value);
                    break;
                case 'similar':
                    if (empty($value)) {
                        break;
                    }

                    $query->where(function ($query) use ($value) {
                        $query->where(function ($query) use ($value) {
                            $query->where('category_id', $value->getCategoryId())
                                ->where('city_id', $value->getCityId());
                        });
                        $query->orWhere('category_id', $value->getCategoryId());
                    });
                    break;
            }
        }

        $query->skip($offset);
        $query->take($limit);
        $query->orderBy($orderBy, $order);

        return $query->get();
    }

    /**
     * @param int $id
     *
     * @return Internship
     * @throws EntityNotFoundException
     */
    public function findOrFail($id)
    {
        $internship = $this->find($id);
        if ($internship === null) {
            throw new EntityNotFoundException("Internship not found");
        }
        return $internship;
    }

    /**
     * @param string $slug
     * @param User   $user
     *
     * @return Internship
     * @throws EntityNotFoundException
     */
    public function findBySlugOrFail($slug, User $user, $withTrashed = false)
    {
        $internship = $this->findBySlug($slug, $user, $withTrashed);
        if ($internship === null) {
            throw new EntityNotFoundException("Internship not found");
        }
        return $internship;
    }
}