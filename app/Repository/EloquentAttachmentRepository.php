<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Exceptions\EntityNotFoundException;
use StudentskePrakse\Model\Attachment;
use StudentskePrakse\Model\User;

class EloquentAttachmentRepository implements AttachmentRepositoryInterface
{
    /**
     * @var Attachment
     */
    private $attachment;

    /**
     * EloquentAttachmentRepository constructor.
     *
     * @param Attachment $attachment
     */
    public function __construct(Attachment $attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * @param Attachment $attachment
     */
    public function save(Attachment $attachment)
    {
        $attachment->save();
    }

    /**
     * @return Collection|Attachment[]
     */
    public function getAll()
    {
        return $this->attachment->all();
    }

    /**
     * @param int $id
     *
     * @return Attachment|null
     */
    public function find($id)
    {
        return $this->attachment->query()->find($id);
    }

    /**
     * @param Attachment $attachment
     */
    public function delete(Attachment $attachment)
    {
        $attachment->delete();
    }

    /**
     * @param Attachment $attachment
     * @param User       $user
     */
    public function saveAttachmentForUser(Attachment $attachment, User $user)
    {
        $user->attachments()->save($attachment);
    }

    /**
     * @param User $user
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Attachment[]
     */
    public function getAttachmentsForUser(User $user, $limit = 100, $offset = 0)
    {
        return $user->attachments()
            ->where('type', '!=', Attachment::TYPE_OFFICE)
            ->orderBy('created_at', 'DESC')
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param User $user
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Attachment[]
     */
    public function getOfficeImagesForUser(User $user, $limit = 100, $offset = 0)
    {
        return $user->attachments()
            ->where('type', Attachment::TYPE_OFFICE)
            ->orderBy('order', 'ASC')
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param int $id
     *
     * @return Attachment
     * @throws EntityNotFoundException
     */
    public function findOrFail($id)
    {
        $attachment = $this->find($id);
        if ($attachment === null) {
            throw new EntityNotFoundException("Attachment not found");
        }
        return $attachment;
    }
}