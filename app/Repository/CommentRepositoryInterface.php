<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Model\Comment;
use StudentskePrakse\Model\Post;

interface CommentRepositoryInterface
{

    /**
     * @param Comment $comment
     */
    public function save(Comment $comment);

    /**
     * @return Collection|Comment[]
     */
    public function getAll();

    /**
     * @param int $id
     *
     * @return Comment|null
     */
    public function find($id);

    /**
     * @param int $id
     *
     * @return Comment
     */
    public function findOrFail($id);

    /**
     * @param Comment $comment
     */
    public function delete(Comment $comment);

    /**
     * @param Comment $comment
     * @param Comment $parent
     */
    public function saveCommentToParent(Comment $comment, Comment $parent);

    /**
     * @param Comment $comment
     * @param Post    $post
     */
    public function saveCommentToPost(Comment $comment, Post $post);

    /**
     * @param Comment $comment
     * @param int     $limit
     * @param int     $offset
     *
     * @return Collection|Comment[]
     */
    public function getCommentsForComment(Comment $comment, $limit = 100, $offset = 0);

    /**
     * @param Post $post
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|Post[]
     */
    public function getCommentsForPost(Post $post, $limit = 100, $offset = 0);
}