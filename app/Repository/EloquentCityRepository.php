<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Exceptions\EntityNotFoundException;
use StudentskePrakse\Model\City;
use StudentskePrakse\Model\Internship;

class EloquentCityRepository implements CityRepositoryInterface
{

    /**
     * @var City
     */
    private $city;

    /**
     * EloquentCityRepository constructor.
     *
     * @param City $city
     */
    public function __construct(City $city)
    {
        $this->city = $city;
    }

    /**
     * @return Collection|City[]
     */
    public function getAll()
    {
        return $this->city->all();
    }

    /**
     * @param int $id
     *
     * @return City|null
     */
    public function find($id)
    {
        return $this->city->query()->find($id);
    }

    /**
     * @param string $name
     *
     * @return Collection|City[]
     */
    public function getByName($name)
    {
        return $this->city->query()->where('name', 'LIKE', '%'.$name.'%')->get();
    }

    /**
     * @param int $id
     *
     * @return City
     * @throws EntityNotFoundException
     */
    public function findOrFail($id)
    {
        $city = $this->find($id);
        if ($city === null) {
            throw new EntityNotFoundException("City not found");
        }
        return $city;
    }

    /**
     * @return Collection|City[]
     */
    public function getCitiesWithInternships()
    {
        return $this->city->query()->whereIn('id', function ($query) {
            $query->distinct()->select('city_id')
                ->from(with(new Internship)->getTable())
                ->where('status', Internship::STATUS_PUBLISHED)
                ->whereNull('deleted_at');
        })->orderBy('name')
            ->get();
    }
}