<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 2/4/2016
 * Time: 4:24 PM
 */

namespace StudentskePrakse\Repository;


use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Model\Post;
use StudentskePrakse\Model\User;

interface PostRepositoryInterface
{

    /**
     * @param Post $post
     */
    public function save(Post $post);

    /**
     * @return Collection|Post[]
     */
    public function getAll();

    /**
     * @param int $id
     *
     * @return Post|null
     */
    public function find($id);

    /**
     * @param int $id
     *
     * @return Post
     */
    public function findOrFail($id);

    /**
     * @param Post $post
     */
    public function delete(Post $post);

    /**
     * @param string $slug
     *
     * @return Post|null
     */
    public function findBySlug($slug);

    /**
     * @param string $slug
     *
     * @return Post
     */
    public function findBySlugOrFail($slug);

    /**
     * @param Post $post
     * @param User $user
     */
    public function savePostToUser(Post $post, User $user);

    /**
     * @param User $user
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|\StudentskePrakse\Model\Post[]
     */
    public function getPostsForUser(User $user, $limit = 100, $offset = 0);
}