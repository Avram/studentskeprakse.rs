<?php namespace StudentskePrakse\Repository;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Exceptions\EntityNotFoundException;
use StudentskePrakse\Model\FAQ;
use StudentskePrakse\Model\Internship;
use StudentskePrakse\Model\User;

class EloquentFAQRepository implements FAQRepositoryInterface
{

    /**
     * @var FAQ
     */
    private $faq;

    /**
     * EloquentFAQRepository constructor.
     *
     * @param FAQ $faq
     */
    public function __construct(FAQ $faq)
    {
        $this->faq = $faq;
    }

    /**
     * @param FAQ $faq
     */
    public function save(FAQ $faq)
    {
        $faq->save();
    }

    /**
     * @return Collection|FAQ[]
     */
    public function getAll()
    {
        return $this->faq->all();
    }

    /**
     * @param int $id
     *
     * @return FAQ|null
     */
    public function find($id)
    {
        return $this->faq->query()->find($id);
    }

    /**
     * @param FAQ $faq
     *
     * @throws \Exception
     */
    public function delete(FAQ $faq)
    {
        $faq->delete();
    }

    /**
     * @param FAQ  $faq
     * @param User $user
     */
    public function saveFAQtoUser(FAQ $faq, User $user)
    {
        $user->faqs()->save($faq);
    }

    /**
     * @param FAQ        $faq
     * @param Internship $internship
     */
    public function saveFAQtoInternship(FAQ $faq, Internship $internship)
    {
        $internship->faqs()->save($faq);
    }


    /**
     * @param User $user
     * @param bool $includeUnanswered
     * @param int  $limit
     * @param int  $offset
     *
     * @return Collection|FAQ[]
     */
    public function getFAQsForUser(User $user, $includeUnanswered = false, $limit = 100, $offset = 0)
    {
        $faqs = $user->faqs();

        if (!$includeUnanswered) {
            $faqs->whereNotNull('answer');
        }

        return $faqs->orderBy('updated_at', 'DESC')
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param Internship $internship
     * @param bool       $includeUnanswered
     * @param int        $limit
     * @param int        $offset
     *
     * @return Collection|FAQ[]
     */
    public function getFAQsForInternship(Internship $internship, $includeUnanswered = false, $limit = 100, $offset = 0)
    {
        $faqs = $internship->faqs();

        if ($includeUnanswered) {
            $faqs->orderBy('answer');
        } else {
            $faqs->whereNotNull('answer');
        }

        return $faqs->skip($offset)
            ->take($limit)
            ->get();
    }

    /**
     * @param FAQ $faq
     *
     * @return Internship|User
     */
    public function getOwner(FAQ $faq)
    {
        return $faq->owner;
    }

    /**
     * @param int $id
     *
     * @return FAQ
     * @throws EntityNotFoundException
     */
    public function findOrFail($id)
    {
        $faq = $this->find($id);
        if ($faq === null) {
            throw new EntityNotFoundException("FAQ not found");
        }

        return $faq;
    }
}