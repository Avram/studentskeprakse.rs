<?php namespace StudentskePrakse\Presenter;

class User extends Presenter
{

    public function logo()
    {
        if (empty($this->entity->logo)) {
            return route('company.profile.dynamicLogo', [$this->entity->slug]);
        }

        return $this->uploadDirectory().DIRECTORY_SEPARATOR.$this->entity->logo;
    }

    public function cover()
    {
        return $this->uploadDirectory().DIRECTORY_SEPARATOR.$this->entity->cover;
    }

    public function description()
    {
        return nl2br($this->entity->description);
    }
}