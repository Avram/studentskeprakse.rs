<?php namespace StudentskePrakse\Presenter;

use Carbon\Carbon;
use StudentskePrakse\Model\Internship as InternshipModel;

class Internship extends Presenter
{
    /**
     * @return string
     */
    public function duration()
    {
        switch ($this->entity->getDurationType()) {
            case InternshipModel::DURATION_DAYS:
                $duration = 'dana';
                break;
            case InternshipModel::DURATION_WEEKS:
                $duration = 'nedelja';
                break;
            case InternshipModel::DURATION_MONTHS:
                $duration = 'meseci';
                break;
            default:
                $duration = '-';
        }

        return $this->entity->getDuration().' '.$duration;
    }

    /**
     * @return string
     */
    public function workingHours()
    {
        $workingHours = $this->entity->getWorkHours();
        return empty($workingHours) ? '&mdash;' : $workingHours.' sati';
    }

    /**
     * @return string
     */
    public function salary()
    {
        $salary = $this->entity->getSalary();
        return (empty($salary)) ? 'neplaćena' : $salary.' RSD';
    }

    /**
     * @return string
     */
    public function mentors()
    {
        $mentors = $this->entity->mentors;
        $output  = '';

        foreach ($mentors as $mentor) {
            $output .= $mentor->getName().'<br />';
        }

        return $output;
    }

    /**
     * @return string
     */
    public function meals()
    {
        $meals  = $this->entity->getMeals();
        $output = '';

        foreach ($meals as $meal) {
            $meal = str_replace([
                InternshipModel::MEAL_BREAKFAST,
                InternshipModel::MEAL_LUNCH,
                InternshipModel::MEAL_DINNER
            ], [
                'Doručak',
                'Ručak',
                'Večera'
            ], $meal);

            $output .= $meal.'<br />';
        }

        return $output;
    }

    public function url()
    {
        $userSlug = $this->entity->user->slug;
        return route('company.internship', [$userSlug, $this->entity->slug]);
    }

    public function published($format = 'd.m.Y.')
    {
        /** @var Carbon $pubDate */
        $pubDate = $this->entity->created_at;
        return $pubDate->format($format);
    }

    public function content()
    {
        return nl2br($this->entity->content);
    }

    public function requiredSkills($cssClass = 'internship-description')
    {
        if (empty($this->entity->required_skills)) {
            return '';
        }

        $html  = '<ul class="'.$cssClass.'">'.PHP_EOL;
        $array = preg_split("/\r\n|\n|\r/", $this->entity->required_skills);

        if (count($array) < 2) {
            return '<p class="'.$cssClass.'">'.$this->entity->required_skills.'</p>';
        }

        foreach ($array as $skill) {
            $skill = trim($skill);
            if (!empty($skill)) {
                $html .= '<li>'.$skill.'</li>'.PHP_EOL;
            }
        }
        return $html.'</ul>'.PHP_EOL;
    }
}