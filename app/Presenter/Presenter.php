<?php namespace StudentskePrakse\Presenter;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

abstract class Presenter
{
    /** @var Model $entity */
    protected $entity;

    /**
     * Presenter constructor.
     *
     * @param Model $entity
     */
    public function __construct(Model $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param string $baseDir
     *
     * @return string
     */
    protected function uploadDirectory($baseDir = 'uploads')
    {
        /** @var Carbon $carbon */
        $carbon = $this->entity->created_at;
        return $baseDir.DIRECTORY_SEPARATOR.$carbon->year.DIRECTORY_SEPARATOR.$carbon->month;
    }
}