<?php namespace StudentskePrakse\Presenter;


use StudentskePrakse\Presenter\Exception\PresenterException;

trait PresentableTrait
{
    protected $presenterInstance;

    /**
     * @return mixed
     * @throws PresenterException
     */
    public function present()
    {
        if (!$this->presenter || !class_exists($this->presenter)) {
            throw new PresenterException('Please set protected $presenter to your presenter class.');
        }

        if (!$this->presenterInstance) {
            $this->presenterInstance = new $this->presenter($this);
        }

        return $this->presenterInstance;
    }
}