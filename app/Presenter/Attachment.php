<?php namespace StudentskePrakse\Presenter;

use Symfony\Component\HttpFoundation\File\File;

class Attachment extends Presenter
{

    /**
     * @return string
     */
    public function attachment()
    {
        return $this->uploadDirectory().DIRECTORY_SEPARATOR.$this->entity->attachment;
    }


    /**
     * @return string
     */
    public function attachmentThumbnail()
    {
        return $this->uploadDirectory().DIRECTORY_SEPARATOR.'thumb-'.$this->entity->attachment;
    }


    /**
     * @return string
     */
    public function pdfNiceFileName()
    {
        $file      = new File(public_path($this->attachment()));
        $extension = $file->getExtension();
        $baseName  = $file->getBasename('.'.$extension);
        $parts     = explode('-', $baseName);
        array_pop($parts);

        return implode('-', $parts).'.'.$extension;
    }

    /**
     * @return string
     */
    public function autoLinkValue()
    {
        $urlRegex = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/';
        return preg_replace($urlRegex, '<a href="$0" target="_blank">$0</a>', strip_tags($this->entity->value));
    }
}