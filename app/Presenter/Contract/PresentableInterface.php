<?php

namespace StudentskePrakse\Presenter\Contract;

interface PresentableInterface
{
    public function present();
}