<?php

namespace StudentskePrakse\Http\ViewComposers;

use Illuminate\View\View;
use StudentskePrakse\Repository\CategoryRepositoryInterface;
use StudentskePrakse\Repository\CityRepositoryInterface;

class SharedVariablesComposer
{

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * @var CityRepositoryInterface
     */
    private $cityRepo;

    /**
     * SharedVariablesComposer constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepo
     * @param CityRepositoryInterface     $cityRepo
     */
    public function __construct(CategoryRepositoryInterface $categoryRepo, CityRepositoryInterface $cityRepo)
    {
        $this->categoryRepo = $categoryRepo;
        $this->cityRepo     = $cityRepo;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $categories = $this->categoryRepo->getCategoriesWithInternships();
        $cities     = $this->cityRepo->getCitiesWithInternships();

        $filters = [
            ['id' => 'paid', 'label' => 'Plaćena', 'name' => 'paid', 'value' => 'paid', 'class' => '', 'checked' => ''],
            ['id' => 'breakfast', 'label' => 'Doručak', 'name' => 'food[breakfast]', 'value' => 'breakfast', 'class' => '', 'checked' => ''],
            ['id' => 'lunch', 'label' => 'Ručak', 'name' => 'food[lunch]', 'value' => 'lunch', 'class' => '', 'checked' => ''],
            ['id' => 'dinner', 'label' => 'Večera', 'name' => 'food[dinner]', 'value' => 'dinner', 'class' => '', 'checked' => ''],
        ];

        //recaptcha site key
        $reCaptchaSiteKey = env('RECAPTCHA_SITE_KEY', '');

        $view->with([
            'searchCategories' => $categories,
            'searchCities'     => $cities,
            'searchFilters'    => $filters,
            'reCaptchaSiteKey' => $reCaptchaSiteKey,
        ]);
    }
}