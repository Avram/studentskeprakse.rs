<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

/**
 * Routes that require authentication
 */
Route::group(['middleware' => ['web', 'auth']], function () {

    /**
     * Administration panel routes
     */
    Route::group(['prefix' => 'admin'], function () {

        Route::get('/', ['as' => 'admin.dash', 'uses' => 'AdminDashboardController@index']);

        /**
         * Internships
         */
        Route::group(['prefix' => 'internships'], function () {
            Route::get('/', ['as' => 'admin.internship.list', 'uses' => 'AdminInternshipController@getInternships']);
            Route::get('/create', ['as' => 'admin.internship.create', 'uses' => 'AdminInternshipController@createInternship']);
            Route::post('/create', ['as' => 'admin.internship.postCreate', 'uses' => 'AdminInternshipController@postCreateInternship']);
            Route::get('/edit/{id}', ['as' => 'admin.internship.update', 'uses' => 'AdminInternshipController@updateInternship']);
            Route::post('/edit/{id}', ['as' => 'admin.internship.postUpdate', 'uses' => 'AdminInternshipController@postUpdateInternship']);
            Route::get('/remove/{id}', ['as' => 'admin.internship.delete', 'uses' => 'AdminInternshipController@deleteInternship']);
            Route::get('/{id}', ['as' => 'admin.internship.single', 'uses' => 'AdminInternshipController@getInternship']);
        });

        /**
         * Comments
         */
        Route::group(['prefix' => 'comments'], function () {
            Route::get('/', ['as' => 'admin.comment.list', 'uses' => 'AdminCommentController@getComments']);
            Route::get('remove/{id}', ['as' => 'admin.comment.delete', 'uses' => 'AdminCommentController@deleteComment']);
            Route::get('/{id}', ['as' => 'admin.comment.single', 'uses' => 'AdminCommentController@getComment']);
        });

        /**
         * Post (adviceS)
         */
        Route::group(['prefix' => 'posts'], function () {
            Route::get('/', ['as' => 'admin.post.list', 'uses' => 'AdminPostController@getPosts']);
            Route::get('create', ['as' => 'admin.post.create', 'uses' => 'AdminPostController@createPost']);
            Route::get('edit/{id}', ['as' => 'admin.post.update', 'uses' => 'AdminPostController@updatePost']);
            Route::get('remove/{id}', ['as' => 'admin.post.delete', 'uses' => 'AdminPostController@deletePost']);
            Route::post('create', ['as' => 'admin.post.postCreate', 'uses' => 'AdminPostController@postCreatePost']);
            Route::post('edit/{id}', ['as' => 'admin.post.postUpdate', 'uses' => 'AdminPostController@postUpdatePost']);
            Route::get('/{id}', ['as' => 'admin.post.single', 'uses' => 'AdminPostController@getPost']);
        });

        /**
         * Users
         */
        Route::group(['prefix' => 'users'], function () {
            Route::get('/', ['as' => 'admin.user.list', 'uses' => 'AdminUserController@getUsers']);
            Route::get('create', ['as' => 'admin.user.create', 'uses' => 'AdminUserController@createUser']);
            Route::post('create', ['as' => 'admin.user.postCreate', 'uses' => 'AdminUserController@postCreateUser']);
            Route::get('edit/{id}', ['as' => 'admin.user.update', 'uses' => 'AdminUserController@updateUser']);
            Route::post('edit/{id}', ['as' => 'admin.user.postUpdate', 'uses' => 'AdminUserController@postUpdateUser']);
            Route::get('remove/{id}', ['as' => 'admin.user.delete', 'uses' => 'AdminUserController@deleteUser']);
            Route::get('/{id}', ['as' => 'admin.user.single', 'uses' => 'AdminUserController@getUser']);
        });

        /**
         * Categories
         */
        Route::group(['prefix' => 'categories'], function () {
            Route::get('/', ['as' => 'admin.category.list', 'uses' => 'AdminCategoryController@getCategories']);
            Route::get('create', ['as' => 'admin.category.create', 'uses' => 'AdminCategoryController@createCategory']);
            Route::post('create', ['as' => 'admin.category.postCreate', 'uses' => 'AdminCategoryController@postCreateCategory']);
            Route::get('edit/{id}', ['as' => 'admin.category.update', 'uses' => 'AdminCategoryController@updateCategory']);
            Route::post('edit/{id}', ['as' => 'admin.category.postUpdate', 'uses' => 'AdminCategoryController@postUpdateCategory']);
            Route::get('remove/{id}', ['as' => 'admin.category.delete', 'uses' => 'AdminCategoryController@deleteCategory']);
            Route::get('/{id}', ['as' => 'admin.category.single', 'uses' => 'AdminCategoryController@getCategory']);
        });

        /**
         * Attachments
         */
        Route::group(['prefix' => 'attachments'], function () {
            Route::get('/{uid}', ['as' => 'admin.attachment.list', 'uses' => 'AdminAttachmentController@getAttachments']);
            Route::get('create/{uid}', ['as' => 'admin.attachment.create', 'uses' => 'AdminAttachmentController@createAttachment']);
            Route::post('create/{uid}', ['as' => 'admin.attachment.postCreate', 'uses' => 'AdminAttachmentController@postCreateAttachment']);
            Route::get('edit/{id}', ['as' => 'admin.attachment.update', 'uses' => 'AdminAttachmentController@updateAttachment']);
            Route::post('edit/{id}', ['as' => 'admin.attachment.postUpdate', 'uses' => 'AdminAttachmentController@postUpdateAttachment']);
            Route::get('remove/{id}', ['as' => 'admin.attachment.delete', 'uses' => 'AdminAttachmentController@deleteAttachment']);
        });

        /**
         * Mentors
         */
        Route::group(['prefix' => 'mentors'], function () {
            Route::get('/{uid}', ['as' => 'admin.mentor.list', 'uses' => 'AdminMentorController@getMentors']);
            Route::get('create/{uid}', ['as' => 'admin.mentor.create', 'uses' => 'AdminMentorController@createMentor']);
            Route::post('create/{uid}', ['as' => 'admin.mentor.postCreate', 'uses' => 'AdminMentorController@postCreateMentor']);
            Route::get('edit/{id}', ['as' => 'admin.mentor.update', 'uses' => 'AdminMentorController@updateMentor']);
            Route::post('edit/{id}', ['as' => 'admin.mentor.postUpdate', 'uses' => 'AdminMentorController@postUpdateMentor']);
            Route::get('remove/{id}', ['as' => 'admin.mentor.delete', 'uses' => 'AdminMentorController@deleteMentor']);
        });

        /**
         * Feedbacks
         */
        Route::group(['prefix' => 'feedbacks'], function () {
            Route::get('/{uid}', ['as' => 'admin.feedback.list', 'uses' => 'AdminFeedbackController@getFeedbacks']);
            Route::get('create/{uid}', ['as' => 'admin.feedback.create', 'uses' => 'AdminFeedbackController@createFeedback']);
            Route::post('create/{uid}', ['as' => 'admin.feedback.postCreate', 'uses' => 'AdminFeedbackController@postCreateFeedback']);
            Route::get('edit/{id}', ['as' => 'admin.feedback.update', 'uses' => 'AdminFeedbackController@updateFeedback']);
            Route::post('edit/{id}', ['as' => 'admin.feedback.postUpdate', 'uses' => 'AdminFeedbackController@postUpdateFeedback']);
            Route::get('remove/{id}', ['as' => 'admin.feedback.delete', 'uses' => 'AdminFeedbackController@deleteFeedback']);
        });

        /**
         * FAQs
         */
        Route::group(['prefix' => 'faqs'], function () {
            Route::get('create/{ownerType}/{ownerId}', ['as' => 'admin.faq.create', 'uses' => 'AdminFAQController@createFAQ']);
            Route::post('create/{ownerType}/{ownerId}', ['as' => 'admin.faq.postCreate', 'uses' => 'AdminFAQController@postCreateFAQ']);
            Route::get('edit/{id}', ['as' => 'admin.faq.update', 'uses' => 'AdminFAQController@updateFAQ']);
            Route::post('edit/{id}', ['as' => 'admin.faq.postUpdate', 'uses' => 'AdminFAQController@postUpdateFAQ']);
            Route::get('remove/{id}', ['as' => 'admin.faq.delete', 'uses' => 'AdminFAQController@deleteFAQ']);
            Route::get('/{ownerType}/{ownerId}', ['as' => 'admin.faq.list', 'uses' => 'AdminFAQController@getFAQs']);
        });
    });


});

/**
 * Public routes
 */
Route::group(['middleware' => 'web'], function () {
    Route::get('style', function () {
        return view('styles');
    });

    Route::group(['prefix' => 'ajax'], function () {
        Route::post('feedback', ['as' => 'ajax.feedback.reply', 'uses' => 'UserController@postFeedbackReply']);
        Route::post('faqs', ['as' => 'ajax.faq.reply', 'uses' => 'UserController@postFAQReply']);

        Route::group(['prefix' => 'internship'], function () {
            Route::post('toggle-status', ['as' => 'ajax.internship.toggleStatus', 'uses' => 'InternshipController@ajaxToggleStatus']);
        });

        Route::group(['prefix' => 'offices'], function () {
            Route::post('upload', ['as' => 'ajax.offices.uploadImage', 'uses' => 'FileUploadController@uploadOfficeImage']);
            Route::post('remove', ['as' => 'ajax.offices.removeImage', 'uses' => 'FileUploadController@removeOfficeImage']);
            Route::post('updateOrder', ['as' => 'ajax.offices.updateOrder', 'uses' => 'FileUploadController@updateOfficeImagesOrder']);
        });

        Route::group(['prefix' => 'user'], function () {
            Route::post('upload-logo', ['as' => 'ajax.user.uploadLogo', 'uses' => 'FileUploadController@uploadLogo']);
            Route::post('upload-cover', ['as' => 'ajax.user.uploadCover', 'uses' => 'FileUploadController@uploadCover']);
        });

        Route::group(['prefix' => 'attachments'], function () {
            Route::post('remove', ['as' => 'ajax.attachments.removeAttachment', 'uses' => 'AttachmentController@removeAttachment']);
        });
    });

    Route::group(['prefix' => 'internship'], function () {
        Route::get('create', ['as' => 'internship.create', 'uses' => 'InternshipController@createInternship']);
        Route::post('create', ['as' => 'internship.postCreate', 'uses' => 'InternshipController@postCreateInternship']);
//        Route::get('edit/{slug}', ['as' => 'internship.update', 'uses' => 'InternshipController@updateInternship']);
//        Route::post('edit/{slug}', ['as' => 'internship.postUpdate', 'uses' => 'InternshipController@postUpdateInternship']);
        Route::get('remove/{slug}', ['as' => 'internship.delete', 'uses' => 'InternshipController@deleteInternship']);
        Route::get('trash/{slug}', ['as' => 'internship.softDelete', 'uses' => 'InternshipController@softDeleteInternship']);
        Route::get('restore/{slug}', ['as' => 'internship.softRestore', 'uses' => 'InternshipController@softRestoreInternship']);
        Route::get('duplicate/{slug}', ['as' => 'internship.duplicate', 'uses' => 'InternshipController@duplicateInternship']);

    });

    Route::group(['prefix' => 'comments'], function () {
        Route::post('/create/{post_id}', ['as' => 'comment.create', 'uses' => 'CommentController@createComment']);
    });

    Route::group(['prefix' => 'cities'], function () {
        Route::get('/search', ['uses' => 'CityController@findCity']);
        Route::get('/{id}', ['uses' => 'CityController@getCity']);
    });

    Route::group(['prefix' => 'posts'], function () {
        Route::get('/', ['as' => 'post.list', 'uses' => 'PostController@getPosts']);
        Route::get('/{slug}', ['as' => 'post.single', 'uses' => 'PostController@getPost']);
    });


    Route::get('email/verify/resend/{id}', ['as' => 'auth.emailResend', 'uses' => 'AuthController@resendVerificationEmail']);
    Route::get('email/verify/{token}', ['as' => 'auth.emailVerify', 'uses' => 'AuthController@verifyEmail']);
    Route::get('register', ['as' => 'auth.register', 'uses' => 'AuthController@getRegistration']);
    Route::post('register', ['as' => 'auth.postRegister', 'uses' => 'AuthController@postRegistration']);
    Route::get('password/reset/{token}', ['as' => 'auth.passwordReset', 'uses' => 'AuthController@getReset']);
    Route::post('password/reset', ['as' => 'auth.postPasswordReset', 'uses' => 'AuthController@postReset']);
    Route::get('password/email', ['as' => 'auth.passwordEmail', 'uses' => 'AuthController@getEmail']);
    Route::post('password/email', ['as' => 'auth.postPasswordEmail', 'uses' => 'AuthController@postEmail']);
    Route::get('login', ['as' => 'auth.login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth.postLogin', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'auth.logout', 'uses' => 'AuthController@getLogout']);


    Route::get('settings', ['as' => 'company.settings.about', 'uses' => 'UserController@displayAboutSettings']);
    Route::post('settings', ['as' => 'company.settings.postAbout', 'uses' => 'UserController@postAboutSettings']);
    Route::get('settings/social', ['as' => 'company.settings.social', 'uses' => 'UserController@displaySocialSettings']);
    Route::post('settings/social', ['as' => 'company.settings.postSocial', 'uses' => 'UserController@postSocialSettings']);
    Route::get('settings/profile', ['as' => 'company.settings.profile', 'uses' => 'UserController@displayProfileSettings']);
    Route::post('settings/profile', ['as' => 'company.settings.postProfile', 'uses' => 'UserController@postProfileSettings']);


    Route::group(['prefix' => 'my-internships'], function () {
        Route::get('/', ['as' => 'company.internships.management', 'uses' => 'UserController@manageInternships']);
        Route::get('/mentors', ['as' => 'company.mentors.management', 'uses' => 'MentorController@manageMentors']);
        Route::get('/mentors/create', ['as' => 'company.mentors.management.create', 'uses' => 'MentorController@addMentor']);
        Route::post('/mentors/create', ['as' => 'company.mentors.management.postCreate', 'uses' => 'MentorController@postAddMentor']);
        Route::get('/mentors/update/{id}', ['as' => 'company.mentors.management.update', 'uses' => 'MentorController@editMentor']);
        Route::post('/mentors/update/{id}', ['as' => 'company.mentors.management.postUpdate', 'uses' => 'MentorController@postEditMentor']);
        Route::get('/mentors/delete/{id}', ['as' => 'company.mentors.management.delete', 'uses' => 'MentorController@removeMentor']);
        Route::get('/faq', ['as' => 'company.faq.management', 'uses' => 'FAQController@manageFAQs']);
        Route::get('/faq/create', ['as' => 'company.faq.management.create', 'uses' => 'FAQController@addFAQ']);
        Route::post('/faq/create', ['as' => 'company.faq.management.postCreate', 'uses' => 'FAQController@postAddFAQ']);
        Route::get('/faq/update/{id}', ['as' => 'company.faq.management.update', 'uses' => 'FAQController@editFAQ']);
        Route::post('/faq/update/{id}', ['as' => 'company.faq.management.postUpdate', 'uses' => 'FAQController@postEditFAQ']);
        Route::get('/faq/delete/{id}', ['as' => 'company.faq.management.delete', 'uses' => 'FAQController@removeFAQ']);
        Route::get('/feedback', ['as' => 'company.feedback.management', 'uses' => 'FeedbackController@manageFeedbacks']);
        Route::get('/feedback/update/{id}', ['as' => 'company.feedback.management.update', 'uses' => 'FeedbackController@updateFeedbacks']);
        Route::post('/feedback/update/{id}', ['as' => 'company.feedback.management.postUpdate', 'uses' => 'FeedbackController@postUpdateFeedbacks']);
        Route::get('/feedback/delete/{id}', ['as' => 'company.feedback.management.delete', 'uses' => 'FeedbackController@deleteFeedbacks']);
        Route::get('/trashed', ['as' => 'company.internships.trash', 'uses' => 'InternshipController@trashed']);
    });


    Route::get('{username}', ['as' => 'company.profile', 'uses' => 'UserController@displayProfile']);
    Route::get('{username}/logo', ['as' => 'company.profile.dynamicLogo', 'uses' => 'UserController@dynamicLogo']);
//    Route::get('{username}/internships', ['as' => 'company.internships', 'uses' => 'UserController@displayInternships']);
//    Route::get('{username}/about', ['as' => 'company.about', 'uses' => 'UserController@displayAbout']);
//    Route::get('{username}/feedback', ['as' => 'company.feedback', 'uses' => 'UserController@displayFeedback']);
    Route::post('{username}/{internship}/feedback', ['as' => 'company.feedback.post', 'uses' => 'UserController@postFeedback']);
//    Route::get('{username}/offices', ['as' => 'company.offices', 'uses' => 'UserController@displayOffices']);
//    Route::get('{username}/attachments', ['as' => 'company.attachments', 'uses' => 'UserController@displayAttachments']);
    Route::post('{username}/{internship}/attachments', ['as' => 'company.attachments.post', 'uses' => 'AttachmentController@postAttachment']);
    Route::post('{username}/{internship}/faq', ['as' => 'company.faq.post', 'uses' => 'UserController@postFAQ']);
//    Route::get('{username}/faq', ['as' => 'company.faqs', 'uses' => 'UserController@displayFAQs']);

    /**----------------------------------------------- new Internship routes ------------------------------------------------------**/

    Route::get('{username}/{internship}', ['as' => 'company.internship', 'uses' => 'InternshipController@displayInternship']);

    Route::get('{username}/{internship}/edit', ['as' => 'company.internship.edit', 'uses' => 'InternshipController@updateInternship']);
    Route::post('{username}/{internship}/edit', ['as' => 'company.internship.postEdit', 'uses' => 'InternshipController@postUpdateInternship']);
    Route::get('{username}/{internship}/edit/about', ['as' => 'company.about.edit', 'uses' => 'UserController@displayAboutSettings']);
    Route::post('{username}/{internship}/edit/about', ['as' => 'company.about.postEdit', 'uses' => 'UserController@postAboutSettings']);
    Route::get('{username}/{internship}/edit/feedback', ['as' => 'company.feedback.edit', 'uses' => 'UserController@manageFeedback']);
    Route::post('{username}/{internship}/edit/feedback', ['as' => 'company.feedback.postEdit', 'uses' => 'UserController@postUpdateFeedback']);
//    Route::get('{username}/{internship}/edit/offices', ['as' => 'company.offices.edit', 'uses' => 'UserController@updateInternship']);
//    Route::post('{username}/{internship}/edit/offices', ['as' => 'company.offices.postEdit', 'uses' => 'UserController@postUpdateInternship']);
//    Route::get('{username}/{internship}/edit/attachments', ['as' => 'company.attachments.edit', 'uses' => 'UserController@updateInternship']);
//    Route::post('{username}/{internship}/edit/attachments', ['as' => 'company.attachments.postEdit', 'uses' => 'UserController@postUpdateInternship']);
    Route::get('{username}/{internship}/edit/faq', ['as' => 'company.faq.edit', 'uses' => 'UserController@manageFAQ']);
    Route::post('{username}/{internship}/edit/faq', ['as' => 'company.faq.postEdit', 'uses' => 'UserController@postUpdateFAQ']);

    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
    Route::post('/', ['as' => 'home.search', 'uses' => 'HomeController@search']);

});

