<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Http\Response;
use StudentskePrakse\Http\Requests;
use StudentskePrakse\Http\Requests\Admin\CreateFAQRequest;
use StudentskePrakse\Http\Requests\Admin\DeleteFAQRequest;
use StudentskePrakse\Http\Requests\Admin\UpdateFAQRequest;
use StudentskePrakse\Model\FAQ;
use StudentskePrakse\Repository\FAQRepositoryInterface;
use StudentskePrakse\Repository\InternshipRepositoryInterface;
use StudentskePrakse\Repository\UserRepositoryInterface;

class AdminFAQController extends Controller
{
    /**
     * @var FAQRepositoryInterface
     */
    private $faqRepo;

    /**
     * @var UserRepositoryInterface
     */

    private $userRepo;
    /**
     * @var InternshipRepositoryInterface
     */
    private $internshipRepo;

    /**
     * AdminFAQController constructor.
     *
     * @param FAQRepositoryInterface        $faqRepo
     * @param UserRepositoryInterface       $userRepo
     * @param InternshipRepositoryInterface $internshipRepo
     */
    public function __construct(FAQRepositoryInterface $faqRepo, UserRepositoryInterface $userRepo, InternshipRepositoryInterface $internshipRepo)
    {
        $this->faqRepo        = $faqRepo;
        $this->userRepo       = $userRepo;
        $this->internshipRepo = $internshipRepo;
    }

    /**
     * @param $ownerType
     * @param $ownerId
     *
     * @return Response
     */
    public function getFAQs($ownerType, $ownerId)
    {
        switch ($ownerType) {
            case FAQ::OWNER_USER :
                $owner = $this->userRepo->findOrFail($ownerId);
                $faqs  = $this->faqRepo->getFAQsForUser($owner);
                break;
            case FAQ::OWNER_INTERNSHIP :
                $owner = $this->internshipRepo->findOrFail($ownerId);
                $faqs  = $this->faqRepo->getFAQsForInternship($owner);
                break;
            default:
                throw new \InvalidArgumentException("Unknown owner type $ownerType. Must be one of: ".implode(', ', FAQ::$ALLOWED_OWNERS));
        }

        return view('faq.admin.faqList', [
            'owner'      => $owner,
            'owner_type' => $ownerType,
            'faqs'       => $faqs,
        ]);
    }

    /**
     * @param $ownerType
     * @param $ownerId
     *
     * @return Response
     */
    public function createFAQ($ownerType, $ownerId)
    {
        switch ($ownerType) {
            case FAQ::OWNER_USER :
                $owner = $this->userRepo->findOrFail($ownerId);
                break;
            case FAQ::OWNER_INTERNSHIP :
                $owner = $this->internshipRepo->findOrFail($ownerId);
                break;
            default:
                throw new \InvalidArgumentException("Unknown owner type $ownerType. Must be one of: ".implode(', ', FAQ::$ALLOWED_OWNERS));
        }

        $faq = new FAQ();

        return view('faq.admin.faqForm', [
            'owner'      => $owner,
            'owner_type' => $ownerType,
            'faq'        => $faq,
        ]);
    }

    /**
     * @param CreateFAQRequest $request
     * @param                  $ownerType
     * @param                  $ownerId
     *
     * @return Response
     */
    public function postCreateFAQ(CreateFAQRequest $request, $ownerType, $ownerId)
    {

        $input = $request->only([
            'question',
            'answer',
        ]);

        $newFAQ = FAQ::make(
            $input['question'],
            $input['answer']
        );

        $this->faqRepo->save($newFAQ);

        switch ($ownerType) {
            case FAQ::OWNER_USER :
                $owner = $this->userRepo->findOrFail($ownerId);
                $this->faqRepo->saveFAQtoUser($newFAQ, $owner);
                break;
            case FAQ::OWNER_INTERNSHIP :
                $owner = $this->internshipRepo->findOrFail($ownerId);
                $this->faqRepo->saveFAQtoInternship($newFAQ, $owner);
                break;
            default:
                throw new \InvalidArgumentException("Unknown owner type $ownerType. Must be one of: ".implode(', ', FAQ::$ALLOWED_OWNERS));
        }

        return redirect()->route('admin.faq.faqList', [$ownerType, $owner->getId()]);

    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function updateFAQ($id)
    {
        $faq = $this->faqRepo->findOrFail($id);

        $owner     = $this->faqRepo->getOwner($faq);
        $ownerType = $faq->getOwnerType();

        return view('faq.admin.faqForm', [
            'user'       => $owner,
            'owner_type' => $ownerType,
            'faq'        => $faq,
        ]);
    }

    /**
     * @param UpdateFAQRequest $request
     * @param                  $id
     *
     * @return Response
     */
    public function postUpdateFAQ(UpdateFAQRequest $request, $id)
    {
        $faq = $this->faqRepo->findOrFail($id);

        $owner = $this->faqRepo->getOwner($faq);

        $input = $request->only([
            'question',
            'answer',
        ]);

        $faq->setQuestion($input['question']);
        $faq->setAnswer($input['answer']);

        $this->faqRepo->save($faq);

        $ownerType = $faq->getOwnerType();

        return redirect()->route('admin.faq.faqList', [$ownerType, $owner->getId()]);
    }

    /**
     * @param DeleteFAQRequest $request
     * @param                  $id
     *
     * @return Response
     */
    public function deleteFAQ(DeleteFAQRequest $request, $id)
    {
        $faq       = $this->faqRepo->findOrFail($id);
        $owner     = $this->faqRepo->getOwner($faq);
        $ownerType = $faq->getOwnerType();
        $this->faqRepo->delete($faq);
        return redirect()->route('admin.faq.faqList', [$ownerType, $owner->getId()]);
    }

}
