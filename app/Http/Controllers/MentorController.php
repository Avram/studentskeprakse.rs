<?php namespace StudentskePrakse\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Mentor\CreateMentorRequest;
use StudentskePrakse\Http\Requests\Mentor\DeleteMentorRequest;
use StudentskePrakse\Http\Requests\Mentor\EditMentorRequest;
use StudentskePrakse\Http\Requests\User\EditProfileRequest;
use StudentskePrakse\Model\Mentor;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\MentorRepositoryInterface;
use Symfony\Component\HttpFoundation\Response;

class MentorController extends Controller
{
    /**
     * @var Guard
     */
    private $guard;
    /**
     * @var MentorRepositoryInterface
     */
    private $mentorRepo;

    /**
     * MentorController constructor.
     *
     * @param Guard                     $guard
     * @param MentorRepositoryInterface $mentorRepo
     */
    public function __construct(Guard $guard, MentorRepositoryInterface $mentorRepo)
    {
        $this->guard      = $guard;
        $this->mentorRepo = $mentorRepo;
    }

    /**
     * @param EditProfileRequest $request
     *
     * @return Response
     */
    public function manageMentors(EditProfileRequest $request)
    {
        /** @var User $user */
        $user    = $this->guard->user();
        $mentors = $this->mentorRepo->getMentorsForUser($user);

        return view('mentor.mentorList', [
            'mentors' => $mentors,
        ]);
    }

    /**
     * @param EditProfileRequest $request
     *
     * @return Response
     */
    public function addMentor(EditProfileRequest $request)
    {
        $mentor = new Mentor;
        return view('mentor.mentorForm', ['mentor' => $mentor]);
    }

    /**
     * @param CreateMentorRequest $request
     *
     * @return Response
     */
    public function postAddMentor(CreateMentorRequest $request)
    {
        /** @var User $user */
        $user = $this->guard->user();

        $input = $request->only([
            'name',
            'description',
            'url',
            'facebook_url',
            'twitter_url',
            'linkedin_url',
        ]);

        $mentor = Mentor::make('', $input['name'], $input['description'], $input['twitter_url'], $input['facebook_url'], $input['linkedin_url'], $input['url']);

        $this->mentorRepo->save($mentor);
        $this->mentorRepo->saveMentorToUser($mentor, $user);

        return redirect()->route('company.mentors.management');
    }

    /**
     * @param EditProfileRequest $request
     * @param                    $id
     *
     * @return Response
     */
    public function editMentor(EditProfileRequest $request, $id)
    {
        $mentor = $this->mentorRepo->findOrFail($id);
        return view('mentor.mentorForm', ['mentor' => $mentor]);
    }

    /**
     * @param EditMentorRequest $request
     * @param                   $id
     *
     * @return Response
     */
    public function postEditMentor(EditMentorRequest $request, $id)
    {
        $input = $request->only([
            'name',
            'description',
            'url',
            'facebook_url',
            'twitter_url',
            'linkedin_url',
        ]);

        $mentor = $this->mentorRepo->findOrFail($id);

        $mentor->setName($input['name']);
        $mentor->setDescription($input['description']);
        $mentor->setUrl($input['url']);
        $mentor->setTwitterUrl($input['twitter_url']);
        $mentor->setFacebookUrl($input['facebook_url']);
        $mentor->setLinkedinUrl($input['linkedin_url']);

        $this->mentorRepo->save($mentor);

        return redirect()->route('company.mentors.management');
    }

    /**
     * @param DeleteMentorRequest $request
     * @param                     $id
     *
     * @return Response
     */
    public function removeMentor(DeleteMentorRequest $request, $id)
    {
        $mentor = $this->mentorRepo->findOrFail($id);
        $this->mentorRepo->delete($mentor);
        return redirect()->route('company.mentors.management');
    }


}