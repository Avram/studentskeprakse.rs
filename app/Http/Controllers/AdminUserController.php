<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use StudentskePrakse\Http\Requests;
use StudentskePrakse\Http\Requests\Admin\CreateUserRequest;
use StudentskePrakse\Http\Requests\Admin\DeleteUserRequest;
use StudentskePrakse\Http\Requests\Admin\UpdateUserRequest;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\CategoryRepositoryInterface;
use StudentskePrakse\Repository\CityRepositoryInterface;
use StudentskePrakse\Repository\UserRepositoryInterface;

class AdminUserController extends Controller
{
    private $userRepo;
    private $cityRepo;
    private $categoryRepo;

    /**
     * AdminUserController constructor.
     *
     * @param UserRepositoryInterface     $userRepo
     * @param CityRepositoryInterface     $cityRepo
     * @param CategoryRepositoryInterface $categoryRepo
     */
    public function __construct(UserRepositoryInterface $userRepo, CityRepositoryInterface $cityRepo, CategoryRepositoryInterface $categoryRepo)
    {
        $this->userRepo     = $userRepo;
        $this->cityRepo     = $cityRepo;
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        $users = $this->userRepo->getAll();
        return view('user.admin.userList', ['users' => $users]);
    }

    /**
     * @return Response
     */
    public function createUser()
    {
        $user       = new User();
        $cities     = $this->cityRepo->getAll();
        $categories = $this->categoryRepo->getAll();

        return view('user.admin.userForm', [
            'user'       => $user,
            'cities'     => $cities,
            'categories' => $categories,
            'userTypes'  => User::$VALID_TYPES
        ]);
    }

    /**
     * @param CreateUserRequest $request
     * @param Hasher            $hasher
     *
     * @return Response
     */
    public function postCreateUser(CreateUserRequest $request, Hasher $hasher)
    {
        $input = $request->only([
            'name',
            'email',
            'password',
            'type',
            'employees',
            'founded',
            'address',
            'description',
            'url',
            'facebook_url',
            'twitter_url',
            'linkedin_url',
            'category_id',
            'city_id',
        ]);

        $newUser = User::make(
            $input['name'],
            $input['email'],
            $hasher->make($input['password']),
            $input['type'],
            $input['employees'],
            $input['founded'],
            $input['address'],
            $input['description'],
            $input['url'],
            $input['facebook_url'],
            $input['twitter_url'],
            $input['linkedin_url']
        );

        $this->userRepo->save($newUser);

        $cat_id  = $input['category_id'];
        $city_id = $input['city_id'];

        $category = $this->categoryRepo->findOrFail($cat_id);
        $city     = $this->cityRepo->findOrFail($city_id);

        $this->userRepo->saveUserToCategory($newUser, $category);
        $this->userRepo->saveUserToCity($newUser, $city);

        return redirect()->route('admin.user.userList');
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function updateUser($id)
    {
        $user = $this->userRepo->findOrFail($id);

        $cities     = $this->cityRepo->getAll();
        $categories = $this->categoryRepo->getAll();

        return view('user.admin.userForm', [
            'user'       => $user,
            'cities'     => $cities,
            'categories' => $categories,
            'userTypes'  => User::$VALID_TYPES
        ]);
    }

    /**
     * @param UpdateUserRequest $request
     * @param Hasher            $hasher
     * @param                   $id
     *
     * @return Response
     */
    public function postUpdateUser(UpdateUserRequest $request, Hasher $hasher, $id)
    {
        $user = $this->userRepo->findOrFail($id);

        $input = $request->only([
            'name',
            'email',
            'password',
            'type',
            'employees',
            'founded',
            'address',
            'description',
            'url',
            'facebook_url',
            'twitter_url',
            'linkedin_url',
            'category_id',
            'city_id',
        ]);

        $user->setName($input['name']);
        $user->setEmail($input['email']);
        if ($input['password'] !== '') {
            $user->setPassword($hasher->make($input['password']));
        }
        $user->setFounded($input['founded']);
        $user->setType($input['type']);
        $user->setEmployees($input['employees']);
        $user->setFounded($input['founded']);
        $user->setAddress($input['address']);
        $user->setDescription($input['description']);
        $user->setUrl($input['url']);
        $user->setFacebookUrl($input['facebook_url']);
        $user->setTwitterUrl($input['twitter_url']);
        $user->setLinkedinUrl($input['linkedin_url']);
        $user->setSlug(Str::slug($input['name']));

        $this->userRepo->save($user);

        $cat_id  = $input['category_id'];
        $city_id = $input['city_id'];

        $category = $this->categoryRepo->findOrFail($cat_id);
        $city     = $this->cityRepo->findOrFail($city_id);

        $this->userRepo->saveUserToCategory($user, $category);
        $this->userRepo->saveUserToCity($user, $city);


        return redirect()->route('admin.user.userList');
    }

    /**
     * @param DeleteUserRequest $request
     * @param                   $id
     *
     * @return Response
     */
    public function deleteUser(DeleteUserRequest $request, $id)
    {
        $user = $this->userRepo->findOrFail($id);
        $this->userRepo->delete($user);
        return redirect()->route('admin.user.userList');
    }
}
