<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use StudentskePrakse\Http\Requests;
use StudentskePrakse\Model\Internship;
use StudentskePrakse\Repository\CategoryRepositoryInterface;
use StudentskePrakse\Repository\InternshipRepositoryInterface;

class HomeController extends Controller
{

    /**
     * @var InternshipRepositoryInterface
     */
    private $internshipRepo;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * HomeController constructor.
     *
     * @param InternshipRepositoryInterface $internshipRepo
     */
    public function __construct(InternshipRepositoryInterface $internshipRepo, CategoryRepositoryInterface $categoryRepo)
    {
        $this->internshipRepo = $internshipRepo;
        $this->categoryRepo   = $categoryRepo;
    }

    /**
     * @return Response
     */
    public function index()
    {
        $collection = $this->internshipRepo->searchInternships(['status' => Internship::STATUS_PUBLISHED], 10);

        return view('internship.search', ['internships' => $collection]);
    }

    /**
     * @param Request $request
     *
     * @return Collection|Internship[]
     */
    public function search(Request $request)
    {
        $data = $request->only('searchData');
        $data = json_decode($data['searchData'], true);

        $search = [];

        $search['category'] = isset($data['categories']) ? $data['categories'] : null;
        $search['city']     = isset($data['cities']) ? $data['cities'] : null;
        $search['food']     = isset($data['misc']) ? $data['misc'] : null; // @TODO: food
        $search['paid']     = isset($data['paid']) ? $data['paid'] : null;
        $search['term']     = isset($data['term']) ? $data['term'] : null;
        $search['status']   = Internship::STATUS_PUBLISHED;
        $orderBy            = !empty($data['sort']) ? $data['sort'] : 'created_at';
        $offset             = !empty($data['offset']) ? (int)$data['offset'] : 0;
        $order              = ($orderBy == 'created_at') ? 'DESC' : 'ASC';

        $collection = $this->internshipRepo->searchInternships($search, 10, $offset, $orderBy, $order);

        foreach ($collection as $internship) {
            $internship->setSlug($internship->present()->url());
            $internship->user->setLogo($internship->user->present()->logo());
            $internship->city->get();
        }

        return $collection;
    }
}
