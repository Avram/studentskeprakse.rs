<?php

namespace StudentskePrakse\Http\Controllers;

use StudentskePrakse\Http\Requests;
use StudentskePrakse\Repository\PostRepositoryInterface;

class PostController extends Controller
{
    /**
     * @var PostRepositoryInterface
     */
    private $postRepo;

    public function __construct(PostRepositoryInterface $postRepo)
    {
        $this->postRepo = $postRepo;
    }

    public function getAdvices()
    {
        $posts = $this->postRepo->getAll();
        return view('post.list', ['posts' => $posts]);
    }

    public function getAdvice($slug)
    {
        $post = $this->postRepo->findBySlug($slug);
        return view('post.single', ['post' => $post]);
    }
}
