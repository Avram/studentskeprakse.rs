<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Support\Str;
use StudentskePrakse\Decorator\SelectboxDecorator;
use StudentskePrakse\Http\Requests;
use StudentskePrakse\Http\Requests\Admin\CreateInternshipRequest;
use StudentskePrakse\Http\Requests\Admin\UpdateInternshipRequest;
use StudentskePrakse\Http\Requests\Internship\DeleteInternshipRequest;
use StudentskePrakse\Model\Internship;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\CategoryRepositoryInterface;
use StudentskePrakse\Repository\CityRepositoryInterface;
use StudentskePrakse\Repository\InternshipRepositoryInterface;
use StudentskePrakse\Repository\MentorRepositoryInterface;
use StudentskePrakse\Repository\UserRepositoryInterface;

class AdminInternshipController extends Controller
{
    /**
     * @var InternshipRepositoryInterface
     */
    private $internshipRepo;

    /**
     * @var CityRepositoryInterface
     */
    private $cityRepo;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var MentorRepositoryInterface
     */
    private $mentorRepo;

    /**
     * AdminInternshipController constructor.
     *
     * @param InternshipRepositoryInterface $internshipRepo
     * @param MentorRepositoryInterface     $mentorRepo
     * @param UserRepositoryInterface       $userRepo
     * @param CityRepositoryInterface       $cityRepo
     * @param CategoryRepositoryInterface   $categoryRepo
     */
    public function __construct(InternshipRepositoryInterface $internshipRepo, MentorRepositoryInterface $mentorRepo, UserRepositoryInterface $userRepo, CityRepositoryInterface $cityRepo, CategoryRepositoryInterface $categoryRepo)
    {
        $this->internshipRepo = $internshipRepo;
        $this->cityRepo       = $cityRepo;
        $this->categoryRepo   = $categoryRepo;
        $this->userRepo       = $userRepo;
        $this->mentorRepo     = $mentorRepo;
    }

    /**
     * @return Response
     */
    public function getInternships()
    {
        $internships = $this->internshipRepo->getAll();
        return view('internship.admin.internshipList', ['internships' => $internships]);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function getInternship($id)
    {
        $internship = $this->internshipRepo->findOrFail($id);
        return view('internship.admin.internshipSingle', ['internship' => $internship]);
    }

    /**
     * @return Response
     */
    public function createInternship()
    {
        $internship = new Internship();
        $categories = $this->categoryRepo->getAll();
        $cities     = $this->cityRepo->getAll();
        $users      = $this->userRepo->getAll();
        $mentors    = [];

        $decoratedUsers = with(new SelectboxDecorator($users))->prepare('name', 'id');

        return view('internship.admin.internshipForm', [
            'internship' => $internship,
            'mentors' => $mentors,
            'users' => $decoratedUsers,
            'categories' => $categories,
            'cities' => $cities
        ]);
    }

    /**
     * @param CreateInternshipRequest $request
     *
     * @return Response
     */
    public function postCreateInternship(CreateInternshipRequest $request)
    {
        $input = $request->only([
            'user_id',
            'city_id',
            'category_id',
            'name',
            'content',
            'required_skills',
            'duration',
            'status',
            'salary',
            'food',
            'work_hours',
        ]);

        $user = $this->userRepo->findOrFail($input['user_id']);

        $internship = new Internship();

        $internship->setName($input['name']);
        $internship->setContent($input['content']);
        $internship->setRequiredSkills($input['required_skills']);
        $internship->setDuration($input['duration']);
        $internship->setSalary($input['salary']);
        $internship->setStatus($input['status']);
        $internship->setMeals($input['food']);
        $internship->setWorkHours($input['work_hours']);

        $internship->setSlug(Str::slug($internship->getName()));

        $city     = $this->cityRepo->findOrFail($input['city_id']);
        $category = $this->categoryRepo->findOrFail($input['category_id']);

        $this->internshipRepo->saveInternshipToCity($internship, $city);
        $this->internshipRepo->saveInternshipToCategory($internship, $category);
        $this->internshipRepo->saveInternshipToUser($internship, $user);


        return redirect()->route('admin.internship.internshipSingle', [$internship->getId()]);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function updateInternship($id)
    {
        $internship = $this->internshipRepo->findOrFail($id);

        /**
         * @var User $user
         */
        $user             = $this->userRepo->findOrFail($internship->getUserId());
        $categories       = $this->categoryRepo->getAll();
        $cities           = $this->cityRepo->getAll();
        $users            = $this->userRepo->getAll();
        $mentors          = $this->mentorRepo->getMentorsForUser($user);
        $selected_mentors = $internship->mentors->lists('id')->toArray();

        $decoratedUsers = with(new SelectboxDecorator($users))->prepare('name', 'id');

        return view('internship.admin.internshipForm', [
            'internship'       => $internship,
            'mentors'          => $mentors,
            'selected_mentors' => $selected_mentors,
            'users'            => $decoratedUsers,
            'categories'       => $categories,
            'cities'           => $cities
        ]);
    }

    /**
     * @param UpdateInternshipRequest $request
     * @param                         $id
     *
     * @return Response
     */
    public function postUpdateInternship(UpdateInternshipRequest $request, $id)
    {
        $input = $request->only([
            'user_id',
            'city_id',
            'category_id',
            'name',
            'content',
            'required_skills',
            'duration',
            'salary',
            'status',
            'food',
            'work_hours',
            'mentors',
        ]);

        $user = $this->userRepo->findOrFail($input['user_id']);

        $internship = $this->internshipRepo->find($id);

        $internship->setName($input['name']);
        $internship->setContent($input['content']);
        $internship->setRequiredSkills($input['required_skills']);
        $internship->setDuration($input['duration']);
        $internship->setSalary($input['salary']);
        $internship->setStatus($input['status']);
        $internship->setMeals($input['food']);
        $internship->setWorkHours($input['work_hours']);

        if (!empty($input['mentors'])) {
            $internship->mentors()->detach($internship->mentors->lists('id')->toArray());
            foreach ($input['mentors'] as $mentor_id) {
                $mentor = $this->mentorRepo->findOrFail($mentor_id);
                $this->mentorRepo->saveMentorToInternship($mentor, $internship);
            }
        }

//        $internship->setSlug(Str::slug($internship->getName()));

        $city     = $this->cityRepo->findOrFail($input['city_id']);
        $category = $this->categoryRepo->findOrFail($input['category_id']);

        $this->internshipRepo->saveInternshipToCity($internship, $city);
        $this->internshipRepo->saveInternshipToCategory($internship, $category);

        $this->internshipRepo->saveInternshipToUser($internship, $user);

        return redirect()->route('admin.internship.internshipSingle', [$internship->getId()]);
    }

    /**
     * @param DeleteInternshipRequest $request
     * @param                         $id
     *
     * @return Response
     */
    public function deleteInternship(DeleteInternshipRequest $request, $id)
    {
        $internship = $this->internshipRepo->findOrFail($id);
        $this->internshipRepo->delete($internship);
        return redirect()->route('admin.internship.internshipList');
    }
}
