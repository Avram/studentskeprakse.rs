<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Exceptions\InvalidAttachmentException;
use StudentskePrakse\Http\Requests;
use StudentskePrakse\Http\Requests\Attachment\AjaxRemoveAttachmentRequest;
use StudentskePrakse\Http\Requests\Attachment\AjaxUpdateImagesOrderRequest;
use StudentskePrakse\Http\Requests\Attachment\AjaxUploadImageRequest;
use StudentskePrakse\Model\Attachment;
use StudentskePrakse\Model\User;
use StudentskePrakse\Presenter\Exception\PresenterException;
use StudentskePrakse\Repository\AttachmentRepositoryInterface;
use StudentskePrakse\Repository\UserRepositoryInterface;
use StudentskePrakse\Service\FileUploader;
use StudentskePrakse\Service\ImageResizer;
use Symfony\Component\HttpFoundation\File\File;

class FileUploadController extends Controller
{

    /**
     * @var Guard
     */
    private $guard;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;
    /**
     * @var AttachmentRepositoryInterface
     */
    private $attachmentRepo;

    public function __construct(Guard $guard, UserRepositoryInterface $userRepo, AttachmentRepositoryInterface $attachmentRepo)
    {

        $this->guard          = $guard;
        $this->userRepo       = $userRepo;
        $this->attachmentRepo = $attachmentRepo;
    }

    /**
     * @param FileUploader           $uploader
     * @param ImageResizer           $resizer
     * @param AjaxUploadImageRequest $request
     *
     * @return array
     * @throws \Exception
     * @throws PresenterException
     */
    public function uploadOfficeImage(FileUploader $uploader, ImageResizer $resizer, AjaxUploadImageRequest $request)
    {
        $file = $request->file('file');

        /** @var User $user */
        $user         = $this->guard->user();
        $officeImages = $this->attachmentRepo->getOfficeImagesForUser($user);

        if ($officeImages->count() >= 6) {
            return [
                'status'  => 'error',
                'message' => 'Nije moguće postaviti više od šest fotografija.'
            ];
        }


        $uploader->setUploadedFile($file);

        try {
            /** @var File $newFile */
            $newFile = $uploader->upload();
        } catch (InvalidAttachmentException $e) {
            //invalid Attachment
            return [
                'status'  => 'error',
                'message' => 'Ovaj tip datoteke nije dozvoljen!',
            ];
        }
        $newFileName = $newFile->getBasename();

        $attachment = Attachment::make(Attachment::TYPE_OFFICE, '', $newFileName, $user->getId());
        $this->attachmentRepo->save($attachment);

        $thumbFileName = 'thumb-'.$newFileName;

        $resizer->loadImageFromFile($newFile->getPathname());
        $resizer->setOutputFileName($uploader->getUploadDirPath().DIRECTORY_SEPARATOR.$thumbFileName);
        $resizer->imageResizeCrop(300, 160);

        return [
            'status' => 'OK',
            'thumb'  => url($attachment->present()->attachmentThumbnail()),
            'image'  => url($attachment->present()->attachment()),
            'id'     => $attachment->getId(),
        ];
    }

    /**
     * @param AjaxRemoveAttachmentRequest $request
     *
     * @return array
     * @throws PresenterException
     */
    public function removeOfficeImage(AjaxRemoveAttachmentRequest $request)
    {
        $id = $request->input('id');

        $image = $this->attachmentRepo->findOrFail($id);

        if ($image === null) {
            return [
                'status'  => 'error',
                'message' => 'Image not found'
            ];
        }

        $thumbnail = public_path($image->present()->attachmentThumbnail());
        if (is_file($thumbnail)) {
            unlink($thumbnail);
        }

        $attachment = public_path($image->present()->attachment());
        if (is_file($attachment)) {
            unlink($attachment);
        }

        $this->attachmentRepo->delete($image);

        return [
            'status' => 'OK',
        ];
    }

    /**
     * @param AjaxUpdateImagesOrderRequest $request
     *
     * @return array
     */
    public function updateOfficeImagesOrder(AjaxUpdateImagesOrderRequest $request)
    {
        $order = json_decode($request->input('order'));

        $count = count($order[0]);

        for ($i = 0; $i < $count; $i++) {
            $image      = $order[0][$i];
            $attachment = $this->attachmentRepo->findOrFail($image->id);
            $attachment->setOrder($i);
            $this->attachmentRepo->save($attachment);
        }

        return [
            'status' => 'OK'
        ];

    }


    /**
     * @param FileUploader                  $uploader
     * @param ImageResizer                  $resizer
     * @param AjaxUploadImageRequest        $request
     * @param AttachmentRepositoryInterface $attachmentRepo
     *
     * @return array
     * @throws \Exception
     */
    public function uploadLogo(FileUploader $uploader, ImageResizer $resizer, AjaxUploadImageRequest $request, AttachmentRepositoryInterface $attachmentRepo)
    {
        $file = $request->file('file');

        /** @var User $user */
        $user = $this->guard->user();

        $uploader->setUploadedFile($file);

        try {
            /** @var File $newFile */
            $newFile = $uploader->upload();
        } catch (InvalidAttachmentException $e) {
            //invalid Attachment
            return [
                'status'  => 'error',
                'message' => 'Ovaj tip datoteke nije dozvoljen!',
            ];
        }
        $newFileName = $newFile->getBasename();

        $resizer->loadImageFromFile($newFile->getPathname());
        $resizer->imageResizeCrop(150, 150);

        $oldLogo = $user->getLogo();
        if (!empty($oldLogo) && is_file($uploader->getUploadDirPath().DIRECTORY_SEPARATOR.$oldLogo)) {
            unlink($uploader->getUploadDirPath().DIRECTORY_SEPARATOR.$oldLogo);
        }

        $user->setLogo($newFileName);
        $this->userRepo->save($user);

        return [
            'status' => 'OK',
            'image'  => url($user->present()->logo()),
        ];
    }

    /**
     * @param FileUploader                  $uploader
     * @param ImageResizer                  $resizer
     * @param AjaxUploadImageRequest        $request
     * @param AttachmentRepositoryInterface $attachmentRepo
     *
     * @return array
     * @throws \Exception
     * @throws PresenterException
     */
    public function uploadCover(FileUploader $uploader, ImageResizer $resizer, AjaxUploadImageRequest $request, AttachmentRepositoryInterface $attachmentRepo)
    {
        $file = $request->file('file');

        /** @var User $user */
        $user = $this->guard->user();

        $uploader->setUploadedFile($file);

        try {
            /** @var File $newFile */
            $newFile = $uploader->upload();
        } catch (InvalidAttachmentException $e) {
            //invalid Attachment
            return [
                'status'  => 'error',
                'message' => 'Ovaj tip datoteke nije dozvoljen!',
            ];
        }
        $newFileName = $newFile->getBasename();

        $resizer->loadImageFromFile($newFile->getPathname());
        $resizer->imageResizeCrop(1400, 288);

        $oldCover = $user->getCover();
        if (!empty($oldCover) && is_file($uploader->getUploadDirPath().DIRECTORY_SEPARATOR.$oldCover)) {
            unlink($uploader->getUploadDirPath().DIRECTORY_SEPARATOR.$oldCover);
        }

        $user->setCover($newFileName);
        $this->userRepo->save($user);

        return [
            'status' => 'OK',
            'image'  => url($user->present()->cover()),
        ];
    }
}
