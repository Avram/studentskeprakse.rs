<?php

namespace StudentskePrakse\Http\Controllers;

use StudentskePrakse\Http\Requests;
use StudentskePrakse\Http\Requests\Admin\CreateCategoryRequest;
use StudentskePrakse\Http\Requests\Admin\UpdateCategoryRequest;
use StudentskePrakse\Model\Category;
use StudentskePrakse\Repository\CategoryRepositoryInterface;

class AdminCategoryController extends Controller
{
    private $categoryRepo;

    /**
     * AdminCategoryController constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepo
     */
    public function __construct(CategoryRepositoryInterface $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCategories()
    {
        $categories = $this->categoryRepo->getAll();
        return view('category.admin.categoryList', ['categories' => $categories]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createCategory()
    {
        $category = new Category();
        return view('category.admin.categoryForm', ['category' => $category]);
    }

    public function postCreateCategory(CreateCategoryRequest $request)
    {
        $input = $request->only([
            'name',
            'order',
            'parent_id'
        ]);

        /**
         * @var Category $newCategory
         */
        $newCategory = Category::make(
            $input['name'],
            $input['order'],
            $input['parent_id']
        );

        $this->categoryRepo->save($newCategory);

        return redirect()->route('admin.category.categoryList', [$newCategory->id]);
    }

    public function updateCategory($id)
    {
        $category = $this->categoryRepo->findOrFail($id);
        return view('category.admin.categoryForm', ['category' => $category]);
    }

    public function postUpdateCategory(UpdateCategoryRequest $request, $id)
    {
        $category = $this->categoryRepo->findOrFail($id);

        $input = $request->only([
            'name',
            'order',
            'parent_id'
        ]);

        $category->setName($input['name']);
        $category->setOrder($input['order']);

        $this->categoryRepo->save($category);

        $parent_id = $input['parent_id'];
        if ($parent_id > 0) {
            $parent = $this->categoryRepo->findOrFail($parent_id);
            $this->categoryRepo->saveCategoryToParent($category, $parent);
        }

        return redirect()->route('admin.category.categoryList', [$category->id]);

    }

    public function deleteCategory($id)
    {
        $category = $this->categoryRepo->findOrFail($id);
        $this->categoryRepo->delete($category);
        return redirect()->route('admin.category.categoryList');
    }

}
