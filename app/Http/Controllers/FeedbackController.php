<?php namespace StudentskePrakse\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Feedback\DeleteFeedbackRequest;
use StudentskePrakse\Http\Requests\Feedback\UpdateFeedbackRequest;
use StudentskePrakse\Http\Requests\User\EditProfileRequest;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\FeedbackRepositoryInterface;
use StudentskePrakse\Repository\InternshipRepositoryInterface;

class FeedbackController extends Controller
{

    /**
     * @var Guard
     */
    private $guard;
    /**
     * @var FeedbackRepositoryInterface
     */
    private $feedbackRepo;
    /**
     * @var InternshipRepositoryInterface
     */
    private $internshipRepo;

    /**
     * FeedbackController constructor.
     *
     * @param Guard                         $guard
     * @param FeedbackRepositoryInterface   $feedbackRepo
     * @param InternshipRepositoryInterface $internshipRepo
     */
    public function __construct(Guard $guard, FeedbackRepositoryInterface $feedbackRepo, InternshipRepositoryInterface $internshipRepo)
    {
        $this->guard        = $guard;
        $this->feedbackRepo = $feedbackRepo;
        $this->internshipRepo = $internshipRepo;
    }

    public function updateFeedbacks(EditProfileRequest $request, $id)
    {
        $feedback = $this->feedbackRepo->findOrFail($id);

        return view('feedback.feedbackForm', [
            'feedback' => $feedback
        ]);
    }

    public function postUpdateFeedbacks(UpdateFeedbackRequest $request, $id)
    {
        $input = $request->only([
            'reply'
        ]);

        $feedback = $this->feedbackRepo->findOrFail($id);

        $feedback->setReply($input['reply']);

        $this->feedbackRepo->save($feedback);

        return redirect()->route('company.feedback.management');
    }

    public function deleteFeedbacks(DeleteFeedbackRequest $request, $id)
    {
        $feedback = $this->feedbackRepo->findOrFail($id);
        $this->feedbackRepo->delete($feedback);

        return redirect()->route('company.feedback.management');
    }

}