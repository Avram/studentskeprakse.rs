<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Response;
use StudentskePrakse\Exceptions\EmptyAttachmentException;
use StudentskePrakse\Exceptions\InvalidAttachmentException;
use StudentskePrakse\Http\Requests;
use StudentskePrakse\Http\Requests\Attachment\AjaxRemoveAttachmentRequest;
use StudentskePrakse\Http\Requests\Attachment\PostAttachmentRequest;
use StudentskePrakse\Model\Attachment;
use StudentskePrakse\Model\User;
use StudentskePrakse\Presenter\Exception\PresenterException;
use StudentskePrakse\Repository\AttachmentRepositoryInterface;
use StudentskePrakse\Repository\UserRepositoryInterface;
use StudentskePrakse\Service\Attachment\AttachmentHandlerManager;
use StudentskePrakse\Service\URLScraper\URLScraperManager;

class AttachmentController extends Controller
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var Guard
     */
    private $guard;
    /**
     * @var AttachmentRepositoryInterface
     */
    private $attachmentRepo;

    /**
     * AttachmentController constructor.
     *
     * @param UserRepositoryInterface       $userRepo
     * @param Guard                         $guard
     * @param AttachmentRepositoryInterface $attachmentRepo
     */
    public function __construct(UserRepositoryInterface $userRepo, Guard $guard, AttachmentRepositoryInterface $attachmentRepo)
    {
        $this->userRepo       = $userRepo;
        $this->guard          = $guard;
        $this->attachmentRepo = $attachmentRepo;
    }

    /**
     * @param PostAttachmentRequest    $request
     * @param AttachmentHandlerManager $manager
     *
     * @return Response
     * @throws EmptyAttachmentException
     */
    public function postAttachment(PostAttachmentRequest $request, AttachmentHandlerManager $manager)
    {
        /** @var User $user */
        $user = $this->guard->user();

        $description = trim($request->input('description'));
        $hasFile     = $request->hasFile('attachment');
        $isLink      = (filter_var($description, FILTER_VALIDATE_URL) !== false);

        if (empty($description) && !$hasFile) {
            throw new EmptyAttachmentException("Attachment can not be empty.");
        }

        if ($hasFile) {
            //file uploaded
            $file       = $request->file('attachment');
            $attachment = $manager->handleFile($file, $description);
        } elseif ($isLink) {
            //link
            $attachment = $manager->handleURL($description);
        } else {
            //regular text
            $attachment = new Attachment();
            $attachment->setType(Attachment::TYPE_TEXT);
            $attachment->setValue($description);
        }

        $this->attachmentRepo->save($attachment);
        $this->attachmentRepo->saveAttachmentForUser($attachment, $user);

        return redirect()->route('company.internship', [$user->getSlug(), $request->internship, '#attachments']);
    }

    /**
     * @param AjaxRemoveAttachmentRequest $request
     *
     * @return array
     * @throws PresenterException
     */
    public function removeAttachment(AjaxRemoveAttachmentRequest $request)
    {
        $id = $request->input('id');

        $image = $this->attachmentRepo->findOrFail($id);

        if ($image === null) {
            return [
                'status'  => 'error',
                'message' => 'Image not found'
            ];
        }

        $thumb = public_path($image->present()->attachmentThumbnail());
        if (is_file($thumb)) {
            unlink($thumb);
        }

        $attachedFile = public_path($image->present()->attachment());
        if (is_file($attachedFile)) {
            unlink($attachedFile);
        }

        $this->attachmentRepo->delete($image);

        return [
            'status' => 'OK',
        ];
    }
}