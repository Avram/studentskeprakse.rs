<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use StudentskePrakse\Decorator\SelectboxDecorator;
use StudentskePrakse\Exceptions\EntityNotFoundException;
use StudentskePrakse\Http\Requests\Internship\AjaxToggleInternshipRequest;
use StudentskePrakse\Http\Requests\Internship\DeleteInternshipRequest;
use StudentskePrakse\Http\Requests\Internship\DuplicateInternshipRequest;
use StudentskePrakse\Http\Requests\Internship\PostCreateInternshipRequest;
use StudentskePrakse\Http\Requests\Internship\RestoreInternshipRequest;
use StudentskePrakse\Http\Requests\Internship\UpdateInternshipRequest;
use StudentskePrakse\Model\Internship;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\AttachmentRepositoryInterface;
use StudentskePrakse\Repository\CategoryRepositoryInterface;
use StudentskePrakse\Repository\CityRepositoryInterface;
use StudentskePrakse\Repository\FAQRepositoryInterface;
use StudentskePrakse\Repository\FeedbackRepositoryInterface;
use StudentskePrakse\Repository\InternshipRepositoryInterface;
use StudentskePrakse\Repository\MentorRepositoryInterface;
use StudentskePrakse\Repository\UserRepositoryInterface;

class InternshipController extends Controller
{
    /**
     * @var InternshipRepositoryInterface
     */
    private $internshipRepo;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;
    /**
     * @var CityRepositoryInterface
     */
    private $cityRepo;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;
    /**
     * @var MentorRepositoryInterface
     */
    private $mentorRepo;
    /**
     * @var ResponseFactory
     */
    private $response;
    /**
     * @var FeedbackRepositoryInterface
     */
    private $feedbackRepo;
    /**
     * @var AttachmentRepositoryInterface
     */
    private $attachmentRepo;
    /**
     * @var FAQRepositoryInterface
     */
    private $faqRepo;

    /**
     * InternshipController constructor.
     *
     * @param InternshipRepositoryInterface $internshipRepo
     * @param MentorRepositoryInterface     $mentorRepo
     * @param UserRepositoryInterface       $userRepo
     * @param CityRepositoryInterface       $cityRepo
     * @param CategoryRepositoryInterface   $categoryRepo
     * @param ResponseFactory               $response
     * @param FeedbackRepositoryInterface   $feedbackRepo
     * @param AttachmentRepositoryInterface $attachmentRepo
     * @param FAQRepositoryInterface        $faqRepo
     */
    public function __construct(InternshipRepositoryInterface $internshipRepo, MentorRepositoryInterface $mentorRepo,
                                UserRepositoryInterface $userRepo, CityRepositoryInterface $cityRepo,
                                CategoryRepositoryInterface $categoryRepo, ResponseFactory $response,
                                FeedbackRepositoryInterface $feedbackRepo, AttachmentRepositoryInterface $attachmentRepo,
                                FAQRepositoryInterface $faqRepo)
    {
        $this->internshipRepo = $internshipRepo;
        $this->userRepo       = $userRepo;
        $this->cityRepo       = $cityRepo;
        $this->categoryRepo   = $categoryRepo;
        $this->mentorRepo     = $mentorRepo;
        $this->response       = $response;
        $this->feedbackRepo   = $feedbackRepo;
        $this->attachmentRepo = $attachmentRepo;
        $this->faqRepo        = $faqRepo;
    }

    /**
     * @param Guard $guard
     * @param       $username
     * @param       $slug
     *
     * @return Response
     * @throws EntityNotFoundException
     */
    public function displayInternship(Guard $guard, $username, $slug)
    {
        $user        = $this->userRepo->findBySlug($username);
        $internship  = $this->internshipRepo->findBySlugOrFail($slug, $user);
        $internships = $this->internshipRepo->getInternshipsForUser($user);

        /** @var Collection $excludeIds */
        $excludeIds = $internships->pluck('id')->toArray();

        $search = [
            'conditions' => ['category' => 'OR', 'city' => 'OR'],
            'similar'    => $internship,
            'status'     => Internship::STATUS_PUBLISHED,
            'exclude'    => $excludeIds,
        ];

        $similar = $this->internshipRepo->searchInternships($search, 5);

        /** @var User $guardUser */
        $guardUser = $guard->user();
        $sameUser  = ($guardUser && ($guardUser->getId() == $user->getId()));

        if (($internship->getStatus() !== Internship::STATUS_PUBLISHED || $internship->isDummy()) && !$sameUser) {
            throw new EntityNotFoundException("Internship not found");
        }

        if (!$sameUser && !$internship->isDummy()) {
            $internship->incrementViewCount();
            $this->internshipRepo->save($internship, ['timestamps' => false]);
        }

        $feedbacks   = $this->feedbackRepo->getFeedbacksForUser($user, true);
        $images      = $this->attachmentRepo->getOfficeImagesForUser($user);
        $attachments = $this->attachmentRepo->getAttachmentsForUser($user);
        $faqs        = $this->faqRepo->getFAQsForUser($user, true);


        return view('user.profile.profile', [
            'user'        => $user,
            'internship'  => $internship,
            'feedback'    => $feedbacks,
            'images'      => $images,
            'attachments' => $attachments,
            'internships' => $internships,
            'faqs'        => $faqs,
            'similar'     => $similar,
        ]);
    }

    /**
     * @param Guard $guard
     *
     * @return Response
     */
    public function createInternship(Guard $guard)
    {
        /**
         * @var User $user
         */
        $user = $guard->user();

        $internship        = new Internship();
        $categories        = $this->categoryRepo->getAll();
        $cities            = $this->cityRepo->getAll();
        $mentors           = $this->mentorRepo->getMentorsForUser($user);
        $selected_mentors  = $this->mentorRepo->getMentorsForInternship($internship)->pluck('id')->toArray();
        $selected_category = null;
        $selected_city     = null;

        $decoratedCategories = with(new SelectboxDecorator($categories))->prepare('name', 'id');
        $decoratedCities     = with(new SelectboxDecorator($cities))->prepare('name', 'id');
        $decoratedMentors    = with(new SelectboxDecorator($mentors))->prepare('name', 'id');

        return view('internship.internshipForm', [
            'internship'        => $internship,
            'mentors'           => $decoratedMentors,
            'categories'        => $decoratedCategories,
            'cities'            => $decoratedCities,
            'selected_mentors'  => $selected_mentors,
            'selected_category' => $selected_category,
            'selected_city'     => $selected_city
        ]);
    }

    /**
     * @param PostCreateInternshipRequest $request
     * @param Guard                       $guard
     *
     * @return Response
     */
    public function postCreateInternship(PostCreateInternshipRequest $request, Guard $guard)
    {
        /**
         * @var User $user
         */
        $user = $guard->user();

        $input = $request->only([
            'city_id',
            'category_id',
            'name',
            'content',
            'required_skills',
            'duration',
            'work_dwm',
            'salary',
            'food',
            'work_hours',
            'mentors',
        ]);

        $internship = new Internship();

        $internship->setName($input['name']);
        $internship->setContent(strip_tags($input['content']));
        $internship->setRequiredSkills(strip_tags($input['required_skills']));
        $internship->setDuration($input['duration']);
        $internship->setDurationType($_POST['work_dwm']);
        $internship->setSalary($input['salary']);
        $internship->setMeals($input['food']);
        $internship->setWorkHours($input['work_hours']);
        $internship->setStatus(Internship::STATUS_PUBLISHED);


        if (Str::slug($internship->getName()) === 'new-internship') {
            $request->flash();
            session()->flash('error', 'Uneli ste naslov prakse koji nije dozvoljen!');
            return redirect()->back();
        }

        $internship->setSlug(Str::slug($internship->getName()));

        $city     = $this->cityRepo->findOrFail($_POST['city_id']);
        $category = $this->categoryRepo->findOrFail($_POST['category_id']);

        $this->internshipRepo->saveInternshipToCity($internship, $city);
        $this->internshipRepo->saveInternshipToCategory($internship, $category);

        $this->internshipRepo->saveInternshipToUser($internship, $user);

        $internship->mentors()->detach($internship->mentors->lists('id')->toArray());

        if (!empty($input['mentors'])) {
            foreach ($input['mentors'] as $id) {
                $mentor = $this->mentorRepo->find($id);
                $this->mentorRepo->saveMentorToInternship($mentor, $internship);
            }
        }


        return redirect()->route('company.internship', [$user->getSlug(), $internship->getSlug()]);
    }

    /**
     * @param Guard $guard
     * @param       $username
     * @param       $slug
     *
     * @return Response
     * @throws EntityNotFoundException
     */
    public function updateInternship(Guard $guard, $username, $slug)
    {
        /**
         * @var User $guardUser
         */
        $guardUser = $guard->user();
        $user      = $this->userRepo->findBySlugOrFail($username);
        $sameUser  = ($guardUser && ($guardUser->getId() == $user->getId()));

        if ($slug === 'new-internship') {
            if (!$sameUser) {
                throw new EntityNotFoundException("Internship not found");
            } else {
                return redirect()->route('company.internship', [$username, $slug]);
            }
        }

        $internship        = $this->internshipRepo->findBySlug($slug, $guardUser);
        $cities            = $this->cityRepo->getAll();
        $categories        = $this->categoryRepo->getAll();
        $mentors           = $this->mentorRepo->getMentorsForUser($guardUser);
        $selected_mentors  = $this->mentorRepo->getMentorsForInternship($internship)->pluck('id')->toArray();
        $selected_category = $internship->getCategoryId();
        $selected_city     = $internship->getCityId();

        $decoratedCategories = with(new SelectboxDecorator($categories))->prepare('name', 'id');
        $decoratedCities     = with(new SelectboxDecorator($cities))->prepare('name', 'id');
        $decoratedMentors    = with(new SelectboxDecorator($mentors))->prepare('name', 'id');

        return view('user.profile.profileEdit', [
            'user'              => $guardUser,
            'internship'        => $internship,
            'mentors'           => $decoratedMentors,
            'categories'        => $decoratedCategories,
            'cities'            => $decoratedCities,
            'selected_mentors'  => $selected_mentors,
            'selected_category' => $selected_category,
            'selected_city'     => $selected_city
        ]);
    }


    /**
     * @param UpdateInternshipRequest $request
     * @param Guard                   $guard
     * @param                         $slug
     *
     * @return Response
     */
    public function postUpdateInternship(UpdateInternshipRequest $request, Guard $guard, $username, $slug)
    {
        /** @var User $user */
        $user = $guard->user();

        $input = $request->only([
            'city_id',
            'category_id',
            'name',
            'content',
            'required_skills',
            'duration',
            'work_dwm',
            'salary',
            'food',
            'work_hours',
            'mentors'
        ]);

        $internship = $this->internshipRepo->findBySlug($slug, $user);

        $internship->setName($input['name']);
        $internship->setContent(strip_tags($input['content']));
        $internship->setRequiredSkills(strip_tags($input['required_skills']));
        $internship->setDuration($input['duration']);
        $internship->setDurationType($input['work_dwm']);
        $internship->setSalary($input['salary']);
        $internship->setMeals($input['food']);
        $internship->setWorkHours($input['work_hours']);

        if (Str::slug($internship->getName()) === 'new-internship') {
            $request->flash();
            session()->flash('error', 'Uneli ste naslov prakse koji nije dozvoljen!');
            return redirect()->back();
        }

        $slug = $internship->getSlug();
        if (empty($slug)) {
            $internship->setSlug(Str::slug($internship->getName()));
        }

        $internship->mentors()->detach($internship->mentors->lists('id')->toArray());
        if (!empty($input['mentors'])) {
            foreach ($input['mentors'] as $id) {
                $mentor = $this->mentorRepo->find($id);
                $this->mentorRepo->saveMentorToInternship($mentor, $internship);
            }
        }

        $city     = $this->cityRepo->findOrFail($input['city_id']);
        $category = $this->categoryRepo->findOrFail($input['category_id']);

        $this->internshipRepo->saveInternshipToCity($internship, $city);
        $this->internshipRepo->saveInternshipToCategory($internship, $category);
        $this->internshipRepo->saveInternshipToUser($internship, $user);

        session()->flash('message', 'Izmene su sačuvane!');
        return redirect()->route('company.internship.edit', [$user->getSlug(), $internship->getSlug()]);
    }

    /**
     * @param DeleteInternshipRequest $request
     * @param Guard                   $guard
     * @param                         $slug
     *
     * @return Response
     */
    public function deleteInternship(DeleteInternshipRequest $request, Guard $guard, $slug)
    {
        /**
         * @var User $user
         */
        $user       = $guard->user();
        $internship = $this->internshipRepo->findBySlug($slug, $user);
        $this->internshipRepo->delete($internship);
        return redirect()->route('company.internships.management');
    }

    /**
     * @param DeleteInternshipRequest $request
     * @param Guard                   $guard
     * @param string                  $slug
     *
     * @return Response
     */
    public function softDeleteInternship(DeleteInternshipRequest $request, Guard $guard, $slug)
    {
        /**
         * @var User $user
         */
        $user       = $guard->user();
        $internship = $this->internshipRepo->findBySlug($slug, $user);
        $this->internshipRepo->softDelete($internship);
        return redirect()->route('company.internships.management');
    }

    /**
     * @param RestoreInternshipRequest $request
     * @param Guard                    $guard
     * @param string                   $slug
     *
     * @return Response
     */
    public function softRestoreInternship(RestoreInternshipRequest $request, Guard $guard, $slug)
    {
        /**
         * @var User $user
         */
        $user       = $guard->user();
        $internship = $this->internshipRepo->findBySlug($slug, $user, true);
        $this->internshipRepo->softRestore($internship);
        return redirect()->route('company.internships.management');
    }

    /**
     * @param Guard $guard
     *
     * @return Response
     */
    public function trashed(Guard $guard)
    {
        /**
         * @var User $user
         */
        $user    = $guard->user();
        $trashed = $this->internshipRepo->searchInternships(['onlyTrashed' => true, 'user' => $user->id]);

        return view('internship.trashed', [
            'internships' => $trashed,
            'user'        => $user
        ]);
    }

    /**
     * @param AjaxToggleInternshipRequest   $request
     * @param InternshipRepositoryInterface $internshipRepo
     *
     * @return null|Internship
     */
    public function ajaxToggleStatus(AjaxToggleInternshipRequest $request, InternshipRepositoryInterface $internshipRepo)
    {
        $input = $request->only(['id']);
        $id    = $input['id'];

        $internship = $internshipRepo->findOrFail($id);
        $internship->toggleStatus();
        $internshipRepo->save($internship);

        return $internship;

    }

    /**
     * @param DuplicateInternshipRequest $request
     * @param Guard                      $guard
     * @param string                     $slug
     *
     * @return Response
     */
    public function duplicateInternship(DuplicateInternshipRequest $request, Guard $guard, $slug)
    {
        /** @var User $user */
        $user = $guard->user();
        /** @var Internship $internship */
        $internship = $this->internshipRepo->findBySlugOrFail($slug, $user);

        $internship->load('user', 'category', 'faqs', 'mentors', 'city');
        /** @var Internship $newInternship */
        $newInternship = $internship->replicate();
        $newInternship->push();
        $slug = $internship->getSlug();

        $i     = 2;
        $check = $slug.'-'.$i;
        while ($this->internshipRepo->findBySlug($check, $user) !== null) {
            $i++;
            $check = $slug.'-'.$i;
        }

        $newInternship->setSlug($check);
        $newInternship->setName($internship->getName().' - kopija');

        $this->internshipRepo->save($newInternship);

        return redirect()->route('internship.update', [$newInternship->getSlug()]);

    }
}
