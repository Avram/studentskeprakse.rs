<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\ResponseFactory;
use ReCaptcha\ReCaptcha;
use StudentskePrakse\Decorator\SelectboxDecorator;
use StudentskePrakse\Exceptions\InvalidAttachmentException;
use StudentskePrakse\Exceptions\ReCaptchaException;
use StudentskePrakse\Http\Requests;
use StudentskePrakse\Http\Requests\FAQ\PostFAQRequest;
use StudentskePrakse\Http\Requests\Feedback\PostFeedbackRequest;
use StudentskePrakse\Http\Requests\User\EditProfileRequest;
use StudentskePrakse\Http\Requests\User\PostFAQReplyRequest;
use StudentskePrakse\Http\Requests\User\PostFeedbackReplyRequest;
use StudentskePrakse\Http\Requests\User\PostUpdateFAQRequest;
use StudentskePrakse\Http\Requests\User\PostUpdateFeedbackRequest;
use StudentskePrakse\Http\Requests\User\UpdateAboutCompanyRequest;
use StudentskePrakse\Http\Requests\User\UpdateFAQRequest;
use StudentskePrakse\Http\Requests\User\UpdateFeedbackRequest;
use StudentskePrakse\Http\Requests\User\UpdateProfileRequest;
use StudentskePrakse\Http\Requests\User\UpdateSocialLinksRequest;
use StudentskePrakse\Model\FAQ;
use StudentskePrakse\Model\Feedback;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\AttachmentRepositoryInterface;
use StudentskePrakse\Repository\CategoryRepositoryInterface;
use StudentskePrakse\Repository\CityRepositoryInterface;
use StudentskePrakse\Repository\FAQRepositoryInterface;
use StudentskePrakse\Repository\FeedbackRepositoryInterface;
use StudentskePrakse\Repository\InternshipRepositoryInterface;
use StudentskePrakse\Repository\UserRepositoryInterface;
use StudentskePrakse\Service\DynamicLogo;
use StudentskePrakse\Service\FileUploader;
use StudentskePrakse\Service\ImageResizer;
use Symfony\Component\HttpFoundation\File\File;


class UserController extends Controller
{

    /**
     * @var ResponseFactory
     */
    private $response;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var Guard
     */
    private $guard;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * @var CityRepositoryInterface
     */
    private $cityRepo;
    /**
     * @param Guard                         $guard
     * @param FeedbackRepositoryInterface   $feedbackRepo
     * @param UserRepositoryInterface       $userRepo
     * @param CategoryRepositoryInterface   $categoryRepo
     * @param InternshipRepositoryInterface $internshipRepo
     * @param CityRepositoryInterface       $cityRepo
     * @param FAQRepositoryInterface        $faqRepo
     */
    public function __construct(Guard $guard, FeedbackRepositoryInterface $feedbackRepo, UserRepositoryInterface $userRepo,
                                CategoryRepositoryInterface $categoryRepo, InternshipRepositoryInterface $internshipRepo,
                                CityRepositoryInterface $cityRepo, FAQRepositoryInterface $faqRepo)
    {
        $this->userRepo       = $userRepo;
        $this->guard          = $guard;
        $this->categoryRepo   = $categoryRepo;
        $this->cityRepo       = $cityRepo;
        $this->internshipRepo = $internshipRepo;
        $this->feedbackRepo   = $feedbackRepo;
        $this->faqRepo        = $faqRepo;
    }
    /**
     * @var InternshipRepositoryInterface
     */
    private $internshipRepo;
    /**
     * @var FeedbackRepositoryInterface
     */
    private $feedbackRepo;

    /**
     * @var FAQRepositoryInterface
     */
    private $faqRepo;

    /**
     * Display user (company profile)
     *
     * @param InternshipRepositoryInterface $internshipRepo
     * @param                               $slug
     *
     * @return Response
     */
    public function displayProfile(InternshipRepositoryInterface $internshipRepo, $slug)
    {
        $user = $this->userRepo->findBySlugOrFail($slug);

        $internships = $internshipRepo->getInternshipsForUser($user);

        return view('user.internships', [
            'user'        => $user,
            'internships' => $internships,
        ]);
    }

    /**
     * @param EditProfileRequest $request
     *
     * @return Response
     */
    public function displayAboutSettings(EditProfileRequest $request, $username, $slug)
    {
        /** @var User $user */
        $user       = $this->guard->user();
        $internship = $this->internshipRepo->findBySlugOrFail($slug, $user);

        $categories          = $this->categoryRepo->getAll();
        $decoratedCategories = with(new SelectboxDecorator($categories))->prepare('name', 'id');
        $selectedCategory    = $user->getCategoryId();

        $cities          = $this->cityRepo->getAll();
        $decoratedCities = with(new SelectboxDecorator($cities))->prepare('name', 'id');
        $selectedCity    = $user->getCityId();

//        return view('user.settingsAbout', [
        return view('user.aboutEdit', [
            'user'              => $user,
            'internship'        => $internship,
            'categories'        => $decoratedCategories,
            'selected_category' => $selectedCategory,
            'cities'            => $decoratedCities,
            'selected_city'     => $selectedCity
        ]);
    }

    /**
     * @param UpdateAboutCompanyRequest $request
     * @param FileUploader              $uploader
     * @param ImageResizer              $resizer
     *
     * @return Response
     * @throws \Exception
     */
    public function postAboutSettings(UpdateAboutCompanyRequest $request, FileUploader $uploader, ImageResizer $resizer)
    {
        /** @var User $user */
        $user = $this->guard->user();

        $input = $request->only([
            'description',
            'employees',
            'founded',
            'category_id',
            'address',
            'city_id',
            'url',
            'facebook_url',
            'twitter_url',
            'linkedin_url',
        ]);

        if (!empty($input['founded'])) {
            $user->setFounded($input['founded']);
        }

        if (!empty($input['category_id'])) {
            $category = $this->categoryRepo->findOrFail($input['category_id']);
            $this->userRepo->saveUserToCategory($user, $category);
        }

        if (!empty($input['description'])) {
            $user->setDescription(strip_tags($input['description']));
        }

        if (!empty($input['employees'])) {
            $user->setEmployees($input['employees']);
        }

        if (!empty($input['city_id'])) {
            $city = $this->cityRepo->findOrFail($input['city_id']);
            $this->userRepo->saveUserToCity($user, $city);
        }

        if (!empty($input['address'])) {
            $user->setAddress($input['address']);
        }

        if (!empty($input['url'])) {
            $user->setUrl($input['url']);
        }

        if (!empty($input['facebook_url'])) {
            $user->setFacebookUrl($input['facebook_url']);
        }

        if (!empty($input['twitter_url'])) {
            $user->setTwitterUrl($input['twitter_url']);
        }

        if (!empty($input['linkedin_url'])) {
            $user->setLinkedinUrl($input['linkedin_url']);
        }

        //logo
        if ($request->hasFile('logo-edit')) {
            $file = $request->file('logo-edit');
            $uploader->setUploadedFile($file);

            try {
                /** @var File $newFile */
                $newFile = $uploader->upload();
            } catch (InvalidAttachmentException $e) {
                $request->flash();
                session()->flash('warning', 'Ovaj tip datoteke nije dozvoljen!');
                return redirect()->back();
            }
            $newFileName = $newFile->getBasename();

            $resizer->loadImageFromFile($newFile->getPathname());
            $resizer->imageResizeCrop(150, 150);

            $oldLogo = $user->getLogo();
            if (!empty($oldLogo) && is_file($uploader->getUploadDirPath().DIRECTORY_SEPARATOR.$oldLogo)) {
                unlink($uploader->getUploadDirPath().DIRECTORY_SEPARATOR.$oldLogo);
            }

            $user->setLogo($newFileName);
        }

        $this->userRepo->save($user);

        session()->flash('message', 'Podaci o kompaniji su ažurirani.');

        return redirect()->back();
    }

    /**
     * @param EditProfileRequest $request
     *
     * @return Response
     */
    public function displaySocialSettings(EditProfileRequest $request)
    {
        /** @var User $user */
        $user = $this->guard->user();

        return view('user.settingsSocial', [
            'user' => $user
        ]);
    }

    /**
     * @param UpdateSocialLinksRequest $request
     *
     * @return Response
     */
    public function postSocialSettings(UpdateSocialLinksRequest $request)
    {
        /** @var User $user */
        $user = $this->guard->user();

        $input = $request->only([
            'url',
            'facebookUrl',
            'twitterUrl',
            'linkedinUrl'
        ]);

        if (!empty($input['url'])) {
            $user->setUrl($input['url']);
        }

        if (!empty($input['twitterUrl'])) {
            $user->setTwitterUrl($input['twitterUrl']);
        }

        if (!empty($input['facebookUrl'])) {
            $user->setFacebookUrl($input['facebookUrl']);
        }

        if (!empty($input['linkedinUrl'])) {
            $user->setLinkedinUrl($input['linkedinUrl']);
        }

        session()->flash('message', 'Linkovi su ažurirani.');

        $this->userRepo->save($user);

        return redirect()->back();
    }

    /**
     * @param EditProfileRequest $request
     *
     * @return Response
     */
    public function displayProfileSettings(EditProfileRequest $request)
    {
        /** @var User $user */
        $user = $this->guard->user();

        return view('user.settingsProfile', [
            'user' => $user
        ]);
    }

    /**
     * @param UpdateProfileRequest $request
     * @param BcryptHasher         $hasher
     *
     * @return Response
     */
    public function postProfileSettings(UpdateProfileRequest $request, BcryptHasher $hasher)
    {
        /** @var User $user */
        $user = $this->guard->user();

        $input = $request->only([
            'name',
            'email',
            'old-pass',
            'new-pass'
        ]);

        $user->setName($input['name']);
        $user->setEmail($input['email']);

        $oldPass = $input['old-pass'];
        if (!empty($oldPass)) {
            if (!$hasher->check($oldPass, $user->getPassword())) {
                session()->flash('error', 'Pogrešna stara lozinka!');
                return redirect()->back();
            }

            $newPass = $input['new-pass'];
            $newPass = $hasher->make($newPass);
            $user->setPassword($newPass);
        }

        $this->userRepo->save($user);

        session()->flash('message', 'Profil je ažuriran.');

        return redirect()->back();
    }

    /**
     * @param FeedbackRepositoryInterface $feedbackRepo
     * @param string                      $slug
     *
     * @return Response
     */
//    public function displayFeedback(FeedbackRepositoryInterface $feedbackRepo, $slug)
//    {
//        $user = $this->userRepo->findBySlugOrFail($slug);
//
//        $feedbacks = $feedbackRepo->getFeedbacksForUser($user);
//
//        return view('user.feedback', [
//            'user'     => $user,
//            'feedback' => $feedbacks,
//        ]);
//    }

    /**
     * @param PostFeedbackRequest $request
     * @param                     $username
     * @param                     $slug
     *
     * @return Response
     * @throws ReCaptchaException
     *
     */
    public function postFeedback(PostFeedbackRequest $request, $username, $slug)
    {
        $user = $this->userRepo->findBySlugOrFail($username);

        $input = $request->only([
            'name',
            'feedback',
            'g-recaptcha-response'
        ]);

        $recaptcha = new ReCaptcha(env('RECAPTCHA_SECRET_KEY', ''));
        $resp      = $recaptcha->verify($input['g-recaptcha-response'], $request->getClientIp());
        if (!$resp->isSuccess()) {
            throw new ReCaptchaException($resp->getErrorCodes());
        }

        $feedback = new Feedback();
        $feedback->setName($input['name']);
        $feedback->setFeedback($input['feedback']);
        $this->feedbackRepo->save($feedback);
        $this->feedbackRepo->saveFeedbackToUser($feedback, $user);

        session()->flash('message', 'Vaš feedback je uspešno poslat.');

        return redirect()->route('company.internship', [$username, $slug, '#feedback']);
    }


    /**
     * @param PostFAQRequest $request
     * @param                $username
     * @param                $slug
     *
     * @return RedirectResponse
     * @throws ReCaptchaException
     */
    public function postFAQ(PostFAQRequest $request, $username, $slug)
    {
        $user = $this->userRepo->findBySlugOrFail($username);

        $input = $request->only([
            'question',
            'g-recaptcha-response'
        ]);

        $recaptcha = new ReCaptcha(env('RECAPTCHA_SECRET_KEY', ''));
        $resp      = $recaptcha->verify($input['g-recaptcha-response'], $request->getClientIp());
        if (!$resp->isSuccess()) {
            throw new ReCaptchaException($resp->getErrorCodes());
        }

        $faq = new FAQ();
        $faq->setQuestion($input['question']);
        $this->faqRepo->save($faq);
        $this->faqRepo->saveFAQtoUser($faq, $user);

        session()->flash('message', 'Vaše pitanje je uspešno poslato.');

        return redirect()->route('company.internship', [$username, $slug, '#faqs']);
    }

    /**
     * @param AttachmentRepositoryInterface $attachmentRepo
     * @param string                        $slug
     *
     * @return Response
     */
//    public function displayOffices(AttachmentRepositoryInterface $attachmentRepo, $slug)
//    {
//        $user = $this->userRepo->findBySlugOrFail($slug);
//
//        $images = $attachmentRepo->getOfficeImagesForUser($user);
//
//        return view('user.offices', [
//            'user'    => $user,
//            'offices' => $images,
//        ]);
//    }


    /**
     * @param AttachmentRepositoryInterface $attachmentRepo
     * @param string                        $slug
     *
     * @return Response
     */
//    public function displayAttachments(AttachmentRepositoryInterface $attachmentRepo, $slug)
//    {
//        $user = $this->userRepo->findBySlugOrFail($slug);
//
//        $attachments = $attachmentRepo->getAttachmentsForUser($user);
//
//        return view('user.attachments', [
//            'user'        => $user,
//            'attachments' => $attachments,
//        ]);
//    }

    /**
     * @param FAQRepositoryInterface $faqRepo
     * @param string                 $slug
     *
     * @return Response
     */
//    public function displayFAQs(FAQRepositoryInterface $faqRepo, $slug)
//    {
//        $user = $this->userRepo->findBySlugOrFail($slug);
//
//        $faqs = $faqRepo->getFAQsForUser($user);
//
//        return view('user.faqs', [
//            'user' => $user,
//            'faqs' => $faqs,
//        ]);
//    }

    /**
     * @param InternshipRepositoryInterface $internshipRepo
     * @param string                        $slug
     *
     * @return Response
     */
//    public function displayInternships(InternshipRepositoryInterface $internshipRepo, $slug)
//    {
//        $user = $this->userRepo->findBySlugOrFail($slug);
//
//        $internships = $internshipRepo->getInternshipsForUser($user);
//
//        return view('user.internships', [
//            'user'        => $user,
//            'internships' => $internships,
//        ]);
//    }

    /**
     * @param $slug
     *
     * @return Response
     */
//    public function displayAbout($slug)
//    {
//        $user = $this->userRepo->findBySlugOrFail($slug);
//
//        return view('user.about', [
//            'user'  => $user,
//            'about' => $user,
//        ]);
//    }

    /**
     * @param InternshipRepositoryInterface $internshipRepo
     *
     * @return Response
     */
    public function manageInternships(InternshipRepositoryInterface $internshipRepo)
    {
        /**
         * @var User $user
         */
        $user = $this->guard->user();

        $internships = $internshipRepo->getInternshipsForUser($user);

        return view('internship.management', [
            'user'        => $user,
            'internships' => $internships
        ]);
    }


    public function manageFAQ(UpdateFAQRequest $request, $username, $slug)
    {
        /** @var User $user */
        $user       = $this->guard->user();
        $faqs  = $this->faqRepo->getFAQsForUser($user, true);
        $internship = $this->internshipRepo->findBySlugOrFail($slug, $user);

        return view('user.faqsEdit', [
            'user'       => $user,
            'faqs'  => $faqs,
            'internship' => $internship
        ]);
    }

    public function postUpdateFAQ(PostUpdateFAQRequest $request, $username, $slug)
    {
        /** @var User $user */
        $user = $this->guard->user();

        $input = $request->only([
            'question',
            'answer'
        ]);

        $faq = new FAQ();
        $faq->setQuestion($input['question']);
        $faq->setAnswer($input['answer']);

        $this->faqRepo->save($faq);
        $this->faqRepo->saveFAQtoUser($faq, $user);

        session()->flash('message', 'Pitanje je sačuvano!');

        return redirect()->back();
    }


    /**
     * @param PostFAQReplyRequest $request
     *
     * @return array
     */
    public function postFAQReply(PostFAQReplyRequest $request)
    {
        $data = $request->only(['id', 'reply']);

        $faq = $this->faqRepo->findOrFail($data['id']);
        $faq->setAnswer($data['reply']);
        $this->faqRepo->save($faq);

        return [
            'status' => 'OK',
            'reply'  => $faq->getAnswer(),
            'id'     => $faq->getId(),
        ];
    }

    /**
     * @param UpdateFeedbackRequest $request
     * @param                       $username
     * @param                       $slug
     *
     * @return Response
     */
    public function manageFeedback(UpdateFeedbackRequest $request, $username, $slug)
    {
        /** @var User $user */
        $user       = $this->guard->user();
        $feedbacks  = $this->feedbackRepo->getFeedbacksForUser($user, true);
        $internship = $this->internshipRepo->findBySlugOrFail($slug, $user);

        return view('user.feedbackEdit', [
            'user'       => $user,
            'feedbacks'  => $feedbacks,
            'internship' => $internship
        ]);
    }

    /**
     * @param PostUpdateFeedbackRequest $request
     * @param                           $username
     * @param                           $slug
     *
     * @return RedirectResponse
     */
    public function postUpdateFeedback(PostUpdateFeedbackRequest $request, $username, $slug)
    {
        /** @var User $user */
        $user = $this->guard->user();

        $input = $request->only([
            'team_description'
        ]);

        if (!empty($input['team_description'])) {
            $user->setTeamDescription($input['team_description']);
        }

        session()->flash('message', 'Profil je ažuriran!');

        $this->userRepo->save($user);
        return redirect()->back();
    }

    /**
     * @param PostFeedbackReplyRequest $request
     *
     * @return array
     */
    public function postFeedbackReply(PostFeedbackReplyRequest $request)
    {
        $data = $request->only(['id', 'reply']);

        $feedback = $this->feedbackRepo->findOrFail($data['id']);
        $feedback->setReply($data['reply']);
        $this->feedbackRepo->save($feedback);

        return [
            'status' => 'OK',
            'reply'  => $feedback->getReply(),
            'id'     => $feedback->getId(),
        ];
    }

    /**
     * @param DynamicLogo $dynamicLogo
     * @param string      $slug
     *
     * @return Response
     */
    public function dynamicLogo(DynamicLogo $dynamicLogo, $slug)
    {
        $user = $this->userRepo->findBySlugOrFail($slug);


        if (!is_dir(public_path('uploads/colorful'))) {
            mkdir(public_path('uploads/colorful'), 0777, true);
        }

        $hash     = md5($user->getName());
        $fileName = public_path('uploads/colorful/'.$hash.'.png');

        header("Content-Type: image/png");

        if (!is_file($fileName)) {
            $logo = $dynamicLogo->makeLogo($user);
            imagepng($logo, $fileName);
        }
        readfile($fileName);
        die;
    }
}
