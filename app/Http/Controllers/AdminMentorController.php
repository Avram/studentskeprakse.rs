<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Http\Response;
use StudentskePrakse\Http\Requests\Admin\CreateMentorRequest;
use StudentskePrakse\Http\Requests\Admin\DeleteMentorRequest;
use StudentskePrakse\Http\Requests\Admin\UpdateMentorRequest;
use StudentskePrakse\Model\Mentor;
use StudentskePrakse\Repository\MentorRepositoryInterface;
use StudentskePrakse\Repository\UserRepositoryInterface;

class AdminMentorController extends Controller
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var MentorRepositoryInterface
     */
    private $mentorRepo;

    /**
     * AdminMentorController constructor.
     *
     * @param MentorRepositoryInterface $mentorRepo
     * @param UserRepositoryInterface   $userRepo
     */
    public function __construct(MentorRepositoryInterface $mentorRepo, UserRepositoryInterface $userRepo)
    {
        $this->mentorRepo = $mentorRepo;
        $this->userRepo   = $userRepo;
    }

    /**
     * @param $user_id
     *
     * @return Response
     */
    public function getMentors($user_id)
    {
        $user    = $this->userRepo->findOrFail($user_id);
        $mentors = $this->mentorRepo->getMentorsForUser($user);

        return view('mentor.admin.mentorList', [
            'user'    => $user,
            'mentors' => $mentors,
        ]);
    }

    /**
     * @param $user_id
     *
     * @return Response
     */
    public function createMentor($user_id)
    {
        $user   = $this->userRepo->findOrFail($user_id);
        $mentor = new Mentor();

        return view('mentor.admin.mentorForm', [
            'user'   => $user,
            'mentor' => $mentor,
        ]);
    }

    /**
     * @param CreateMentorRequest $request
     * @param                     $user_id
     *
     * @return Response
     */
    public function postCreateMentor(CreateMentorRequest $request, $user_id)
    {
        $user = $this->userRepo->findOrFail($user_id);

        $input = $request->only([
            'name',
            'description',
            'url',
            'facebook_url',
            'twitter_url',
            'linkedin_url',
        ]);

        $newMentor = Mentor::make(
            '', //image
            $input['name'],
            $input['description'],
            $input['twitter_url'],
            $input['facebook_url'],
            $input['linkedin_url'],
            $input['url']
        );

        $this->mentorRepo->save($newMentor);
        $this->mentorRepo->saveMentorToUser($newMentor, $user);

        return redirect()->route('admin.mentor.mentorList', [$user->getId()]);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function updateMentor($id)
    {
        $mentor = $this->mentorRepo->findOrFail($id);

        $user = $this->userRepo->findOrFail($mentor->getUserId());

        return view('mentor.admin.mentorForm', [
            'user'   => $user,
            'mentor' => $mentor,
        ]);
    }

    /**
     * @param UpdateMentorRequest $request
     * @param                     $id
     *
     * @return Response
     */
    public function postUpdateMentor(UpdateMentorRequest $request, $id)
    {
        $mentor = $this->mentorRepo->findOrFail($id);

        $user = $this->userRepo->findOrFail($mentor->getUserId());

        $input = $request->only([
            'name',
            'description',
            'url',
            'facebook_url',
            'twitter_url',
            'linkedin_url',
        ]);

        $mentor->setName($input['name']);
        $mentor->setDescription($input['description']);
        $mentor->setUrl($input['url']);
        $mentor->setFacebookUrl($input['facebook_url']);
        $mentor->setTwitterUrl($input['twitter_url']);
        $mentor->setLinkedinUrl($input['linkedin_url']);

        $this->mentorRepo->save($mentor);

        return redirect()->route('admin.mentor.mentorList', [$user->getId()]);
    }

    /**
     * @param DeleteMentorRequest $request
     * @param                     $id
     *
     * @return Response
     */
    public function deleteMentor(DeleteMentorRequest $request, $id)
    {
        $mentor = $this->mentorRepo->findOrFail($id);
        $user   = $this->userRepo->findOrFail($mentor->getUserId());
        $this->mentorRepo->delete($mentor);
        return redirect()->route('admin.mentor.mentorList', [$user->getId()]);
    }


}
