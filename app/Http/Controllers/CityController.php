<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Http\Request;
use StudentskePrakse\Http\Requests;
use StudentskePrakse\Repository\CityRepositoryInterface;

class CityController extends Controller
{
    private $cityRepo;

    public function __construct(CityRepositoryInterface $cityRepo)
    {
        $this->cityRepo = $cityRepo;
    }

    public function getCity($id)
    {
        $city = $this->cityRepo->findOrFail($id);
        return view('city', ['city' => $city]);
    }

    public function findCity(Request $request)
    {
        $term   = $request->input(['term']);
        $cities = $this->cityRepo->getByName($term);
        return $cities;
    }
}
