<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests;
use StudentskePrakse\Http\Requests\FAQ\CreateFAQRequest;
use StudentskePrakse\Http\Requests\FAQ\DeleteFAQRequest;
use StudentskePrakse\Http\Requests\FAQ\EditFAQRequest;
use StudentskePrakse\Http\Requests\User\EditProfileRequest;
use StudentskePrakse\Model\FAQ;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\FAQRepositoryInterface;
use Symfony\Component\HttpFoundation\Response;

class FAQController extends Controller
{
    /**
     * @var FAQRepositoryInterface
     */
    private $faqRepo;

    /**
     * @var Guard
     */
    private $guard;

    /**
     * FAQController constructor.
     *
     * @param Guard                  $guard
     * @param FAQRepositoryInterface $faqRepo
     */
    public function __construct(Guard $guard, FAQRepositoryInterface $faqRepo)
    {
        $this->guard   = $guard;
        $this->faqRepo = $faqRepo;
    }

    /**
     * @param EditProfileRequest $request
     *
     * @return Response
     */
    public function manageFAQs(EditProfileRequest $request)
    {
        /** @var User $user */
        $user = $this->guard->user();
        $faqs = $this->faqRepo->getFAQsForUser($user, true);

        return view('faq.faqList', [
            'faqs' => $faqs,
        ]);
    }

    /**
     * @param EditProfileRequest $request
     *
     * @return Response
     */
    public function addFAQ(EditProfileRequest $request)
    {
        $faq = new FAQ;
        return view('faq.faqForm', ['faq' => $faq]);
    }

    /**
     * @param CreateFAQRequest $request
     *
     * @return Response
     */
    public function postAddFAQ(CreateFAQRequest $request)
    {
        /** @var User $user */
        $user = $this->guard->user();

        $input = $request->only([
            'question',
            'answer',
        ]);

        $faq = FAQ::make($input['question'], $input['answer']);

        $this->faqRepo->save($faq);
        $this->faqRepo->saveFAQtoUser($faq, $user);

        return redirect()->route('company.faq.management');
    }

    /**
     * @param EditProfileRequest $request
     * @param                    $id
     *
     * @return Response
     */
    public function editFAQ(EditProfileRequest $request, $id)
    {
        $faq = $this->faqRepo->findOrFail($id);
        return view('faq.faqForm', ['faq' => $faq]);
    }

    /**
     * @param EditFAQRequest $request
     * @param                $id
     *
     * @return Response
     */
    public function postEditFAQ(EditFAQRequest $request, $id)
    {
        $input = $request->only([
            'question',
            'answer',
        ]);

        $faq = $this->faqRepo->findOrFail($id);

        $faq->setQuestion($input['question']);
        $faq->setAnswer($input['answer']);

        $this->faqRepo->save($faq);

        return redirect()->route('company.faq.management');
    }

    /**
     * @param DeleteFAQRequest $request
     * @param                  $id
     *
     * @return Response
     */
    public function removeFAQ(DeleteFAQRequest $request, $id)
    {
        $faq = $this->faqRepo->findOrFail($id);
        $this->faqRepo->delete($faq);
        return redirect()->route('company.faq.management');
    }
}
