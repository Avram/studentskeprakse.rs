<?php

namespace StudentskePrakse\Http\Controllers;

use StudentskePrakse\Http\Requests;
use StudentskePrakse\Http\Requests\Admin\CreateAttachmentRequest;
use StudentskePrakse\Http\Requests\Admin\UpdateAttachmentRequest;
use StudentskePrakse\Model\Attachment;
use StudentskePrakse\Repository\AttachmentRepositoryInterface;
use StudentskePrakse\Repository\UserRepositoryInterface;

class AdminAttachmentController extends Controller
{

    private $userRepo;
    private $attachmentRepo;

    public function __construct(UserRepositoryInterface $userRepo, AttachmentRepositoryInterface $attachmentRepo)
    {
        $this->userRepo       = $userRepo;
        $this->attachmentRepo = $attachmentRepo;
    }

    public function getAttachments($uid)
    {
        $user        = $this->userRepo->findOrFail($uid);
        $attachments = $this->attachmentRepo->getAttachmentsForUser($user);

        return view('attachment.admin.list', [
            'user'       => $user,
            'attachment' => $attachments,
        ]);

    }

    public function createAttachment($uid)
    {
        $user       = $this->userRepo->findOrFail($uid);
        $attachment = new Attachment();

        return view('attachment.admin.form', [
            'user'       => $user,
            'attachment' => $attachment
        ]);
    }


    public function postCreateAttachment(CreateAttachmentRequest $request, $uid)
    {
        $user = $this->userRepo->findOrFail($uid);

        $input = $request->only([
            'user_id',
            'type',
            'value',
            'attachment'
        ]);

        $newAttachment = Attachment::make(
            $input['type'],
            $input['value'],
            $input['attachment'],
            $input['user_id']
        );

        $this->attachmentRepo->save($newAttachment);
        $this->attachmentRepo->saveAttachmentForUser($newAttachment, $user);

        return redirect()->route('admin.attachment.list', [$user->getId()]);
    }

    public function updateAttachment($id)
    {
        $attachment = $this->attachmentRepo->findOrFail($id);

        $user = $this->userRepo->findOrFail($attachment->getUserId());

        return view('attachment.admin.form', [
            'user'       => $user,
            'attachment' => $attachment
        ]);

    }

    public function postUpdateMentor(UpdateAttachmentRequest $request, $id)
    {
        $attachment = $this->attachmentRepo->findOrFail($id);

        $user = $this->userRepo->findOrFail($attachment->getUserId());

        $input = $request->only([
            'user_id',
            'type',
            'value',
            'attachment'
        ]);

        $attachment->setType($input['name']);
        $attachment->setValue($input['value']);
        $attachment->setAttachment($input['attachment']);

        $this->attachmentRepo->save($attachment);

        return redirect()->route('admin.attachment.list', [$user->getId()]);
    }


    public function deleteAttachment($id)
    {
        $attachment = $this->attachmentRepo->findOrFail($id);
        $user       = $this->userRepo->findOrFail($attachment->user_id);
        $this->attachmentRepo->delete($attachment);
        return redirect()->route('admin.attachment.list', [$user->getId()]);
    }


}
