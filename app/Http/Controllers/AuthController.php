<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use ReCaptcha\ReCaptcha;
use StudentskePrakse\Exceptions\ReCaptchaException;
use StudentskePrakse\Http\Requests\Auth\LogInRequest;
use StudentskePrakse\Http\Requests\Auth\PostLoginRequest;
use StudentskePrakse\Http\Requests\Auth\RegistrationRequest;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\UserRepositoryInterface;
use StudentskePrakse\Service\AppMailer;

class AuthController extends Controller
{
    use ResetsPasswords;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    public function __construct(UserRepositoryInterface $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * @param RegistrationRequest $request
     *
     * @return Response
     */
    public function getRegistration(RegistrationRequest $request)
    {
        return view('auth.register');
    }

    /**
     * @param AppMailer           $mailer
     * @param RegistrationRequest $request
     * @param Hasher              $hasher
     * @param Guard|StatefulGuard $guard
     *
     * @return Response
     * @throws ReCaptchaException
     */
    public function postRegistration(AppMailer $mailer, RegistrationRequest $request, Hasher $hasher, Guard $guard)
    {
        $input = $request->only([
            'name',
            'email',
            'password',
            'type',
            'employees',
            'founded',
            'address',
            'description',
            'url',
            'g-recaptcha-response'
        ]);

        if (empty($input['g-recaptcha-response'])) {
            session()->flash('error', 'Morate rešiti ReCaptcha-u');
            $request->flash();
            return redirect()->back();
        }

        $testUser = $this->userRepo->findByName(trim($input['name']));

        if ($testUser !== null) {
            session()->flash('warning', 'Kompanija sa tim imenom već postoji!');
            $request->flash();
            return redirect()->back();
        }


        $testUser = $this->userRepo->findByEmail(trim($input['email']));

        if ($testUser !== null) {
            session()->flash('warning', 'Nalog sa tom e-mail adresom već postoji!');
            $request->flash();
            return redirect()->back();
        }


        $recaptcha = new ReCaptcha(env('RECAPTCHA_SECRET_KEY', ''));
        $resp      = $recaptcha->verify($input['g-recaptcha-response'], $request->getClientIp());
        if (!$resp->isSuccess()) {
            throw new ReCaptchaException($resp->getErrorCodes());
        }

        $newUser = User::make(
            $input['name'],
            $input['email'],
            $hasher->make($input['password']),
            $input['type'],
            $input['employees'],
            $input['founded'],
            $input['address'],
            $input['description'],
            $input['url']
        );

        $this->userRepo->save($newUser);

        $mailer->sendActivationMail($newUser);

        session()->flash('message', 'Verifikacioni e-mail je poslat na vašu e-mail adresu.');

        return redirect()->route('home');
    }

    /**
     * @param LogInRequest $request
     *
     * @return Response
     */
    public function getLogin(LogInRequest $request)
    {
        return view('auth.login');
    }

    /**
     * @param PostLoginRequest $request
     * @param Guard            $guard
     *
     * @return Response
     */
    public function postLogin(PostLoginRequest $request, Guard $guard)
    {
        $input = $request->only([
            'email',
            'password',
            'remember',
        ]);

        $success = $guard->attempt([
            'email'    => $input['email'],
            'password' => $input['password'],
        ], $input['remember']);

        if (!$success) {
            session()->flash('error', 'Pogrešna e-mail adresa ili lozinka!');
            $request->flash();
            return redirect()->back();
        }

        return redirect()->route('home');
    }

    /**
     * @param Guard $guard
     *
     * @return Redirector
     */
    public function getLogout(Guard $guard)
    {
        $guard->logout();
        return redirect()->route('home');
    }

    /**
     * @param UserRepositoryInterface $userRepo
     * @param Guard                   $guard
     * @param string                  $token
     *
     * @return Response
     */
    public function verifyEmail(UserRepositoryInterface $userRepo, Guard $guard, $token)
    {
        $user = $userRepo->findByTokenOrFail($token);

        $user->setVerified(true);
        $user->clearVerificationToken();
        $userRepo->save($user);

        session()->flash('message', 'Vaš nalog je verifikovan.');

        $guard->login($user);

        return redirect()->route('internship.create');
    }

    /**
     * @param UserRepositoryInterface $userRepo
     * @param AppMailer               $mailer
     * @param                         $id
     *
     * @return Response
     */
    public function resendVerificationEmail(UserRepositoryInterface $userRepo, AppMailer $mailer, $id)
    {
        $user = $userRepo->findOrFail($id);


        if ($user->isVerified()) {
            session()->flash('message', 'Ovaj nalog je već verifikovan.');
            return redirect()->route('auth.login');
        }

        $user->setVerificationToken();
        $mailer->sendActivationMail($user);
        $userRepo->save($user);

        session()->flash('message', 'Verifikacioni e-mail je poslat na vašu e-mail adresu.');
        return redirect()->route('auth.login');
    }
}