<?php

namespace StudentskePrakse\Http\Controllers;

use StudentskePrakse\Http\Requests;
use StudentskePrakse\Http\Requests\Comment\CreateCommentRequest;
use StudentskePrakse\Model\Comment;
use StudentskePrakse\Repository\CommentRepositoryInterface;
use StudentskePrakse\Repository\PostRepositoryInterface;

class CommentController extends Controller
{
    /**
     * @var CommentRepositoryInterface
     */
    private $commentRepo;
    /**
     * @var PostRepositoryInterface
     */
    private $postRepo;

    public function __construct(CommentRepositoryInterface $commentRepo, PostRepositoryInterface $postRepo)
    {
        $this->commentRepo = $commentRepo;
        $this->postRepo    = $postRepo;
    }

    public function createComment(CreateCommentRequest $request, $post_id)
    {
        $post    = $this->postRepo->findOrFail($post_id);
        $comment = new Comment;
        $comment->setName($request->input('name'));
        $comment->setContent($request->input('content'));

        $post->comments()->save($comment);

        return redirect()->route('post.single', [$post->slug]);
    }
}
