<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Http\Response;
use StudentskePrakse\Http\Requests;
use StudentskePrakse\Http\Requests\Admin\CreateFeedbackRequest;
use StudentskePrakse\Http\Requests\Admin\DeleteFeedbackRequest;
use StudentskePrakse\Http\Requests\Admin\UpdateFeedbackRequest;
use StudentskePrakse\Model\Feedback;
use StudentskePrakse\Repository\FeedbackRepositoryInterface;
use StudentskePrakse\Repository\UserRepositoryInterface;

class AdminFeedbackController extends Controller
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var FeedbackRepositoryInterface
     */
    private $feedbackRepo;

    /**
     * AdminFeedbackController constructor.
     *
     * @param FeedbackRepositoryInterface $feedbackRepo
     * @param UserRepositoryInterface     $userRepo
     */
    public function __construct(FeedbackRepositoryInterface $feedbackRepo, UserRepositoryInterface $userRepo)
    {
        $this->feedbackRepo = $feedbackRepo;
        $this->userRepo     = $userRepo;
    }

    /**
     * @param $uid
     *
     * @return Response
     */
    public function getFeedbacks($uid)
    {
        $user      = $this->userRepo->findOrFail($uid);
        $feedbacks = $this->feedbackRepo->getFeedbacksForUser($user);

        return view('feedback.admin.feedbackList', [
            'user'      => $user,
            'feedbacks' => $feedbacks,
        ]);
    }

    /**
     * @param $uid
     *
     * @return Response
     */
    public function createFeedback($uid)
    {
        $user     = $this->userRepo->findOrFail($uid);
        $feedback = new Feedback();

        return view('feedback.admin.feedbackForm', [
            'user'     => $user,
            'feedback' => $feedback,
        ]);
    }

    /**
     * @param CreateFeedbackRequest $request
     * @param                       $uid
     *
     * @return Response
     */
    public function postCreateFeedback(CreateFeedbackRequest $request, $uid)
    {
        $user = $this->userRepo->findOrFail($uid);

        $input = $request->only([
            'feedback',
            'feedback_name',
            'feedback_reply',
        ]);

        $newFeedback = Feedback::make(
            '', //image
            $input['feedback'],
            $input['feedback_name'],
            $input['feedback_reply']
        );

        $this->feedbackRepo->save($newFeedback);
        $this->feedbackRepo->saveFeedbackToUser($newFeedback, $user);

        return redirect()->route('admin.feedback.feedbackList', [$user->getId()]);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function updateFeedback($id)
    {
        $feedback = $this->feedbackRepo->findOrFail($id);

        $user = $this->userRepo->findOrFail($feedback->getUserId());

        return view('feedback.admin.feedbackForm', [
            'user'     => $user,
            'feedback' => $feedback,
        ]);
    }

    /**
     * @param UpdateFeedbackRequest $request
     * @param                       $id
     *
     * @return Response
     */
    public function postUpdateFeedback(UpdateFeedbackRequest $request, $id)
    {
        $feedback = $this->feedbackRepo->findOrFail($id);

        $input = $request->only([
            'feedback',
            'feedback_name',
            'feedback_reply',
        ]);

        $feedback->setFeedback($input['feedback']);
        $feedback->setName($input['feedback_name']);
        $feedback->setReply($input['feedback_reply']);

        $user = $this->userRepo->findOrFail($feedback->getUserId());

        $this->feedbackRepo->save($feedback);

        return redirect()->route('admin.feedback.feedbackList', [$user->getId()]);
    }

    /**
     * @param DeleteFeedbackRequest $request
     * @param                       $id
     *
     * @return Response
     */
    public function deleteFeedback(DeleteFeedbackRequest $request, $id)
    {
        $feedback = $this->feedbackRepo->findOrFail($id);
        $user     = $this->userRepo->findOrFail($feedback->getUserId());
        $this->feedbackRepo->delete($feedback);
        return redirect()->route('admin.feedback.feedbackList', [$user->getId()]);
    }

}
