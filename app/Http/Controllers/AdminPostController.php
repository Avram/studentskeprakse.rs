<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use StudentskePrakse\Http\Requests\Admin\CreatePostRequest;
use StudentskePrakse\Http\Requests\Admin\DeletePostRequest;
use StudentskePrakse\Http\Requests\Admin\UpdatePostRequest;
use StudentskePrakse\Model\Post;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\PostRepositoryInterface;

class AdminPostController extends Controller
{

    /**
     * @var PostRepositoryInterface
     */
    private $postRepo;

    /**
     * AdminPostController constructor.
     *
     * @param PostRepositoryInterface $postRepo
     */
    public function __construct(PostRepositoryInterface $postRepo)
    {
        $this->postRepo = $postRepo;
    }

    /**
     * @return Response
     */
    public function getPosts()
    {
        $posts = $this->postRepo->getAll();
        return view('post.admin.postList', ['posts' => $posts]);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function getPost($id)
    {
        $post = $this->postRepo->findOrFail($id);
        return view('post.admin.postSingle', ['post' => $post]);
    }

    /**
     * @return Response
     */
    public function createPost()
    {
        $post = new Post();
        return view('post.admin.postForm', ['post' => $post]);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function updatePost($id)
    {
        $post = $this->postRepo->findOrFail($id);
        return view('post.admin.postForm', ['post' => $post]);
    }

    /**
     * @param CreatePostRequest $request
     * @param Guard             $guard
     *
     * @return Response
     */
    public function postCreatePost(CreatePostRequest $request, Guard $guard)
    {
        /**
         * @var User $user
         */
        $user = $guard->user();
        $post = new Post;
        $post->setTitle($request->input('title'));
        $post->setContent($request->input('content'));
        $slug = Str::slug($post->getTitle());
        $post->setSlug($slug);

        $user->posts()->save($post);
        return redirect()->route('admin.post.postSingle', [$post->getId()]);
    }

    /**
     * @param UpdatePostRequest $request
     * @param Guard             $guard
     * @param                   $id
     *
     * @return Response
     */
    public function postUpdatePost(UpdatePostRequest $request, Guard $guard, $id)
    {
        /**
         * @var User $user
         */
        $user = $guard->user();
        $post = $this->postRepo->findOrFail($id);
        $post->setTitle($request->input('title'));
        $post->setContent($request->input('content'));
        $slug = Str::slug($post->getTitle());
        $post->setSlug($slug);

        $user->posts()->save($post);
        return redirect()->route('admin.post.postSingle', [$post->getId()]);
    }

    /**
     * @param DeletePostRequest $request
     * @param                   $id
     *
     * @return Response
     */
    public function deletePost(DeletePostRequest $request, $id)
    {
        $post = $this->postRepo->findOrFail($id);
        $this->postRepo->delete($post);
        return redirect()->route('admin.post.postList');
    }
}
