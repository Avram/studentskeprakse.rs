<?php

namespace StudentskePrakse\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use StudentskePrakse\Http\Requests;
use StudentskePrakse\Http\Requests\Admin\DeleteCommentRequest;
use StudentskePrakse\Model\Comment;
use StudentskePrakse\Repository\CommentRepositoryInterface;

class AdminCommentController extends Controller
{
    /**
     * @var CommentRepositoryInterface
     */
    private $commentRepo;

    public function __construct(CommentRepositoryInterface $commentRepo)
    {
        $this->commentRepo = $commentRepo;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments()
    {
        $comments = $this->commentRepo->getAll();
        return $comments;
    }

    /**
     * @param $id
     *
     * @return null|Comment
     */
    public function getComment($id)
    {
        $comment = $this->commentRepo->find($id);
        return $comment; //@TODO: Dodati view
    }

    /**
     * @param DeleteCommentRequest $id
     *
     * @return mixed
     */
    public function deleteComment(DeleteCommentRequest $id)
    {
        $comment = $this->commentRepo->findOrFail($id);
        $post    = $comment->parent;
        $this->commentRepo->delete($comment);
        return redirect()->route('admin.post.single', [$post->slug]);
    }
}