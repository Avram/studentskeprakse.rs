<?php

namespace StudentskePrakse\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use StudentskePrakse\Model\User;

class CheckEmailVerified
{

    /**
     * @var Guard
     */
    private $guard;

    /**
     * CheckEmailVerified constructor.
     *
     * @param Guard $guard
     */
    public function __construct(Guard $guard)
    {
        $this->guard = $guard;
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * @var User $user
         */
        $user = $this->guard->user();
        if ($user && !$user->isVerified()) {
            session()->flash('account-not-verified-id', $user->getId());
            $this->guard->logout();
            return redirect()->route('auth.login');
        }

        return $next($request);
    }
}
