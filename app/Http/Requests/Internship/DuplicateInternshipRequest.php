<?php

namespace StudentskePrakse\Http\Requests\Internship;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\EloquentInternshipRepository;

class DuplicateInternshipRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param EloquentInternshipRepository $internshipRepo
     * @param Guard                        $guard
     *
     * @return bool
     */
    public function authorize(EloquentInternshipRepository $internshipRepo, Guard $guard)
    {
        /** @var User $user */
        $user = $guard->user();

        if ($user === null) {
            return false;
        }

        $internship = $internshipRepo->findBySlugOrFail($this->slug, $user);

        return (int)$user->getId() === (int)$internship->getUserId();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
