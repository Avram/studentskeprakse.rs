<?php

namespace StudentskePrakse\Http\Requests\Internship;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\InternshipRepositoryInterface;

class DeleteInternshipRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param InternshipRepositoryInterface $internshipRepo
     * @param Guard                         $guard
     *
     * @return bool
     */
    public function authorize(InternshipRepositoryInterface $internshipRepo, Guard $guard)
    {
        /**
         * @var User $user
         */
        $user       = $guard->user();
        $internship = $internshipRepo->findBySlug($this->slug, $user, true);

        return $user->getId() === $internship->getUserId();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
}
