<?php

namespace StudentskePrakse\Http\Requests\Internship;

class PostCreateInternshipRequest extends CreateInternshipRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id'         => 'required',
            'category_id'     => 'required',
            'name'            => 'string|required',
            'content'         => 'string|required',
            'required_skills' => 'string',
            'duration'        => 'string',
            'work_dwm'        => 'required',
            'salary'          => 'string',
            'status'          => 'string',
            'food'            => 'array',
            'work_hours'      => 'string',
        ];
    }
}
