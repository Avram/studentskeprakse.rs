<?php

namespace StudentskePrakse\Http\Requests\Internship;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\EloquentInternshipRepository;

class UpdateInternshipRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param EloquentInternshipRepository $internshipRepo
     * @param Guard                        $guard
     *
     * @return bool
     */
    public function authorize(EloquentInternshipRepository $internshipRepo, Guard $guard)
    {
        /**
         * @var User $user
         */
        $user       = $guard->user();
        $internship = $internshipRepo->findBySlug($this->internship, $user);

        return (int)$user->getId() === (int)$internship->getUserId();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id'         => 'required',
            'category_id'     => 'required',
            'name'            => 'string',
            'content'         => 'string',
            'required_skills' => 'string',
            'duration'        => 'string',
            'work_dwm'        => 'string',
            'salary'          => 'string',
            'food'            => 'array',
            'work_hours'      => 'string',
        ];
    }
}
