<?php

namespace StudentskePrakse\Http\Requests\Attachment;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\AttachmentRepositoryInterface;

class AjaxRemoveAttachmentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Guard                         $guard
     * @param AttachmentRepositoryInterface $attachmentRepo
     *
     * @return bool
     */
    public function authorize(Guard $guard, AttachmentRepositoryInterface $attachmentRepo)
    {
        /** @var User $user */
        $user = $guard->user();
        $id   = $this->input('id');

        if ($user === null) {
            return false;
        }

        $attachment = $attachmentRepo->findOrFail($id);

        return (int)$attachment->getUserId() === (int)$user->getId();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
