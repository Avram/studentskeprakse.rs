<?php namespace StudentskePrakse\Http\Requests\Admin;


use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;

class CreateCategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Guard $guard
     *
     * @return mixed
     */
    public function authorize(Guard $guard)
    {
        return $guard->user()->isAdministrator();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent_id' => 'required',
            'name'      => 'required',
            'order'     => 'required'
        ];

    }
}