<?php

namespace StudentskePrakse\Http\Requests\Admin;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;

class UpdateInternshipRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Guard $guard
     *
     * @return bool
     */
    public function authorize(Guard $guard)
    {
        return $guard->user()->isAdministrator();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'         => 'required',
            'city_id'         => 'required',
            'category_id'     => 'required',
            'name'            => 'string',
            'content'         => 'string',
            'required_skills' => 'string',
            'duration'        => 'string',
            'salary'          => 'string',
            'status'          => 'string',
            'food'            => 'array',
            'work_hours'      => 'string',
        ];
    }
}
