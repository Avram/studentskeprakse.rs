<?php

namespace StudentskePrakse\Http\Requests\Admin;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;

class CreateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Guard $guard
     *
     * @return bool
     */
    public function authorize(Guard $guard)
    {
        return $guard->user()->isAdministrator();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|unique:users,name',
            'email'        => 'required|unique:users,email',
            'password'     => 'required|confirmed',
            'address'      => 'string',
            'description'  => 'string',
            'url'          => 'url',
            'facebook_url' => 'url',
            'twitter_url'  => 'url',
            'linkedin_url' => 'url',
            'category_id'  => 'required|numeric',
            'city_id'      => 'required|numeric',
        ];
    }
}
