<?php

namespace StudentskePrakse\Http\Requests\User;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\FeedbackRepositoryInterface;

class PostFeedbackReplyRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Guard                       $guard
     * @param FeedbackRepositoryInterface $feedbackRepo
     *
     * @return bool
     */
    public function authorize(Guard $guard, FeedbackRepositoryInterface $feedbackRepo)
    {
        /** @var User $guardUser */
        $guardUser = $guard->user();
        if (!$guardUser) {
            return false;
        }

        $feedback = $feedbackRepo->find($this->input('id'));

        return $guardUser->getId() === $feedback->getUserId();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'    => 'integer',
            'reply' => 'string',
        ];
    }
}
