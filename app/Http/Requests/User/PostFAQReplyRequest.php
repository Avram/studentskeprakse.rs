<?php

namespace StudentskePrakse\Http\Requests\User;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;
use StudentskePrakse\Model\FAQ;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\FAQRepositoryInterface;
use StudentskePrakse\Repository\FeedbackRepositoryInterface;

class PostFAQReplyRequest extends Request
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Guard                  $guard
     * @param FAQRepositoryInterface $faqRepo
     *
     * @return bool
     */
    public function authorize(Guard $guard, FAQRepositoryInterface $faqRepo)
    {
        /** @var User $guardUser */
        $guardUser = $guard->user();
        if (!$guardUser) {
            return false;
        }

        $faq = $faqRepo->find($this->input('id'));

        return $guardUser->getId() === $faq->getOwnerId();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'    => 'integer',
            'reply' => 'string',
        ];
    }
}
