<?php

namespace StudentskePrakse\Http\Requests\User;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;

class UpdateAboutCompanyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Guard $guard
     *
     * @return bool
     */
    public function authorize(Guard $guard)
    {
        return !$guard->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'  => 'string|required',
            'logo-edit'    => 'image',
            'employees'    => 'string',
            'founded'      => 'integer',
            'category_id'  => 'integer|required',
            'address'      => 'string',
            'city_id'      => 'integer',
            'url'          => 'url',
            'facebook_url' => 'url',
            'twitter_url'  => 'url',
            'linkedin_url' => 'url',
        ];
    }
}
