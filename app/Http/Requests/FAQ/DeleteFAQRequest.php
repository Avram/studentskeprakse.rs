<?php

namespace StudentskePrakse\Http\Requests\FAQ;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\FAQRepositoryInterface;

class DeleteFAQRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Guard                  $guard
     * @param FAQRepositoryInterface $faqRepo
     *
     * @return bool
     */
    public function authorize(Guard $guard, FAQRepositoryInterface $faqRepo)
    {
        if ($guard->guest()) {
            return false;
        }

        /** @var User $user */
        $user = $guard->user();

        $faq = $faqRepo->findOrFail($this->id);
        return $faq->getOwnerId() === $user->getId();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
