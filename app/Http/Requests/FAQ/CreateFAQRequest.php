<?php

namespace StudentskePrakse\Http\Requests\FAQ;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;

class CreateFAQRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Guard $guard
     *
     * @return bool
     */
    public function authorize(Guard $guard)
    {
        return !$guard->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question' => 'string|required',
            'answer'   => 'string',
        ];
    }
}
