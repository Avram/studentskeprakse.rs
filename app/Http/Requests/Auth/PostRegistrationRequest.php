<?php namespace StudentskePrakse\Http\Requests\Auth;


class PostRegistrationRequest extends RegistrationRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|unique:users,name',
            'email'    => 'required|unique:users,email',
            'password' => 'required',
        ];
    }
}