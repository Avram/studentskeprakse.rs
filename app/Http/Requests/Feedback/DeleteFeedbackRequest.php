<?php

namespace StudentskePrakse\Http\Requests\Feedback;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;
use StudentskePrakse\Repository\FeedbackRepositoryInterface;

class DeleteFeedbackRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Guard $guard, FeedbackRepositoryInterface $feedbackRepo)
    {
        if ($guard->guest()) {
            return false;
        }

        /** @var User $user */
        $user = $guard->user();

        $feedback = $feedbackRepo->findOrFail($this->id);
        return $feedback->getUserId() === $user->getId();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
