<?php

namespace StudentskePrakse\Http\Requests\Mentor;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;

class CreateMentorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Guard $guard
     *
     * @return bool
     */
    public function authorize(Guard $guard)
    {
        return !$guard->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'string|required',
            'description'  => 'string',
            'url'          => 'url',
            'facebook_url' => 'url',
            'twitter_url'  => 'url',
            'linkedin_url' => 'url',
        ];
    }
}
