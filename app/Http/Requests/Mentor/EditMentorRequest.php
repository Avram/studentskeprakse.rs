<?php

namespace StudentskePrakse\Http\Requests\Mentor;

use Illuminate\Contracts\Auth\Guard;
use StudentskePrakse\Http\Requests\Request;
use StudentskePrakse\Model\User;
use StudentskePrakse\Repository\MentorRepositoryInterface;

class EditMentorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Guard                     $guard
     * @param MentorRepositoryInterface $mentorRepo
     *
     * @return bool
     */
    public function authorize(Guard $guard, MentorRepositoryInterface $mentorRepo)
    {
        if ($guard->guest()) {
            return false;
        }

        /** @var User $user */
        $user = $guard->user();

        $mentor = $mentorRepo->findOrFail($this->id);
        return $mentor->getUserId() === $user->getId();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'string|required',
            'description'  => 'string',
            'url'          => 'url',
            'facebook_url' => 'url',
            'twitter_url'  => 'url',
            'linkedin_url' => 'url',
        ];
    }
}
