<?php

namespace StudentskePrakse\Model;

use Illuminate\Database\Eloquent\Model;

class FAQ extends Model
{
    const OWNER_USER = 'user';
    const OWNER_INTERNSHIP = 'internship';
    const OWNER_CLASS_USER = User::class;
    const OWNER_CLASS_INTERNSHIP = Internship::class;

    public static $ALLOWED_OWNERS = [
        self::OWNER_USER,
        self::OWNER_INTERNSHIP
    ];

    /**
     * @var string
     */
    protected $table = 'faqs';
    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return integer
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @return integer
     */
    public function getOwnerId()
    {
        return (int)$this->owner_id;
    }

    /**
     * @return string
     */
    public function getOwnerTypeClass()
    {
        return $this->owner_type;
    }

    /**
     * @return null|string
     */
    public function getOwnerType()
    {
        if ($this->owner_type == self::OWNER_CLASS_USER) {
            return self::OWNER_USER;
        } elseif ($this->owner_type == self::OWNER_CLASS_INTERNSHIP) {
            return self::OWNER_INTERNSHIP;
        }

        return null;
    }

    /**
     * @return User|Internship
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function owner()
    {
        return $this->morphTo();
    }

    /**
     * @param string $question
     * @param string $answer
     *
     * @return static
     */
    public static function make($question, $answer)
    {

        return new static([
            'question' => $question,
            'answer'   => $answer
        ]);
    }
}
