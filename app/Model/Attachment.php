<?php

namespace StudentskePrakse\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use StudentskePrakse\Presenter\Contract\PresentableInterface;
use StudentskePrakse\Presenter\PresentableTrait;

class Attachment extends Model implements PresentableInterface
{

    use PresentableTrait;

    const TYPE_LINK = 'link';
    const TYPE_IMAGE = 'image';
    const TYPE_TEXT = 'text';
    const TYPE_PDF = 'pdf';
    const TYPE_VIDEO = 'video';
    const TYPE_OFFICE = 'office';

    const VIDEO_YOUTUBE = 'youtube';
    const VIDEO_VIMEO = 'vimeo';
    const VIDEO_NOTVIDEO = 'notvideo';
    const VIDEO_UNKNOWN = 'unknown';

    protected $presenter = \StudentskePrakse\Presenter\Attachment::class;

    /**
     * @var array
     */
    static $VALID_TYPES = [
        self::TYPE_LINK,
        self::TYPE_IMAGE,
        self::TYPE_TEXT,
        self::TYPE_PDF,
        self::TYPE_VIDEO,
        self::TYPE_OFFICE
    ];

    /**
     * @var string
     */
    protected $table = 'attachments';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return integer
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @return integer
     */
    public function getUserId()
    {
        return (int)$this->user_id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * Contains the url of an optional file attached to the Attachment.
     *
     * @param string $attachment
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * @return string
     */
    public function getURL()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setURL($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param integer $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getVideoType()
    {
        if ($this->getType() !== self::TYPE_VIDEO) {
            return self::VIDEO_NOTVIDEO;
        }

        if (stripos($this->getURL(), 'youtu') !== false) {
            return self::VIDEO_YOUTUBE;
        } elseif (stripos($this->getURL(), 'vimeo') !== false) {
            return self::VIDEO_VIMEO;
        }

        return self::VIDEO_UNKNOWN;
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('StudentskePrakse\Model\User');
    }

    /**
     * @param string  $type
     * @param string  $value
     * @param string  $attachment
     * @param integer $user_id
     *
     * @return static
     */
    public static function make($type, $value, $attachment, $user_id)
    {
        if (!in_array($type, self::$VALID_TYPES)) {
            throw new \InvalidArgumentException("Invalid $type value. Possible options are: ".implode(', ', self::$VALID_TYPES));
        }

        return new static([
            'type'       => $type,
            'value'      => $value,
            'attachment' => $attachment,
            'user_id'    => $user_id
        ]);
    }

    /**
     * @return null|string
     */
    public function getUploadPath()
    {
        if ($this->exists) {
            /** @var Carbon $carbon */
            $carbon = $this->created_at;
            return public_path('uploads'.DIRECTORY_SEPARATOR.$carbon->year.DIRECTORY_SEPARATOR.$carbon->month);
        } else {
            return null;
        }
    }
}
