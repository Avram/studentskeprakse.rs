<?php

namespace StudentskePrakse\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Feedback extends Model
{
    /**
     * @var string
     */
    protected $table = 'feedbacks';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return integer
     *
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * @param string $feedback
     */
    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getReply()
    {
        return $this->reply;
    }

    /**
     * @param string $reply
     */
    public function setReply($reply)
    {
        $this->reply = $reply;
    }

    /**
     * @return integer
     */
    public function getUserId()
    {
        return (int)$this->user_id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('StudentskePrakse\Model\User');
    }

    /**
     * @param string $image
     * @param string $feedback
     * @param string $name
     * @param string $reply
     *
     * @return static
     */
    public static function make($image, $feedback, $name, $reply)
    {
        return new static ([
            'image'    => $image,
            'feedback' => $feedback,
            'name'     => $name,
            'reply'    => $reply
        ]);
    }
}
