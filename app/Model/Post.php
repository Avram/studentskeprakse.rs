<?php

namespace StudentskePrakse\Model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const TYPE_ADVICE = 'advice';
    const TYPE_POST = 'post';

    static $VALID_TYPES = [
        self::TYPE_ADVICE,
        self::TYPE_POST
    ];

    const STATUS_PUBLISHED = 'publish';
    const STATUS_DRAFT = 'draft';

    static $VALID_STATUSES = [
        self::STATUS_PUBLISHED,
        self::STATUS_DRAFT
    ];

    /**
     * @var string
     */
    protected $table = 'posts';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return integer
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @return integer
     */
    public function getUserId()
    {
        return (int)$this->user_id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        if (!in_array($status, self::$VALID_STATUSES)) {
            throw new \InvalidArgumentException("Post status must be one of: ".implode(', ', self::$VALID_STATUSES));
        }

        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Upvote a comment
     */
    public function upVote()
    {
        $this->up_votes++;
    }

    /**
     * Upvote a comment
     */
    public function downVote()
    {
        $this->down_votes++;
    }

    /**
     * @return integer
     */
    public function getUpVotes()
    {
        return $this->up_votes;
    }

    /**
     * @return integer
     */
    public function getDownVotes()
    {
        return $this->down_votes;
    }

    /**
     * @return integer
     */
    public function getVotes()
    {
        return $this->getUpVotes() - $this->getDownVotes();
    }

    /**
     * @return \StudentskePrakse\Model\Comment
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $title
     * @param string $content
     * @param string $status
     * @param string $type
     * @param string $up_votes
     * @param string $down_votes
     * @param string $slug
     *
     * @return static
     */
    public static function make($title, $content, $status, $type, $up_votes, $down_votes, $slug)
    {
        if (!in_array($type, self::$VALID_TYPES)) {
            throw new \InvalidArgumentException("Invalid $type value. Possible options are: ".implode(', ', self::$VALID_TYPES));
        }

        return new static([
            'title'      => $title,
            'content'    => $content,
            'status'     => $status,
            'type'       => $type,
            'up_votes'   => $up_votes,
            'down_votes' => $down_votes,
            'slug'       => $slug
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany('StudentskePrakse\Model\Comment', 'parent');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('StudentskePrakse\Model\User');
    }
}
