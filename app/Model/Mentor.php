<?php

namespace StudentskePrakse\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Mentor extends Model
{
    /**
     * @var string
     */
    protected $table = 'mentors';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return integer
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @return integer
     */
    public function getUserId()
    {
        return (int)$this->user_id;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTwitterUrl()
    {
        return $this->twitter_url;
    }

    /**
     * @param mixed $twitter_url
     */
    public function setTwitterUrl($twitter_url)
    {
        $this->twitter_url = $twitter_url;
    }

    /**
     * @return mixed
     */
    public function getFacebookUrl()
    {
        return $this->facebook_url;
    }

    /**
     * @param mixed $facebook_url
     */
    public function setFacebookUrl($facebook_url)
    {
        $this->facebook_url = $facebook_url;
    }

    /**
     * @return mixed
     */
    public function getLinkedinUrl()
    {
        return $this->linkedin_url;
    }

    /**
     * @param mixed $linkedin_url
     */
    public function setLinkedinUrl($linkedin_url)
    {
        $this->linkedin_url = $linkedin_url;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $image
     * @param string $name
     * @param string $description
     * @param string $twitter_url
     * @param string $facebook_url
     * @param string $linkedin_url
     * @param string $url
     *
     * @return static
     */
    public static function make($image, $name, $description, $twitter_url, $facebook_url, $linkedin_url, $url)
    {
        return new static([
            'image'        => $image,
            'name'         => $name,
            'description'  => $description,
            'twitter_url'  => $twitter_url,
            'facebook_url' => $facebook_url,
            'linkedin_url' => $linkedin_url,
            'url'          => $url
        ]);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('StudentskePrakse\Model\User');
    }

    /**
     * @return BelongsTo
     */
    public function internship()
    {
        return $this->belongsToMany('StudentskePrakse\Model\Internship');
    }
}
