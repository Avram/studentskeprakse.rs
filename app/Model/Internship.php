<?php

namespace StudentskePrakse\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use StudentskePrakse\Presenter\Contract\PresentableInterface;
use StudentskePrakse\Presenter\PresentableTrait;

class Internship extends Model implements PresentableInterface
{
    use SoftDeletes, PresentableTrait;

    const MEAL_BREAKFAST = 'breakfast';
    const MEAL_LUNCH = 'lunch';
    const MEAL_DINNER = 'dinner';

    static $ALLOWED_MEALS = [
        self::MEAL_BREAKFAST,
        self::MEAL_LUNCH,
        self::MEAL_DINNER,
    ];

    const STATUS_PUBLISHED = 'publish';
    const STATUS_DRAFT = 'draft';

    static $ALLOWED_STATUSES = [
        self::STATUS_PUBLISHED,
        self::STATUS_DRAFT,
    ];


    const DURATION_DAYS = 'days';
    const DURATION_WEEKS = 'weeks';
    const DURATION_MONTHS = 'months';

    static $ALLOWED_DURATION_TYPES = [
        self::DURATION_DAYS,
        self::DURATION_WEEKS,
        self::DURATION_MONTHS,
    ];

    protected $presenter = \StudentskePrakse\Presenter\Internship::class;

    /**
     * @var string
     */
    protected $table = 'internships';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @return integer
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @return integer
     */
    public function getUserId()
    {
        return (int)$this->user_id;
    }

    /**
     * @return integer
     */
    public function getCategoryId()
    {
        return (int)$this->category_id;
    }

    /**
     * @return integer
     */
    public function getCityId()
    {
        return (int)$this->city_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getRequiredSkills()
    {
        return $this->required_skills;
    }

    /**
     * @param string $required_skills
     */
    public function setRequiredSkills($required_skills)
    {
        $this->required_skills = $required_skills;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function toggleStatus()
    {
        if ($this->status == self::STATUS_PUBLISHED) {
            return $this->status = self::STATUS_DRAFT;
        } else {
            return $this->status = self::STATUS_PUBLISHED;
        }
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getDurationType()
    {
        return $this->duration_type;
    }

    /**
     * @param $duration_type
     */
    public function setDurationType($duration_type)
    {
        if (!in_array($duration_type, self::$ALLOWED_DURATION_TYPES)) {
            throw new \InvalidArgumentException("Duration type is not in the list of allowed duration types: ".implode(', ', self::$ALLOWED_DURATION_TYPES));
        }

        $this->duration_type = $duration_type;
    }

    /**
     * @return string
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param string $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    /**
     * @return bool
     */
    public function hasSalary()
    {
        return !empty($this->salary);
    }

    /**
     * @return array
     */
    public function getMeals()
    {
        return explode(',', $this->food);
    }

    /**
     * @param string $meal
     *
     * @return boolean
     */
    public function hasMeal($meal)
    {
        return in_array($meal, $this->getMeals());
    }

    /**
     * @return bool
     */
    public function hasMeals()
    {
        return !empty($this->food);
    }

    /**
     * @param array|string $meals
     */
    public function setMeals($meals)
    {
        $meals      = is_array($meals) ? implode(',', $meals) : $meals;
        $this->food = $meals;
    }

    /**
     * @return string
     */
    public function getWorkHours()
    {
        return $this->work_hours;
    }

    /**
     * @param string $work_hours
     */
    public function setWorkHours($work_hours)
    {
        $this->work_hours = $work_hours;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return integer
     */
    public function getViewCount()
    {
        return $this->views;
    }

    /**
     * Increment view counter
     */
    public function incrementViewCount()
    {
        $this->views++;
    }

    public function isDummy()
    {
        return ($this->slug == 'new-internship');
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('StudentskePrakse\Model\Category');
    }

    /**
     * @return BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('StudentskePrakse\Model\City');
    }

    /**
     * @return MorphMany
     */
    public function faqs()
    {
        return $this->morphMany('StudentskePrakse\Model\FAQ', 'owner');
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('StudentskePrakse\Model\User');
    }

    /**
     * @return BelongsToMany
     */
    public function mentors()
    {
        return $this->belongsToMany('StudentskePrakse\Model\Mentor');
    }

    /**
     * @param string  $name
     * @param string  $content
     * @param string  $required_skills
     * @param boolean $status
     * @param string  $duration
     * @param string  $salary
     * @param string  $food
     * @param string  $work_hours
     * @param string  $slug
     *
     * @return static
     */
    public static function make($name, $content, $required_skills, $status, $duration, $salary, $food, $work_hours, $slug)
    {
        return new static([
            'name'            => $name,
            'content'         => $content,
            'required_skills' => $required_skills,
            'status'          => $status,
            'duration'        => $duration,
            'salary'          => $salary,
            'food'            => $food,
            'work_hours'      => $work_hours,
            'slug'            => $slug
        ]);
    }

    /**
     * @return Internship
     */
    public static function dummy()
    {
        $object = new Internship;
        $object->setSlug('new-internship');
        $object->setStatus(self::STATUS_PUBLISHED);
        return $object;
    }
}
