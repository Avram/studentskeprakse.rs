<?php

namespace StudentskePrakse\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class City extends Model
{
    /**
     * @var string
     */
    protected $table = 'cities';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return integer
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return HasMany
     */
    public function internships()
    {
        return $this->hasMany('StudentskePrakse\Model\Internship');
    }

    /**
     * @return HasMany
     */
    public function users()
    {
        return $this->hasMany('StudentskePrakse\Model\User');
    }

    /**
     * @param string $name
     *
     * @return static
     */
    public static function make($name)
    {
        return new static([
            'name' => $name,
        ]);
    }
}
