<?php

namespace StudentskePrakse\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Comment extends Model
{
    /**
     * @var string
     */
    protected $table = 'comments';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return integer
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return Post|Comment
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Model $parent
     */
    public function setParent(Model $parent)
    {
        if (!$parent instanceof Comment && !$parent instanceof Post) {
            throw new \InvalidArgumentException("Comment parent must be instance of Comment or Post!");
        }

        $this->parent = $parent;
    }

    /**
     * @return integer
     */
    public function getUpVotes()
    {
        return $this->up_votes;
    }

    /**
     * @return integer
     */
    public function getDownVotes()
    {
        return $this->down_votes;
    }

    /**
     * @return integer
     */
    public function getVotes()
    {
        return $this->getUpVotes() - $this->getDownVotes();
    }

    /**
     * Upvote a comment
     */
    public function upVote()
    {
        $this->up_votes++;
    }

    public function downVote()
    {
        $this->down_votes++;
    }

    /**
     * @return MorphTo
     */
    public function parent()
    {
        return $this->morphTo();
    }

    /**
     * @return MorphMany
     */
    public function comments()
    {
        return $this->morphMany('StudentskePrakse\Model\Comment', 'parent');
    }

    /**
     * @param string $name
     *
     * @return static
     */
    public static function make($name)
    {

        return new static([
            'name' => $name
        ]);
    }
}
