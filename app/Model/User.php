<?php

namespace StudentskePrakse\Model;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use StudentskePrakse\Presenter\Contract\PresentableInterface;
use StudentskePrakse\Presenter\PresentableTrait;

class User extends Authenticatable implements PresentableInterface
{
    use CanResetPassword, PresentableTrait;

    protected $presenter = \StudentskePrakse\Presenter\User::class;

    const TYPE_ADMIN = 'admin';
    const TYPE_COMPANY = 'company';

    /**
     * @var array
     */
    static $VALID_TYPES = [
        self::TYPE_ADMIN,
        self::TYPE_COMPANY
    ];

    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return integer
     */
    public function getCategoryId()
    {
        return (int)$this->category_id;
    }

    /**
     * @return integer
     */
    public function getCityId()
    {
        return (int)$this->city_id;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        if (!in_array($type, self::$VALID_TYPES)) {
            throw new \InvalidArgumentException("Type must be one of the valid types: ".implode(', ', self::$VALID_TYPES));
        }

        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * @param string $employees
     */
    public function setEmployees($employees)
    {
        $this->employees = $employees;
    }

    /**
     * @return integer
     */
    public function getFounded()
    {
        return $this->founded;
    }

    /**
     * @param integer $founded
     */
    public function setFounded($founded)
    {
        $this->founded = $founded;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


    /**
     * @return string
     */
    public function getTeamDescription()
    {
        return $this->team_description;
    }

    /**
     * @param $team_description
     */
    public function setTeamDescription($team_description)
    {
        $this->team_description = $team_description;
    }

    public function setVerificationToken($token = null)
    {
        $token       = $token ?: str_random(30);
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getVerificationToken()
    {
        return $this->token;
    }

    /**
     * Remove email verification token
     */
    public function clearVerificationToken()
    {
        $this->token = null;
    }

    /**
     * @return boolean
     */
    public function isVerified()
    {
        return (boolean)$this->verified;
    }

    /**
     * @param bool $verified
     */
    public function setVerified($verified = true)
    {
        $this->verified = $verified;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }


    /**
     * @return string
     */
    public function getFacebookUrl()
    {
        return $this->facebook_url;
    }

    /**
     * @param string $facebook_url
     */
    public function setFacebookUrl($facebook_url)
    {
        $this->facebook_url = $facebook_url;
    }

    /**
     * @return string
     */
    public function getTwitterUrl()
    {
        return $this->twitter_url;
    }

    /**
     * @param string $twitter_url
     */
    public function setTwitterUrl($twitter_url)
    {
        $this->twitter_url = $twitter_url;
    }

    /**
     * @return string
     */
    public function getLinkedinUrl()
    {
        return $this->linkedin_url;
    }

    /**
     * @param string $linkedin_url
     */
    public function setLinkedinUrl($linkedin_url)
    {
        $this->linkedin_url = $linkedin_url;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @param string $cover
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    }

    /**
     * @param string  $name
     * @param string  $email
     * @param string  $password
     * @param string  $type
     * @param string  $employees
     * @param integer $founded
     * @param string  $address
     * @param string  $description
     * @param string  $url
     * @param string  $facebook_url
     * @param string  $twitter_url
     * @param string  $linkedin_url
     *
     * @return static
     */
    public static function make($name, $email, $password, $type, $employees, $founded, $address, $description, $url, $facebook_url = null, $twitter_url = null, $linkedin_url = null)
    {
        return new static([
            'name'         => $name,
            'email'        => $email,
            'password'     => $password,
            'type'         => $type,
            'employees'    => $employees,
            'founded'      => $founded,
            'address'      => $address,
            'description'  => $description,
            'url'          => $url,
            'facebook_url' => $facebook_url,
            'twitter_url'  => $twitter_url,
            'linkedin_url' => $linkedin_url,
            'slug'         => str_slug($name)
        ]);
    }

    /**
     * Automatically set email verification token
     */
    public static function boot()
    {
        static::creating(function (User $user) {
            $user->setVerificationToken();
        });
    }

    /**
     * @return HasMany
     */
    public function attachments()
    {
        return $this->hasMany('StudentskePrakse\Model\Attachment');
    }

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('StudentskePrakse\Model\Category');
    }

    /**
     * @return BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('StudentskePrakse\Model\City');
    }

    /**
     * @return MorphMany
     */
    public function faqs()
    {
        return $this->morphMany('StudentskePrakse\Model\FAQ', 'owner');
    }

    /**
     * @return HasMany
     */
    public function feedbacks()
    {
        return $this->hasMany('StudentskePrakse\Model\Feedback');
    }

    /**
     * @return HasMany
     */
    public function internships()
    {
        return $this->hasMany('StudentskePrakse\Model\Internship');
    }

    /**
     * @return HasMany
     */
    public function mentors()
    {
        return $this->hasMany('StudentskePrakse\Model\Mentor');
    }

    /**
     * @return HasMany
     */
    public function posts()
    {
        return $this->hasMany('StudentskePrakse\Model\Post');
    }

    /**
     * @return boolean
     */
    public function isAdministrator()
    {
        return ($this->getType() === self::TYPE_ADMIN);
    }

    /**
     * @return boolean
     */
    public function isCompany()
    {
        return ($this->getType() === self::TYPE_COMPANY);
    }
}
