<?php

namespace StudentskePrakse\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Category extends Model
{
    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return integer
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @return integer
     */
    public function getParentId()
    {
        return (int)$this->parent_id;
    }

    /**
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param integer $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return HasMany
     */
    public function internships()
    {
        return $this->hasMany('StudentskePrakse\Model\Internship');
    }

    /**
     * @return HasMany
     */
    public function users()
    {
        return $this->hasMany('StudentskePrakse\Model\User');
    }

    /**
     * @return HasOne
     */
    public function parent()
    {
        return $this->hasOne('StudentskePrakse\Model\Category', 'parent_id');
    }

    /**
     * @return HasMany
     */
    public function children()
    {
        return $this->hasMany('StudentskePrakse\Model\Category', 'parent_id');
    }

    /**
     * @param string $name
     * @param int    $order
     * @param int    $parent_id
     *
     * @return static
     */
    public static function make($name, $order, $parent_id = 0)
    {

        return new static([
            'name'      => $name,
            'order'     => $order,
            'parent_id' => $parent_id,
        ]);
    }
}
