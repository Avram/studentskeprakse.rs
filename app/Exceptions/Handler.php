<?php

namespace StudentskePrakse\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     *
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception               $e
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if (env('APP_ENV') === 'local') {
            return $this->renderExceptionWithWhoops($e);
        }

        //model not found
        if ($e instanceof EntityNotFoundException || $e instanceof NotFoundHttpException) {
            return response()->view('errors.generic', [
                'title'   => 'Nije pronađeno',
                'message' => 'Tražena stranica nije pronađena!',
            ], 404);
        }

        //recaptcha
        if ($e instanceof ReCaptchaException) {
            return response()->view('errors.generic', [
                'title'   => 'ReCaptcha greška',
                'message' => $e->getMessage(),
            ], 500);
        }

        //empty Attachment
        if ($e instanceof EmptyAttachmentException) {
            session()->flash('warning', 'Ne možete postaviti prazan prilog.');
            return redirect()->back();
        }

        //invalid Attachment
        if ($e instanceof InvalidAttachmentException) {
            session()->flash('error', 'Ovaj tip datoteke nije dozvoljen!');
            return redirect()->back();
        }

        return parent::render($request, $e);
    }

    /**
     * Render an exception using Whoops.
     *
     * @param  \Exception $e
     *
     * @return \Illuminate\Http\Response
     */
    protected function renderExceptionWithWhoops(Exception $e)
    {
        $whoops = new \Whoops\Run;
        $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());

        return new \Illuminate\Http\Response(
            $whoops->handleException($e),
            $e->getStatusCode(),
            $e->getHeaders()
        );
    }
}
