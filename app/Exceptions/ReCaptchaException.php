<?php namespace StudentskePrakse\Exceptions;

use Exception;

class ReCaptchaException extends \Exception
{
    private $errors = [
        'missing-input-secret'   => 'The secret parameter is missing.',
        'invalid-input-secret'   => 'The secret parameter is invalid or malformed.',
        'missing-input-response' => 'The response parameter is missing.',
        'invalid-input-response' => 'The response parameter is invalid or malformed.',
    ];

    /**
     * ReCaptchaException constructor.
     *
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        if (empty($errors)) {
            $this->message = 'Unknown error';
            return 0;
        }

        $arr = [];

        foreach ($errors as $err) {
            $arr[] = $this->errors[$err];
        }

        $this->message = implode("\n", $arr);
    }

}